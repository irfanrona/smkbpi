<?php
class Admin extends CI_model
{
    function berita()
    {
        $this->db->order_by('id_berita', 'DESC');
        return $this->db->get('berita')->result();
    }
    function acara()
    {
        $this->db->limit(6);
        $this->db->order_by('id', 'DESC');
        return $this->db->get('events')->result();
    }
    function galeri()
    {
        $this->db->order_by('id_galeri', 'DESC');
        return $this->db->get('galeri')->result();
    }
}
