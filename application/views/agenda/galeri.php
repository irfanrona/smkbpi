<!doctype html>

			<!-- Start Content -->
				<div id="content">
				  <div class="container">
					<div class="row portfolio-page">

					  <!-- Start Recent Projects Carousel -->
					  <ul id="portfolio-list" data-animated="fadeIn">
					  <?php foreach ($model as $data) : ?>
						<li>
						  <img src="<?php echo base_url('assets/images/galeri/'.$data->foto);?>" alt="" />
						  <div class="portfolio-item-content">
							<span class="header" style="color:#000080;"><?= $data->judul_galeri; ?></span>
							<p class="body text-uppercase" style="color:#000080;"><?= $data->deskripsi; ?></p>
						  </div>
						  <a href="<?php echo base_url('assets/images/galeri/'.$data->foto); ?>"><i class="more"><i class="fa fa-download"></i></i></a>
						</li>
						<?php endforeach; ?>
					  </ul>
					  <!-- End Portfolio Items -->
					</div>
				  </div>
				</div>
				<!-- End Content -->
				
				<!-- Start Content -->
					<div id="content">
					  <div class="container">
						<div class="row sidebar-page">

						  <!-- Page Content -->
						  <div class="col-md-9 page-content">

							<!-- Single Testimonial -->
							<div class="classic-testimonials">
							  <div class="testimonial-content">
								<p>Mohon maaf apabila tidak semua peserta yang terdokumentasi oleh Panitia Fiesta SMK BPI Bandung 2017.</p>
							  </div>
							  <div class="testimonial-author"><span>Panitia Fiesta SMK BPI Bandung</span> - Tim ICT SMK BPI Bandung</div>
							</div>
							<!-- End Testimonial -->
						  </div>
						</div>
					  </div>
					</div>  
				<!-- End Content -->