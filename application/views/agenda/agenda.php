<!doctype html>
	
	<!-- Agenda -->		
			<!-- Start Content -->
			<div id="content" style="margin-top:9px;">
			  <div class="container">
				<div class="project-page row">

				  <!-- Start Single Project Slider -->
				  <div class="project-media col-md-8">
					<div class="touch-slider project-slider">
					  <div class="item">
						<a class="lightbox" title="Pasanggiri Jaipong" href="<?php echo base_url();?>assets/images/portfolio-1/52.png" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/52.png" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Pasanggiri Jaipong" href="<?php echo base_url();?>assets/images/portfolio-1/53.png" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/53.png" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Pasanggiri Jaipong" href="<?php echo base_url();?>assets/images/portfolio-1/54.png" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/54.png" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Pasanggiri Jaipong" href="<?php echo base_url();?>assets/images/portfolio-1/56.png" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/56.png" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Pasanggiri Jaipong" href="<?php echo base_url();?>assets/images/portfolio-1/57.png" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/57.png" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Pasanggiri Jaipong" href="<?php echo base_url();?>assets/images/portfolio-1/58.png" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/59.png" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Pasanggiri Jaipong" href="<?php echo base_url();?>assets/images/portfolio-1/59.png" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/59.png" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Pasanggiri Jaipong" href="<?php echo base_url();?>assets/images/portfolio-1/60.png" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/60.png" style="width:100%;">
						</a>
					  </div>
					</div>
				  </div>
				  <!-- End Single Project Slider -->

				  <!-- Start Project Content -->
				  <div class="project-content col-md-4">
					<h4><span style="color:#000080">Pasanggiri Jaipong</span></h4>
					<p>Pasanggiri Jaipong tingkat SD dan SMP dalam rangkaian acara LABORA FIESTA 2020.</p>
					<h4><span style="color:#f8ba01;">Pasanggiri Jaipong</span></h4>
					<ul>
					  <li><strong>Diselenggarakan:</strong> 29 Februari 2020 </li>
					</ul>
					<div class="post-share">
					  <span>Share This:</span>
					  <a class="facebook" href="http://www.facebook.com/SmkBpiBandung"><i class="fa fa-facebook"></i></a>
					  <a class="twitter" href="http://www.twitter.com/smkbpibdg"><i class="fa fa-twitter"></i></a>
					</div>
				  </div>
				  <!-- End Project Content -->

				</div>

				</div>
				</div>
				<!-- End Recent Projects Carousel -->
						
			  </div>
			</div>
			<!-- End Content -->
	
	<!-- Agenda -->		
			<!-- Start Content -->
			<div id="content" style="margin-top:9px;">
			  <div class="container">
				<div class="project-page row">

				  <!-- Start Single Project Slider -->
				  <div class="project-media col-md-8">
					<div class="touch-slider project-slider">
					  <div class="item">
						<a class="lightbox" title="Workshop IOT" href="<?php echo base_url();?>assets/images/portfolio-1/44.png" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/44.png" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Workshop IOT" href="<?php echo base_url();?>assets/images/portfolio-1/45.png" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/45.png" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Workshop IOT" href="<?php echo base_url();?>assets/images/portfolio-1/46.png" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/46.png" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Workshop IOT" href="<?php echo base_url();?>assets/images/portfolio-1/47.png" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/47.png" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Workshop IOT" href="<?php echo base_url();?>assets/images/portfolio-1/48.png" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/48.png" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Workshop IOT" href="<?php echo base_url();?>assets/images/portfolio-1/49.png" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/49.png" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Workshop IOT" href="<?php echo base_url();?>assets/images/portfolio-1/50.png" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/50.png" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Workshop IOT" href="<?php echo base_url();?>assets/images/portfolio-1/51.png" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/51.png" style="width:100%;">
						</a>
					  </div>
					</div>
				  </div>
				  <!-- End Single Project Slider -->

				  <!-- Start Project Content -->
				  <div class="project-content col-md-4">
					<h4><span style="color:#000080">Pameran dan Workshop IoT</span></h4>
					<p>Pameran hasil karya siswa SMK BPI dan Workshop IoT tingkat SMP se-kota Bandung dalam rangkaian acara LABORA FIESTA 2020.</p>
					<h4><span style="color:#f8ba01;">Pameran dan Workshop</span></h4>
					<ul>
					  <li><strong>Diselenggarakan:</strong> 20 Februari 2020 </li>
					</ul>
					<div class="post-share">
					  <span>Share This:</span>
					  <a class="facebook" href="http://www.facebook.com/SmkBpiBandung"><i class="fa fa-facebook"></i></a>
					  <a class="twitter" href="http://www.twitter.com/smkbpibdg"><i class="fa fa-twitter"></i></a>
					</div>
				  </div>
				  <!-- End Project Content -->

				</div>

				</div>
				</div>
				<!-- End Recent Projects Carousel -->
						
			  </div>
			</div>
			<!-- End Content -->
	
	<!-- Agenda -->		
			<!-- Start Content -->
			<div id="content" style="margin-top:9px;">
			  <div class="container">
				<div class="project-page row">

				  <!-- Start Single Project Slider -->
				  <div class="project-media col-md-8">
					<div class="touch-slider project-slider">
					  <div class="item">
						<a class="lightbox" title="Launching TEFA" href="<?php echo base_url();?>assets/images/portfolio-1/37.png" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/37.png" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Launching TEFA" href="<?php echo base_url();?>assets/images/portfolio-1/38.png" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/38.png" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Launching TEFA" href="<?php echo base_url();?>assets/images/portfolio-1/39.png" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/39.png" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Launching TEFA" href="<?php echo base_url();?>assets/images/portfolio-1/40.png" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/40.png" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Launching TEFA" href="<?php echo base_url();?>assets/images/portfolio-1/41.png" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/41.png" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Launching TEFA" href="<?php echo base_url();?>assets/images/portfolio-1/42.png" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/42.png" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Launching TEFA" href="<?php echo base_url();?>assets/images/portfolio-1/43.png" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/43.png" style="width:100%;">
						</a>
					  </div>
					</div>
				  </div>
				  <!-- End Single Project Slider -->

				  <!-- Start Project Content -->
				  <div class="project-content col-md-4">
					<h4><span style="color:#000080">Peresmian dan Sosialisasi TEFA SMK BPI</span></h4>
					<p>Peresmian ruangan Teaching Factory (TEFA) SMK BPI oleh Kepala KCD Wil. VII serta sosialisasi program TEFA.</p>
					<h4><span style="color:#f8ba01;">Peresmian TEFA</span></h4>
					<ul>
					  <li><strong>Diselenggarakan:</strong> 22 Agustus 2019 </li>
					</ul>
					<div class="post-share">
					  <span>Share This:</span>
					  <a class="facebook" href="http://www.facebook.com/SmkBpiBandung"><i class="fa fa-facebook"></i></a>
					  <a class="twitter" href="http://www.twitter.com/smkbpibdg"><i class="fa fa-twitter"></i></a>
					</div>
				  </div>
				  <!-- End Project Content -->

				</div>

				</div>
				</div>
				<!-- End Recent Projects Carousel -->
						
			  </div>
			</div>
			<!-- End Content -->
	
	<!-- Agenda -->
			
			<!-- Start Content -->
			<div id="content" style="margin-top:9px;">
			  <div class="container">
				<div class="project-page row">

				  <!-- Start Single Project Slider -->
				  <div class="project-media col-md-8">
					<div class="touch-slider project-slider">
					  <div class="item">
						<a class="lightbox" title="Ujikom TKJ" href="<?php echo base_url();?>assets/images/portfolio-1/30.jpg" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/30.jpg" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Ujikom TKJ" href="<?php echo base_url();?>assets/images/portfolio-1/31.jpg" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/31.jpg" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Ujikom TKJ" href="<?php echo base_url();?>assets/images/portfolio-1/32.jpg" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/32.jpg" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Ujikom TKJ" href="<?php echo base_url();?>assets/images/portfolio-1/33.jpg" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/33.jpg" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Ujikom TKJ" href="<?php echo base_url();?>assets/images/portfolio-1/34.jpg" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/34.jpg" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Ujikom TKJ" href="<?php echo base_url();?>assets/images/portfolio-1/35.jpg" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/35.jpg" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Ujikom TKJ" href="<?php echo base_url();?>assets/images/portfolio-1/36.jpg" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/36.jpg" style="width:100%;">
						</a>
					  </div>
					</div>
				  </div>
				  <!-- End Single Project Slider -->

				  <!-- Start Project Content -->
				  <div class="project-content col-md-4">
					<h4><span style="color:#000080">Ujikom AP dan TKJ</span></h4>
					<p>Ujian Kompetensi Keahlian Untuk bidang Keahlian Administrasi Perkantoran dan Teknik Komputer Jaringan.</p>
					<h4><span style="color:#f8ba01;">Ujian Kompetensi Keahlian</span></h4>
					<ul>
					  <li><strong>Diselenggarakan:</strong> 16 Februari 2017 </li>
					</ul>
					<div class="post-share">
					  <span>Share This:</span>
					  <a class="facebook" href="http://www.facebook.com/SmkBpiBandung"><i class="fa fa-facebook"></i></a>
					  <a class="twitter" href="http://www.twitter.com/smkbpibdg"><i class="fa fa-twitter"></i></a>
					</div>
				  </div>
				  <!-- End Project Content -->

				</div>

				</div>
				</div>
				<!-- End Recent Projects Carousel -->
						
			  </div>
			</div>
			<!-- End Content -->
	
	<!-- Agenda -->
			
			<!-- Start Content -->
			<div id="content" style="margin-top:9px;">
			  <div class="container">
				<div class="project-page row">

				  <!-- Start Single Project Slider -->
				  <div class="project-media col-md-8">
					<div class="touch-slider project-slider">
					  <div class="item">
						<a class="lightbox" title="Fiesta SMK BPI" href="<?php echo base_url();?>assets/images/portfolio-1/1.jpg" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/1.jpg" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Fiesta SMK BPI" href="<?php echo base_url();?>assets/images/portfolio-1/24.jpg" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/24.jpg" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Fiesta SMK BPI" href="<?php echo base_url();?>assets/images/portfolio-1/25.jpg" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/25.jpg" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Fiesta SMK BPI" href="<?php echo base_url();?>assets/images/portfolio-1/2.jpg" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class=" fafa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/2.jpg" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Fiesta SMK BPI" href="<?php echo base_url();?>assets/images/portfolio-1/3.jpg" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/3.jpg" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Fiesta SMK BPI" href="<?php echo base_url();?>assets/images/portfolio-1/4.jpg" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/4.jpg" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Fiesta SMK BPI" href="<?php echo base_url();?>assets/images/portfolio-1/5.jpg" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/5.jpg" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Fiesta SMK BPI" href="<?php echo base_url();?>assets/images/portfolio-1/6.jpg" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/6.jpg" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Fiesta SMK BPI" href="<?php echo base_url();?>assets/images/portfolio-1/7.jpg" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/7.jpg" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Fiesta SMK BPI" href="<?php echo base_url();?>assets/images/portfolio-1/8.jpg" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/8.jpg" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Fiesta SMK BPI" href="<?php echo base_url();?>assets/images/portfolio-1/9.jpg" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/9.jpg" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Fiesta SMK BPI" href="<?php echo base_url();?>assets/images/portfolio-1/10.jpg" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/10.jpg" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Fiesta SMK BPI" href="<?php echo base_url();?>assets/images/portfolio-1/11.jpg" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/11.jpg" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Fiesta SMK BPI" href="<?php echo base_url();?>assets/images/portfolio-1/12.jpg" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/12.jpg" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Fiesta SMK BPI" href="<?php echo base_url();?>assets/images/portfolio-1/13.jpg" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/13.jpg" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Fiesta SMK BPI" href="<?php echo base_url();?>assets/images/portfolio-1/14.jpg" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/14.jpg" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Fiesta SMK BPI" href="<?php echo base_url();?>assets/images/portfolio-1/15.jpg" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/15.jpg" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Fiesta SMK BPI" href="<?php echo base_url();?>assets/images/portfolio-1/16.jpg" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/16.jpg" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Fiesta SMK BPI" href="<?php echo base_url();?>assets/images/portfolio-1/17.jpg" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/17.jpg" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Fiesta SMK BPI" href="<?php echo base_url();?>assets/images/portfolio-1/18.jpg" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/18.jpg" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Fiesta SMK BPI" href="<?php echo base_url();?>assets/images/portfolio-1/19.jpg" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/19.jpg" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Fiesta SMK BPI" href="<?php echo base_url();?>assets/images/portfolio-1/20.jpg" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/20.jpg" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Fiesta SMK BPI" href="<?php echo base_url();?>assets/images/portfolio-1/21.jpg" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/21.jpg" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Fiesta SMK BPI" href="<?php echo base_url();?>assets/images/portfolio-1/22.jpg" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/22.jpg" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Fiesta SMK BPI" href="<?php echo base_url();?>assets/images/portfolio-1/23.jpg" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/23.jpg" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Fiesta SMK BPI" href="<?php echo base_url();?>assets/images/portfolio-1/26.jpg" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/26.jpg" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Fiesta SMK BPI" href="<?php echo base_url();?>assets/images/portfolio-1/27.jpg" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/27.jpg" style="width:100%;">
						</a>
					  </div>
					  <div class="item">
						<a class="lightbox" title="Fiesta SMK BPI" href="<?php echo base_url();?>assets/images/portfolio-1/28.jpg" data-lightbox-gallery="gallery2">
						  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
						  <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/28.jpg" style="width:100%;">
						</a>
					  </div>
					</div>
				  </div>
				  <!-- End Single Project Slider -->

				  <!-- Start Project Content -->
				  <div class="project-content col-md-4">
					<h4><span style="color:#000080">FIESTA SMK BPI Bandung</span></h4>
					<p>Lomba Kreatifitas Pelajar dengan lomba akustik tingkat SMP dan SMA/SMK dan lomba Jaipongan dari Tingkat TK, SD dan SMP .</p>
					<h4><span style="color:#f8ba01;">Kegiatan Acara Fiesta</span></h4>
					<ul>
					  <li><strong>Diselenggarakan:</strong> 9,11-12 Februari 2017 </li>
					</ul>
					<div class="post-share">
					  <span>Share This:</span>
					  <a class="facebook" href="http://www.facebook.com/SmkBpiBandung"><i class="fa fa-facebook"></i></a>
					  <a class="twitter" href="http://www.twitter.com/smkbpibdg"><i class="fa fa-twitter"></i></a>
					</div>
				  </div>
				  <!-- End Project Content -->

				</div>

				</div>
				</div>
				<!-- End Recent Projects Carousel -->