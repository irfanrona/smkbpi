<!doctype html>
	<!-- Start Content -->
	<div id="content">
		<div class="container">
            <div class="page-content" style="margin-top:-50px">

                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                        <h1 class="page-header" style="color:blue;">Welcome to
                                <small>Dashboard web admin SMK BPI Bandung</small>
                        </h1>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-right">
                        <img src="<?php echo base_url() ?>assets/images/pegawai.png" width="80px" height="40px" style="margin-top:30px;"></img>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3 portfolio-item">
                        <div class="menu-item blue" style="height:150px;">
                            <a href="<?php echo base_url().'adminController/acara'?>" class="text-center" data-toggle="modal">
                                <i class="fa fa-calendar"></i>
                                    <h5 class="white"> Acara</h5>
                            </a>
                        </div> 
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 portfolio-item">
                        <div class="menu-item green" style="height:150px;">
                            <a href="<?php echo base_url().'adminController/berita'?>" class="text-center" data-toggle="modal">
                                <i class="fa fa-sticky-note"></i>
                                    <h5 class="white"> Berita</h5>
                            </a>
                        </div> 
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 portfolio-item">
                        <div class="menu-item light-orange" style="height:150px;">
                            <a href="<?php echo base_url().'adminController/gallery'?>" class="text-center" data-toggle="modal">
                                <i class="fa fa-image"></i>
                                    <h5 class="white"> Galeri</h5>
                            </a>
                        </div> 
                    </div>
                    <!-- <div class="col-lg-3 col-md-3 col-sm-3 portfolio-item">
                        <div class="menu-item purple" style="height:150px;">
                            <a href="<?php echo base_url().'adminController/gallery'?>" class="text-center" data-toggle="modal">
                                <i class="fa fa-user"></i>
                                    <h5 class="white"> Kepegawaian</h5>
                            </a>
                        </div> 
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 portfolio-item">
                        <div class="menu-item red" style="height:150px;">
                            <a href="<?php echo base_url().'adminController/gallery'?>" class="text-center" data-toggle="modal">
                                <i class="fa fa-trophy"></i>
                                    <h5 class="white"> Prestasi</h5>
                            </a>
                        </div> 
                    </div> -->
                    <div class="col-lg-3 col-md-3 col-sm-3 portfolio-item">
                        <div class="menu-item color" style="height:150px;">
                            <a href="<?php echo base_url().'adminController/setting'?>" class="text-center" data-toggle="modal">
                                <i class="fa fa-cogs"></i>
                                    <h5 class="white"> Pengaturan</h5>
                            </a>
                        </div> 
                    </div>
                </div>

            </div>
		</div>
	</div>
	<!-- End Content -->