<style>
    .radius{
        border-radius: 50px;
        height: 50px;
        width: 100%;
    }
    .enter{
        padding-top:50px;
    }
    @media (min-width: 768px) {
        .enter{
            padding-top:10px;
        }
    }
</style>
<div class="pricing-section" style="padding-top:60px; padding-bottom:60px;background:#f9f9f9; margin-bottom:40px;">
    <div class="container">
    
    <div class="row pricing-tables">

        <div class="col-md-5 col-sm-5">
            <div style="align:center">
                <div class="pricing-table highlight-plan" style="">
                    <div class="plan-price">
                        <div style="padding:30px;padding-bottom:20px">
                        <div class="text-center">
                            <img src="<?php echo base_url() ?>assets/images/pegawai.png" width="150px" height="50px"></img>
                        </div>
                        <?= $this->session->flashdata('pesan') ?>
                            <form method="POST" action="<?= base_url('home/auth') ?>">
                                <div class="form-group">
                                    <input type="text" class="form-control radius" name="username" placeholder="Username ..." required>
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control radius" name="password" placeholder="Password ..." required>
                                </div>
                            <hr>
                                <button type="submit" class="btn btn-primary radius">Login</button>
                            </form>
                            <hr>
                            <div class="text-center">
                                <a class="small" href="forgot-password.html">Lupa Password?</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-md-7 col-sm-7">
            <div class="row">
                <div class="col-md-12 enter">
                <div class="big-title text-center">
                    <h1><strong>LOGIN, </strong>Hanya untuk petugas yang diizinkan </h1>
                </div>
                <p class="text-center" style="margin-bottom:20px;">Website SMK BPI Bandung.</p>
                </div>
                <div class="col-md-12">
                    <img src="<?= base_url('assets/images/web-cogs.png') ?>" class="img-fluid d-block w-100">
                </div>
            </div>
        </div>
    </div>
    </div>
</div>