

    <!-- Start Content -->

	<div id="content">

		<div class="container">

            <div class="page-content">

                <div class="row">

                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-8">

                        <p class="text-uppercase" style="margin-top:10px">DATA NEWS</p>

                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 text-right">

                        <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">

                            <i class="fa fa-plus"></i> Buat Berita

                        </button>

                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12">

                        <div class="collapse" id="collapseExample">

                            <div class="well">

                                <form method="POST" action="<?= base_url('adminController/save_berita') ?>">

                                    <div class="form-group">

                                        <label>Judul</label>

                                        <input type="text" name="judul_berita" class="form-control" placeholder="...">

                                    </div>

                                    <div class="form-group">

                                        <label>Berita</label>

                                        <textarea class="form-control" name="berita" rows="3"></textarea>

                                    </div>

                                    <div class="form-group text-right">

                                        <button type="submit" class="btn btn-success">SIMPAN</button>

                                    </div>

                                </form>

                            </div>

                        </div>

                    </div>

                </div>

                <hr>

                <div class="flash-data" data-flashdata="<?=$this->session->flashdata('notif')?>"></div>

                <div class="row">

                    <div class="table-responsive">

                        <table class="table table-hover" id="mydata">

                            <thead>

                                <tr>

                                    <th class="text-center">NO</th>

                                    <th>Judul</th>

                                    <th>Berita</th>

                                    <th>Tanggal</th>

                                    <th class="text-center"><span class="fa fa-cog"></span></th>

                                </tr>

                            </thead>

                            <tbody>

                                <?php

                                    $no = 1;

                                    foreach ($model as $data) :

                                        ?>

                                        <tr>

                                            <td class="text-center"><?= $no; ?></td>

                                            <td><?= $data->judul_berita; ?></td>

                                            <td class="read"><?= $data->berita; ?></td>

                                            <td><?= date('Y-m-d', strtotime($data->create_dt)); ?></td>

                                            <td colspan="2" class="text-center">

                                                <a href="javascript:void();" data-id="<?= $data->id_berita; ?>" data-judul="<?= $data->judul_berita; ?>" data-create_dt="<?= date('Y-m-d', strtotime($data->create_dt)); ?>" data-berita="<?= $data->berita; ?>" data-toggle="modal" data-target="#form-edit" class="btn btn-warning"><span class="fa fa-pencil"></span></a>

                                                <a href="<?= base_url('adminController/delete_berita/'.$data->id_berita) ?>" data-id="<?= $data->id_berita; ?>" class="btn btn-danger btn-delete"><span class="fa fa-trash"></span></a>

                                            </td>

                                        </tr>

                                    <?php

                                        $no++;

                                    endforeach;

                                    ?>

                            </tbody>

                        </table>

                    </div>

                </div>



            </div>

		</div>

	</div>

    <!-- End Content -->

    <!-- ====================================== MODAL =============================================== -->

    <div class="modal fade" id="form-edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="margin-top:100px">

    <div class="modal-dialog" role="document">

        <div class="modal-content">

        <div class="modal-header">

            <h5 class="modal-title" id="exampleModalLabel">Data Berita</h5>

            <button type="button" class="close" data-dismiss="modal" aria-label="Close">

            <span aria-hidden="true">&times;</span>

            </button>

        </div>

                <div class="modal-body">

                    <form method="POST" action="<?= base_url('adminController/update_berita') ?>">

                        <input type="hidden" id="id" name="id_berita">

                        <div class="form-group">

                            <label>Judul</label>

                            <input type="text" class="form-control" id="judul" name="judul_berita">

                        </div>

                        <div class="form-group">

                            <label>Berita</label>

                            <textarea class="form-control" id="berita" name="berita" rows="3"></textarea>

                        </div>

                        <div class="form-group">

                            <label>Tanggal Posting</label>

                            <input type="text" class="form-control date" id="create_dt" name="create_dt">

                        </div>

                    </div>

                    <div class="modal-footer">

                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>

                        <button type="submit" class="btn btn-primary">Ubah</button>

                    </form>

                </div>

            </div>

        </div>

    </div>

    

    <script>

        $('#form-edit').on('show.bs.modal', function(event) {

            var div = $(event.relatedTarget)

            var modal = $(this)



            modal.find('#id').attr("value", div.data('id'));

            modal.find('#judul').attr("value", div.data('judul'));

            modal.find('#berita').attr("value", div.data('berita'));

            modal.find('#create_dt').attr("value", div.data('create_dt'));

        });

    </script>