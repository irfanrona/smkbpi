<!-- Start Content -->
    <div id="content">
        <div class="container">
            <div class="page-content">
                <div class="row">
                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-8">
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="well">
                                <div class="alert warning">
                                  <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>  
                                  <strong>Ketentuan!</strong> 
                                  <p style="color: white">Isi data secara lengkap dan jelas sesuai dengan data yang tertera di <b>KARTU KELUARGA</b> atau <b>AKTA KELAHIRAN</b>. Setelah mengisi pendaftaran mohon tunggu informasi berikutnya yang akan disampaikan ke nomor kontak pendaftar.</p>
                                </div>
                                <form method="POST" action="<?= base_url('home/pendaftaran') ?>" enctype="multipart/form-data">
                                    <br><h1 class="title" align="center">FORMULIR PENDAFTARAN</h1>
                                    <h1 class="title" align="center">PESERTA DIDIK BARU TA 2020/2021</h1><br><br>
                                    <div class="form-group">
                                        <label style="font-weight: bold">Nama Lengkap Calon Peserta Didik</label>
                                        <input type="text" name="name" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label style="font-weight: bold">Nomor Induk Siswa Nasional (NISN)</label>
                                        <input type="text" name="nisn" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label style="font-weight: bold">Jenis Kelamin</label>
                                        <select class="form-control" name="gender" required>
                                            <option>--Pilih Salah Satu--</option>
                                            <option value="L">Laki-Laki</option>
                                            <option value="P">Perempuan</option>
                                      </select>
                                    </div>
                                    <div class="form-group">
                                        <label style="font-weight: bold">Tempat Lahir</label>
                                        <input type="text" name="birthplace" class="form-control" required>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-lg-6">
                                            <label style="font-weight: bold">Tanggal Lahir</label>
                                            <div class="input-group">
                                                <input type="text" name="birthdate" id="datepicker" class="form-control bg-light border-0 small" aria-label="Search" aria-describedby="basic-addon2">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label style="font-weight: bold">Agama</label>
                                        <input type="text" name="religion" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label style="font-weight: bold">Alamat Rumah Terakhir</label>
                                        <textarea class="form-control" name="address" rows="3" required></textarea>
                                        </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="form-group col-lg-6">
                                                <label style="font-weight: bold"> Nomor Handphone Orang Tua </label>
                                                <input type="text" name="notelp" class="form-control" required>
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <label style="font-weight: bold"> Nomor Handphone Calon Peserta Didik</label>
                                                <input type="text" name="nohp" class="form-control" required>
                                            </div>
                                        </div>
                                        <span style="color: red; font-size: 12px;">*Pastikan nomor handphone di atas telah diisi dengan benar karena data ini akan menjadi media komunikasi PPDB Online</span>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-group">
                                            <label style="font-weight: bold"> Alamat E-mail </label>
                                            <input type="email" name="email" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label style="font-weight: bold">Nama Ayah/ Ibu/ Wali</label>
                                        <input type="text" name="parentname" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label style="font-weight: bold">Asal Sekolah</label>
                                        <input type="text" name="school" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label style="font-weight: bold">Jurusan yang Dipilih (pilih salah satu)</label>
                                        <div class="custom-control custom-radio">
                                          <input type="radio" class="custom-control-input" id="otkp" name="major" value="OTKP" required>
                                          <label class="custom-control-label" for="otkp">OTKP (Otomatisasi Tata Kelola Perkantoran)</label><br>
                                          <input type="radio" class="custom-control-input" id="rpl" name="major" value="RPL">
                                          <label class="custom-control-label" for="rpl">RPL (Rekayasa Perangkat Lunak)</label><br>
                                          <input type="radio" class="custom-control-input" id="tkj" name="major" value="TKJ">
                                          <label class="custom-control-label" for="tkj">TKJ (Teknik Komputer Jaringan)</label><br>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label style="font-weight: bold">Dari mana anda mengetahui Informasi SMK BPI?</label><br>
                                        <input type="checkbox"  id="checkItem" name="information[]" value="Media Cetak">
                                            <label for="option1"> Melalui Media Cetak (Koran/ Brosur/ Famplet/ Spanduk)</label><br>
                                        <input type="checkbox"  id="checkItem" name="information[]" value="Media Elektronik">
                                            <label for="option2"> Melalui Media Elektronik (TV/ Radio)</label><br>
                                        <input type="checkbox"  id="checkItem" name="information[]" value="Media Online">
                                            <label for="option2"> Melalui Media Online (Situs Web/ Sosial Media)</label><br>
                                        <input type="checkbox"  id="checkItem" name="information[]" value="Info Keluarga">
                                            <label for="option3"> Melalui Informasi Keluarga/ Famili</label><br>
                                        <input type="checkbox"  id="checkItem" name="information[]" value="Kerjasama">
                                            <label for="option4"> Melalui Kerjasama dengan pihak Sekolah (MoU)</label><br>
                                        <input type="checkbox"  id="checkItem" name="information[]" value="Lainnya">
                                            <label for="option5"> Lainnya</label><br><br>
                                        <span style="color: red; font-size: 15px;">Catatan : Pengisian formulir pendaftaran bukan jaminan diterima, segera selesaikan proses administrasi sebelum kuota habis</span>
                                    </div><br>
                                    <div class="form-group text-center">
                                        <center><button type="submit" class="btn btn-primary">DAFTAR</button></center>
                                    </div>
                                </form>
                            </div>
                    </div>
                </div>
                <br>
            </div>
        </div>
    </div>

<script type="text/javascript">
    var elements = document.querySelectorAll('input,select,textarea');

    for (var i = elements.length; i--;) {
        elements[i].addEventListener('invalid', function () {
            this.scrollIntoView(false);
        });
    }

    $(document).ready(function(){

     $('#datepicker').datepicker({
      dateFormat: "dd-mm-yy",
      maxDate: '-10y',
      minDate: '-20y',
      changeMonth: true,
      changeYear: true

     });

    });
    
</script>