<style type="text/css">
	@media only screen and (max-width : 550px) {

		.pricing-tables {
			margin-left: -2px;
		}
	}

	@media(min-width : 375px) {

		.pricing-tables {
			margin-left: -30px;
		}
	}

	@media(min-width : 414px) {

		.pricing-tables {
			margin-left: -20px;
		}
	}

	@media(min-width : 768px) {
		.pricing-tables {
			margin-left: 150px;
		}

		.pricing-tables .highlight-plan {
			width: 400px;
		}
	}

	@media (min-width: 1024px) {
		.pricing-tables {
			margin-left: 350px;
		}

		.pricing-tables .highlight-plan {
			width: 400px;
		}

	}

	@media (min-width: 1152px) {
		.pricing-tables {
			margin-left: 345px;
		}

		.pricing-tables .highlight-plan {
			width: 400px;
		}

	}
</style>

<section id="home">
	<!-- Tampilan Carousel -->
	<div id="main-slide" class="carousel slide" data-ride="carousel">

		<ol class="carousel-indicators">
			<li data-target="#main-slide" data-slide-to="0" class="active"></li>
			<li data-target="#main-slide" data-slide-to="1"></li>
			<li data-target="#main-slide" data-slide-to="2"></li>
			<li data-target="#main-slide" data-slide-to="3"></li>
			<li data-target="#main-slide" data-slide-to="4"></li>
			<li data-target="#main-slide" data-slide-to="5"></li>
		</ol>
		<div class="carousel-inner">
			<div class="item active">
				<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/slider/first.jpg" alt="slider">
				<div class="slider-content">
					<div class="col-md-12 text-center">
						<h2 class="animated2">
							<span style="color:#000080"><b>Welcome to <strong>SMK BPI</strong> Site</b></span>
						</h2>
						<h3 class="animated3">
							<span style="color:#000080"><b>Bermartabat, Berkualitas, dan Terpercaya !</b></span>
						</h3>
						</p>
					</div>
				</div>
			</div>

			<!-- <div class="item">
					<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/slider/maulid.png" alt="slider">
						<div class="slider-content">
							<div class="col-md-12 text-center">
							</div>
						</div>
				  </div>				  				   -->
			<!-- <div class="item">
					<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/slider/ppdbnew.png" alt="slider">
						<div class="slider-content">
							<div class="col-md-12 text-center">
							<p class="animated6" style="margin-top:60px;margin-left:130px;"><a href="https://api.whatsapp.com/send?phone=6282116359966&text=Saya%20tertarik%20untuk%20mendaftarkan%20anak%20saya%20di%20SMK%20BPI" class="slider btn btn-system btn-large">Daftar Sekarang!</a>
							</p>
							</div>
						</div>
				  </div> -->
			<div class="item">
				<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/slider/yayasan1.png" alt="slider">
				<div class="slider-content">
					<div class="col-md-5">
						<p class="animated6" style="margin-top:60px;"><a href="https://ppdb.bpi.web.id/" class="slider btn btn-system btn-large">Web Yayasan BPI</a>
						</p>
					</div>
				</div>
			</div>
			<!-- <div class="item">
					<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/slider/jurusan.jpg" alt="slider">
						<div class="slider-content">
							<div class="col-md-12 text-center">
							</div>
						</div>
				  </div> -->
			<div class="item">
				<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/slider/ap.jpg" alt="slider">
				<div class="slider-content">
					<div class="col-md-12 text-center">
					</div>
				</div>
			</div>
			<div class="item">
				<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/slider/rpl_fix.jpg" alt="slider">
				<div class="slider-content">
					<div class="col-md-12 text-center">
					</div>
				</div>
			</div>
			<div class="item">
				<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/slider/tkj.jpg" alt="slider">
				<div class="slider-content">
					<div class="col-md-12 text-center">
					</div>
				</div>
			</div>
			<!-- <div class="item">
					<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/slider/pengawas.jpg" alt="slider">
					<div class="slider-content">
					  <div class="col-md-12 text-center">
						<p class="animated6" style="margin-top:70px;"><a href="http://psmk.kemdikbud.go.id/" class="slider btn btn-system btn-large">Pengawas SMK</a>
						</p>
					  </div>
					</div>
				  </div> -->
			<!-- <div class="item">
					<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/slider/mou.jpg" alt="slider">
						<div class="slider-content">
							<div class="col-md-12 text-center">
							</div>
						</div>
				  </div>
				  <div class="item">
					<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/slider/pahlawan.jpg" alt="slider">
						<div class="slider-content">
							<div class="col-md-12 text-center">
							</div>
						</div>
				  </div> -->
			<!-- <div class="item">
					<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/slider/baktis.jpg" alt="slider">
					<div class="slider-content">
					  <div class="col-md-12 text-center">
						<p class="animated6" style="margin-top:70px;"><a href="http://jabar.tribunnews.com/2017/04/21/peringati-hari-kartini-pelajar-smk-bpi-gelar-bakti-sosial-di-pasar-palasari" class="slider btn btn-system btn-large">Tribun Jabar</a>
						</p>
					  </div>
					</div>
				  </div> -->
			<!-- <div class="item">
					<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/slider/ujikom-rpl.jpg" alt="slider">
						<div class="slider-content">
							<div class="col-md-12 text-center">
								
							</div>
						</div>
				  </div>
				  <div class="item">
					<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/slider/ujikom-tkj.jpg" alt="slider">
						<div class="slider-content">
							<div class="col-md-12 text-center">
								
							</div>
						</div>
				  </div> -->
			<!-- <div class="item">
					<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/slider/verifikasi.jpg" alt="slider">
						<div class="slider-content">
							<div class="col-md-12 text-center">
								
							</div>
						</div>
				  </div> -->
			<!-- <div class="item">
					<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/slider/table.jpg" alt="slider">
						<div class="slider-content">
							<div class="col-md-12 text-center">
								
							</div>
						</div>
				  </div>
				  <div class="item">
					<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/slider/cantik.jpg" alt="slider">
						<div class="slider-content">
							<div class="col-md-12 text-center">
							</div>
						</div>
				  </div> -->
			<!-- <div class="item">
					<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/slider/kelompok_rohis.jpg" alt="slider">
						<div class="slider-content">
							<div class="col-md-12 text-center">
								
							</div>
						</div>
				  </div> -->

		</div>

		<!-- Tampilan Panah -->
		<a class="left carousel-control" href="#main-slide" data-slide="prev">
			<span><i class="fa fa-angle-left"></i></span>
		</a>
		<a class="right carousel-control" href="#main-slide" data-slide="next">
			<span><i class="fa fa-angle-right"></i></span>
		</a>
	</div>
</section>

<div id="content" class="full-sections">
	<div class="section" style="padding-top:60px; padding-bottom:60px; border-top:1px solid #eee; border-bottom:1px solid #eee; background:#f9f9f9;">
		<div class="container">

			<div class="row">

				<div class="col-md-6">

					<div class="big-title">
						<h1><strong>Mengapa Harus</strong> memilih SMK ?</h1>
					</div>


					<!-- Pembatas -->
					<div class="hr1" style="margin-bottom:14px;"></div>
					<p>Video yang menjelaskan perbedaan antara sekolah pada Sekolah Menengah Atas dengan Sekolah Menengah Kejuruan seperti <a href="http://www.smkbpi.sch.id/" target="_blank">SMK BPI Bandung</a>, SMK BISA !</p>
					<div class="hr1" style="margin-bottom:20px;"></div>
				</div>

				<!-- Video Profil -->
				<div class="col-md-6">
					<iframe width="600" height="350" src="https://www.youtube.com/embed/fqufyZnJtgk" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

				</div>

			</div>

		</div>
	</div>
</div>


<!-- Acara Terbaru -->
<div id="content">
	<div class="container">
		<div class="page-content">

			<div class="recent-projects">
				<h4 class="title"><span>Acara Terbaru</span></h4>
				<div class="projects-carousel touch-carousel">

					<!-- Foto Acara Terbaru update-->
					<?php foreach ($acara as $data) : ?>
						<div class="portfolio-item item">
							<div class="portfolio-border">

								<div class="portfolio-thumb">
									<a class="lightbox">
										<div class="thumb-overlay"><i class="fa fa-arrow-alt"></i></div>
										<center><img alt="" src="<?php echo base_url('assets/images/events/' . $data->image); ?>" style="height: 180px" /></center>
									</a>
								</div>

								<!-- Penjelasan -->
								<div class="portfolio-details">
									<a href="<?php echo site_url('home/galeri'); ?>">
										<h4><?= $data->title ?></h4>
										<span><?= $data->description ?></span>
									</a>
								</div>
							</div>
						</div>
					<?php endforeach; ?>

				</div>
			</div>
		</div>
	</div><br><br>



	<div id="content" style="margin-top:-55px;">
		<div class="container">
			<div class="latest-posts">
				<h4 class="classic-title"><span><b>Berita Terbaru</b></span></h4>
				<div class="latest-posts-classic custom-carousel touch-carousel" data-appeared-items="3">

					<?php foreach ($berita as $data) : ?>
						<div class="post-row item">
							<div class="left-meta-post">
								<div class="post-date"><span class="day"><?= date('d', strtotime($data->create_dt)) ?></span><span class="month"><?= date('M',  strtotime($data->create_dt)) ?></span></div>
								<div class="post-type"><i class="fa fa-picture-o"></i></div>
							</div>
							<h3 class="post-title"><a href="#"><?= $data->judul_berita ?></a></h3>
							<div class="post-content readmore">
								<p><?= $data->berita ?></p>
							</div>
						</div>
					<?php endforeach; ?>

				</div>
			</div>
		</div>
	</div>

	<div id="content" class="full-sections">
		<div class="section" style="padding-top:60px; padding-bottom:60px; border-top:1px solid #eee; border-bottom:1px solid #eee; background:#f9f9f9;">
			<div class="container">

				<div class="row">

					<div class="col-md-6">

						<div class="big-title">
							<h1><strong>Profil SMK BPI</strong> Bandung</h1>
							<p class="title-desc">Berkualitas, Terpercaya dan Berprestasi !</p>
						</div>

						<p>SMK BPI Bandung adalah sebuah sekolah kejuruan yang dibawahi <a href="http://main.bpiedu.id/" target="_blank">Yayasan Badan Perguruan Indonesia </a>, yang mempunyai 3 Program Studi keahlian yang sudah terakreditasi antara lain :</p>

						<!-- Pembatas -->
						<div class="hr1" style="margin-bottom:14px;"></div>

						<div class="row">
							<div class="col-md-7">
								<ul class="icons-list">
									<li><i class="fa fa-check-circle"></i> Otomatisasi dan Tata Kelola Perkantoran.</li>
									<li><i class="fa fa-check-circle"></i> Rekayasa Perangkat Lunak.</li>
									<li><i class="fa fa-check-circle"></i> Teknik Komputer Jaringan.</li>
								</ul>
							</div>
						</div>

						<div class="hr1" style="margin-bottom:20px;"></div>

						<!-- Read more Profil -->
						<a href="<?php echo site_url('home/profil'); ?>" class="btn-system btn-small">Read More Profil SMK BPI</a>

						<div class="hr1" style="margin-bottom:20px;"></div>
					</div>

					<!-- Video Profil -->
					<div class="col-md-6">
						<iframe width="600" height="350" src="https://www.youtube.com/embed/2L2qn0pO2Gw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
					</div>

				</div>

			</div>
		</div>
	</div>

	<!-- Tampilan Animasi Text -->
	<div class="section purchase" style="<?php base_url(); ?>background: url(assets/images/parallax/tes.png) no-repeat;">
		<div class="container">
			<div class="section-video-content text-center">

				<!-- Tampilan Animasi Text -->
				<h1 class="fittext wite-text uppercase tlt" style="color:#f8ba01; font-weight:400; ">
					<span class="texts">
						<span>Berakhlak Mulia</span>
						<span>Berprestasi</span>
						<span>Organisasi</span>
						<span>Inovatif</span>
						<span>Kreatif</span>
					</span>
					adalah Pedoman<br />SMK<strong> BPI</strong> Bandung
				</h1>
			</div>
		</div>
	</div>
	<!-- Pembatas-->
	<div class="hr1" style="margin-bottom:50px;"></div>

	<!-- Tampilan Alumni -->
	<div>
		<div class="container">
			<div class="row">

				<div class="col-md-3">
					<!-- Start Big Heading -->
					<div class="big-title">
						<h1>Alumni <strong>SMK BPI</strong></h1>
						<p class="title-desc">Alumni Side</p>
					</div>
					<!-- End Big Heading -->

					<!-- Some Text -->
					<p>Ini adalah Pesan Kesan Alumni setelah lulus dari SMK BPI Bandung.</p>

					<!-- Divider -->
					<div class="hr1" style="margin-bottom:10px;"></div>

				</div>

				<!-- Alumni 1 -->
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="team-member">
						<!-- Alumni 1 -->
						<div class="member-photo">
							<img alt="" src="<?php echo base_url(); ?>assets/images/team/hamdan.jpg" />
							<div class="member-name">Hamdan Hanafi<span style="color:black; font-weight:500;">PT. Smooets</span></div>
						</div>
						<!-- Alumni 1 Words -->
						<div class="member-info">
							<p>" SMK BPI adalah sekolah yang menyenangkan, Alhamdulillah, saya dibimbing dan dididik oleh guru-guru yang berpengalaman serta selalu disiplin penuh terhadap siswanya."</p>
						</div>
					</div>
				</div>

				<!-- Alumni 2 -->
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="team-member">
						<!-- Alumni 2 -->
						<div class="member-photo">
							<img alt="" src="<?php base_url(); ?>assets/images/team/try.jpg" />
							<div class="member-name">Achmad Try August<span style="color:black;font-weight:500;">Teknisi IT di PT.BIT</span></div>
						</div>
						<!-- Alumni 2 Words -->
						<div class="member-info">
							<p>" Luar biasa, karena saya sangat menikmati setiap detik saat bersekolah di SMK BPI. Banyak hal dan kenangan menarik yang saya bisa dapat di SMK BPI."</p>
						</div>
					</div>
				</div>

				<!-- Alumni 3 -->
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="team-member">
						<!-- Alumni 3 -->
						<div class="member-photo">
							<img alt="" src="<?php base_url(); ?>assets/images/team/meta.jpg" />
							<div class="member-name">Meta Rahayu<span style="color:black; font-weight:500">Manajemen UPI</span></div>
						</div>
						<!-- Alumni 3 Words -->
						<div class="member-info">
							<p>" Alhamdulillah, banyak ilmu dan manfaat yang saya dapatkan selama belajar di SMK BPI, berkat program yang dilakukan di sekolah, saya dapat menjadi pribadi yang lebih baik."</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<!-- Pembatas -->
	<div class="hr1" style="margin-bottom:60px;"></div>

	<!-- Tampilan Mitra Perusahaan -->
	<div class="partner">
		<div class="container">
			<div class="row">
				<div class="big-title text-center">
					<h1>Mitra <strong>Perusahaan</strong></h1>
					<p class="title-desc">Kita bekerja sama dan mitra dengan Perusahaan</p>
				</div>

				<!-- Carousel Mitra Perusahaan --->
				<div class="our-clients">
					<div class="clients-carousel custom-carousel touch-carousel navigation-3" data-appeared-items="6" data-navigation="true">

						<!-- Perusahaan 1 -->
						<div class="client-item item">
							<a href="https://www.smooets.com/"><img src="<?php echo base_url(); ?>assets/images/smooets.png" alt="" style="margin-top:20px;" /></a>
						</div>

						<!-- Perusahaan 2 -->
						<div class="client-item item">
							<a href="http://www.mikrotik.com/"><img src="<?php echo base_url(); ?>assets/images/mikrotik.png" alt="" /></a>
						</div>

						<!-- Perusahaan 3 -->
						<div class="client-item item">
							<a href="https://www.skyline.net.id/"><img src="<?php echo base_url(); ?>assets/images/skyline.png" alt="" /></a>
						</div>

						<!-- Perusahaan 4 -->
						<div class="client-item item">
							<a href="http://www.cgs.co.id/newcgs/"><img src="<?php echo base_url(); ?>assets/images/cgs.jpg" alt="" /></a>
						</div>

						<!-- Perusahaan 5 -->
						<div class="client-item item">
							<a href="http://www.axiooworld.com/"><img src="<?php echo base_url(); ?>assets/images/axioo.png" alt="" /></a>
						</div>

						<!-- Perusahaan 6 -->
						<div class="client-item item">
							<a href="http://www.melsa.net.id/"><img src="<?php echo base_url(); ?>assets/images/melsa.png" alt="" /></a>
						</div>

						<!-- Perusahaan 7 -->
						<div class="client-item item">
							<a href="http://www.amikom.ac.id/"><img src="<?php echo base_url(); ?>assets/images/amikom1.png" alt="" /></a>
						</div>

						<!-- Perusahaan 8 -->
						<div class="client-item item">
							<a href="http://www.msvpictures.com/"><img src="<?php echo base_url(); ?>assets/images/msv.png" alt="" /></a>
						</div>

						<!-- Perusahaan 9 -->
						<div class="client-item item">
							<a href="http://www.belajarmikrotik.com/"><img src="<?php echo base_url(); ?>assets/images/belajar_mikrotik.png" alt="" /></a>
						</div>

						<!-- Perusahaan 10 -->
						<div class="client-item item">
							<a href="https://www.inti.co.id/"><img src="<?php echo base_url(); ?>assets/images/inti.png" alt="" /></a>
						</div>

						<!-- Perusahaan 11 -->
						<div class="client-item item">
							<a href="https://www.itb.ac.id/"><img src="<?php echo base_url(); ?>assets/images/itb.jpg" alt="" /></a>
						</div>

						<!-- Perusahaan 12 -->
						<div class="client-item item">
							<a href="http://aspapi.org/"><img src="<?php echo base_url(); ?>assets/images/aspapi.jpg" alt="" /></a>
						</div>



					</div>
				</div>
			</div>
		</div>
	</div>