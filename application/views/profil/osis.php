<!doctype html>
			
	<!-- Start Content -->
	<div id="content">
		<div class="container">
		<div class="page-content">
			<div class="error-page">
			<img src="<?php echo base_url(); ?>assets/images/osis.png" style="margin-top:-120px;"></img>
			</div>
		</div>
		</div>
	</div>
	<!-- End Content -->
	
	<!-- Start Content -->
    <div id="content" style="margin-top:-200px;">
      <div class="container">
        <div class="page-content">


          <div class="row">

            <div class="col-md-7">

              <!-- Osis Heading -->
              <h4 class="classic-title"><span><i class="fa fa-users"></i> OSIS SMK BPI Bandung</span></h4>

              <!-- Tentang Osis -->
              <p style="text-align:justify"><img style="float:left; width:190px; height:160px; margin-right:30px;" alt="" src="<?php echo base_url();?>assets/images/osis_logo.png" />Selamat datang pada laman Osis SMK BPI Bandung. Osis SMK BPI Bandung adalah salah satu media atau tempat organisasi yang dimiliki SMK BPI Bandung sebagai pencetak kader pemimpin, mandiri, dan berorganisasi, serta ajang silahturahmi antar Siswa/i SMK BPI Bandung.</p>
              <p style="text-align:justify">OSIS SMK BPI Bandung mempunyai banyak sekali agenda acara yang dimiliki dan dirancang tiap tahunnya agar membuat Siswa/i SMK BPI Bandung berperan aktif dalam organisasi Internal maupun External.</p>
            </div>

            <div class="col-md-5" style="margin-top:37px;">

              <!-- Start Touch Slider -->
              <div class="touch-slider" data-slider-navigation="true" data-slider-pagination="true">
                <div class="item"><img alt="" src="<?php echo base_url();?>assets/images/osis3.jpg"></div>
                <div class="item"><img alt="" src="<?php echo base_url();?>assets/images/osis2.jpg"></div>
                <div class="item"><img alt="" src="<?php echo base_url();?>assets/images/osis1.jpg"></div>
                <div class="item"><img alt="" src="<?php echo base_url();?>assets/images/osis4.png"></div>
              </div>
              <!-- End Touch Slider -->

            </div>
			</div>
	
		  <!-- Visi Misi Osis -->
          <h4 class="classic-title" style="margin-top:20px;"><span><i class="fa fa-calendar-check-o"></i> Visi Misi OSIS SMK BPI</span></h4>

          <!-- Visi -->
				<p style="margin-left:30px;">
					<b>Visi</b>
					<div class="row" style="margin-left:30px;">
						<div class="col-md-6">
						  <ul class="icons-list" >
							<li><i class="fa fa-check-circle"></i> Menjadikan siswa/siswi SMK BPI lebih Disiplin, Kreatif, Berahlak mulia dan memiliki Budi Pekerti yang baik.</li>
						  </ul>
						</div>
					</div>
				</p>
				<p style="margin-left:30px;">
					<b>Misi</b>
					<div class="row" style="margin-left:30px;">
						<div class="col-md-6">
						  <ul class="icons-list" >
							<li><i class="fa fa-check-circle"></i> Menumbuh kembangkan keimanan dan ketakwaan kepada Tuhan YME.</li>
							<li><i class="fa fa-check-circle"></i> Meningkatkan kesadaran siswa/siswi yang taat aturan dan disiplin.</li>
							<li><i class="fa fa-check-circle"></i> Menumbuhkan rasa kekeluargaan antar siswa.</li>
							<li><i class="fa fa-check-circle"></i> Mengembangkan bakat, minat dan potensi siswa.</li>
						  </ul>
						</div>
					</div>
				</p>
          <!-- End Visi Misi Osis -->
		  
		  <!-- Visi Misi Osis -->
          <h4 class="classic-title" style="margin-top:30px;"><span><i class="fa fa-street-view"></i> Struktur Organisasi OSIS SMK BPI Periode 2019-2020</span></h4>
			<table style="margin-left:20px; font-size:15px;">
				<tr height="30px">
					<td width="190px"><b>- Kepala Sekolah</b></td>
					<td width="20px">: </td>
					<td style="color:#000080">Dedi Indrayana, S.Pd., M.Si.</td>
				</tr>
				<tr height="30px">
					<td><b>- Wakasek Kesiswaan</b></td>
					<td>: </td>
					<td style="color:#000080">Ade Aso, S.Pd.</td>
				</tr>
				<tr height="30px">
					<td><b>- Pembina OSIS</b></td>
					<td>: </td>
					<td style="color:#000080">Andry Refianto</td>
				</tr>
				<tr height="30px">
					<td><b>- Ketua Osis</b></td>
					<td>: </td>
					<td style="color:#000080">Zidan Andhika Hidayat</td>
					<td width="20px;"></td>
					<td>Kelas XI TKJ</td>
				</tr>
				<tr height="30px">
					<td width="120px"><b>- Wakil Ketua Osis</b></td>
					<td width="20px">: </td>
					<td style="color:#000080">Stifan Hilfa</td>
					<td width="20px;"></td>
					<td>Kelas X RPL</td>
				</tr>
				<tr height="30px">
					<td width="120px"><b>- Sekretaris</b></td>
					<td width="20px">: </td>
					<td style="color:#000080">Feni Febri Maharani</td>
					<td width="20px;"></td>
					<td>Kelas XI OTKP</td>
				</tr>
				<tr height="30px">
					<td width="120px"><b>- Wakil Sekretaris</b></td>
					<td width="20px">: </td>
					<td style="color:#000080">Tresnawati Mega Putri</td>
					<td width="20px;"></td>
					<td>Kelas X OTKP</td>
				</tr>
				<tr height="30px">
					<td width="120px"><b>- Bendahara</b></td>
					<td width="20px">: </td>
					<td style="color:#000080">Shinta Wulan Sari</td>
					<td width="20px;"></td>
					<td>Kelas XI OTKP</td>
				</tr>
				<tr height="30px">
					<td width="120px"><b>- Wakil Bendahara</b></td>
					<td width="20px">: </td>
					<td style="color:#000080">Nayla Syaleza</td>
					<td width="20px;"></td>
					<td>Kelas X OTKP</td>
				</tr>
				<tr height="110px">
					<td width="120px"><b>- Sekbid Keimanan dan Ketaqwaan terhadap Tuhan Yang Maha Esa</b></td>
					<td width="20px">: </td>
					<td style="color:#000080">
						<ul>
							<li>Salma Salsa Bila</li>
							<li>Nazma Tsania A.</li>
							<li>Nanda Farell A.</li>
						</ul>
					</td>
					<td width="20px;"></td>
					<td>
						<ul>
							<li>Kelas XI OTKP</li>
							<li>Kelas XI OTKP</li>
							<li>Kelas X RPL</li>
						</ul>
					</td>
				</tr>
				<tr height="110px">
					<td width="120px"><b>- Sekbid Pembinaan Kepribadian dan Budi Pekerti Luhur</b></td>
					<td width="20px">: </td>
					<td style="color:#000080">
						<ul>
							<li>Rizki Hendriawan</li>
							<li>Akbar Ramadhan K.</li>
							<li>M. Fadel Wijaya B.</li>
						</ul>
					</td>
					<td width="20px;"></td>
					<td>
						<ul>
							<li>Kelas XI TKJ</li>
							<li>Kelas X RPL</li>
							<li>Kelas X RPL</li>
						</ul>
					</td>
				</tr>
				<tr height="110px">
					<td width="120px"><b>- Sekbid Pembinaan Pendidikan Pendahuluan Bela Negara</b></td>
					<td width="20px">: </td>
					<td style="color:#000080">
						<ul>
							<li>Jessica Yolanda T.</li>
							<li>Elwy Jodaliano</li>
						</ul>
					</td>
					<td width="20px;"></td>
					<td>
						<ul>
							<li>Kelas XI OTKP</li>
							<li>Kelas X TKJ</li>
						</ul>
					</td>
				</tr>
				<tr height="110px">
					<td width="120px"><b>- Sekbid Prestasi Akademik</b></td>
					<td width="20px">: </td>
					<td style="color:#000080">
						<ul>
							<li>Fabian</li>
							<li>Marisa Mardiani A.</li>
							<li>Rika Pujianti</li>
						</ul>
					</td>
					<td width="20px;"></td>
					<td>
						<ul>
							<li>Kelas XI RPL</li>
							<li>Kelas X OTKP</li>
							<li>Kelas X OTKP</li>
						</ul>
					</td>
				</tr>
				<tr height="110px">
					<td width="120px"><b>- Sekbid Pembinaan Berorganisasi Pendidikan Politik dan Kepemimpinan</b></td>
					<td width="20px">: </td>
					<td style="color:#000080">
						<ul>
							<li>Syifa Azzahra</li>
							<li>Amelia Frisca</li>
							<li>Salman Ahmad R.</li>
						</ul>
					</td>
					<td width="20px;"></td>
					<td>
						<ul>
							<li>Kelas XI OTKP</li>
							<li>Kelas X OTKP</li>
							<li>Kelas X TKJ</li>
						</ul>
					</td>
				</tr>
				<tr height="110px">
					<td width="120px"><b>- Sekbid Pembinaan Keterampilan dan Kewirausahaan</b></td>
					<td width="20px">: </td>
					<td style="color:#000080">
						<ul>
							<li>Gespan Fayyadh O.</li>
							<li>Faishal Saefur R.</li>
							<li>Tanisa Riyaturahma</li>
						</ul>
					</td>
					<td width="20px;"></td>
					<td>
						<ul>
							<li>Kelas XI RPL</li>
							<li>Kelas XI RPL</li>
							<li>Kelas XI OTKP</li>
						</ul>
					</td>
				</tr>
				<tr height="110px">
					<td width="120px"><b>- Sekbid Pembinaan Jasmani dan Daya Kreasi</b></td>
					<td width="20px">: </td>
					<td style="color:#000080">
						<ul>
							<li>Yogi Harta</li>
							<li>Azhar M. Ar-Rafi</li>
							<li>M. Jeril Mutawari</li>
						</ul>
					</td>
					<td width="20px;"></td>
					<td>
						<ul>
							<li>Kelas XI TKJ</li>
							<li>Kelas XI TKJ</li>
							<li>Kelas X RPL</li>
						</ul>
					</td>
				</tr>
				<tr height="110px">
					<td width="120px"><b>- Sekbid Pembinaan Persepsi, Apresiasi dan Daya Kreasi Seni</b></td>
					<td width="20px">: </td>
					<td style="color:#000080">
						<ul>
							<li>Merlin Rosmayanti</li>
							<li>Marvel Ravindra D.</li>
							<li>Lusyana Rayhan V.</li>
						</ul>
					</td>
					<td width="20px;"></td>
					<td>
						<ul>
							<li>Kelas XI OTKP</li>
							<li>Kelas X TKJ</li>
							<li>Kelas X OTKP</li>
						</ul>
					</td>
				</tr>
			</table>
		
			<h4 class="classic-title" style="margin-top:30px;"><span><i class="fa fa-male"></i><i class="fa fa-female"></i> Tentang OSIS SMK BPI</span></h4>
			
			<div class="member-socail">
			      <strong>Ikuti Kegiatan Kami di Akun: </strong>
                    <a href="https://www.instagram.com/osissmkbpibdg/"><i class="fa fa-instagram"></i> @osissmkbpibdg </a>
                  
            </div>

          <div class="row" style="">
			
            <!-- Ketua Osis -->
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="team-member">
                <div class="member-photo" style="margin-top:60px; width:400px;">
                  <img alt="" src="<?php echo base_url();?>assets/images/osis_zidan.png" />
                  <div class="member-name">Zidan Andhika H. (XI TKJ) <span>Ketua Osis</span></div>
                </div>
                <!-- Ketua Osis Social Links 
                <div class="member-socail">
                  <a class="twitter" href="https://twitter.com/alifgufron_"><i class="fa fa-twitter"></i></a>
                  <a class="twitter" href="https://www.facebook.com/alif.gupron"><i class="fa fa-facebook"></i></a>
				  <a class="mail" href="https://plus.google.com/u/0/117257350640643604863"><i class="fa fa-envelope"></i></a>
                  <a class="flickr" href="https://www.instagram.com/alifgufron/"><i class="fa fa-instagram"></i></a>
                </div> -->
              </div>
            </div>
          </div>
		  <div class="row" style="margin-top:60px;">

            <!-- Ketua Osis -->
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="team-member">
                <div class="member-photo" style="width:400px;">
                  <img alt="" src="<?php echo base_url();?>assets/images/osis_stifan.png" />
                  <div class="member-name">Stifan Hilfa (X RPL) <span>W. Ketua Osis</span></div>
                </div>
                <!-- Ketua Osis Social Links 
                <div class="member-socail">
                  <a class="twitter" href="#"><i class="fa fa-twitter"></i></a>
                  <a class="twitter" href="#"><i class="fa fa-facebook"></i></a>
                  <a class="mail" href="#"><i class="fa fa-envelope"></i></a>
                  <a class="flickr" href="https://www.instagram.com/lizashopia/"><i class="fa fa-instagram"></i></a>
                </div> -->
              </div>
            </div>
          </div>	
         
			</div>
		</div>
	</div>