    <div id="content">

      <div class="container">

        <div class="page-content">

			<!-- <h4 class="classic-title"><span><i class="fa fa-trophy"></i> Prestasi yang diraih siswa SMK BPI</span></h4> -->

			<div class="big-title text-center">
			  <h1>Prestasi Siswa/i <strong>SMK BPI</strong></h1>
			  <p class="title-desc">Bermartabat, Berkualitas, Terpercaya dan Berprestasi</p>
			</div>

          	<div class="row" style="margin-top:20px; ">
			

	            <!-- Ketua Osis -->

	            <div class="col-md-4 col-sm-6 col-xs-12" style="margin-top:20px; ">

	              <div class="team-member">

	                <div class="member-photo">

	                  <img alt="" style="width:400px; height:250px" src="<?php echo base_url();?>assets/images/prestasi/prestasi_lombajaringan.jpg" />

	                  <div class="member-name">Juara 1 - Olimpiade Jaringan Mikrotik <span>2016</span></div>

	                </div>

	              </div>

	            </div>


		        <div class="col-md-4 col-sm-6 col-xs-12" style="margin-top:20px; ">

		            <div class="team-member">

		                <div class="member-photo">

		                  	<img alt="" style="width:400px; height:250px" src="<?php echo base_url();?>assets/images/prestasi/prestasi_lombaPUPR2.jpg" />

		                  	<div class="member-name">Juara 1 - Karya Ilmiah Nasional<span>2018</span></div>

		                </div>

		            </div>

		        </div>


	        	<div class="col-md-4 col-sm-6 col-xs-12" style="margin-top:20px; ">

		            <div class="team-member">

		                <div class="member-photo">

		                  	<img alt="" style="width:400px; height:250px" src="<?php echo base_url();?>assets/images/prestasi/KI2019.jpg" />

		                  	<div class="member-name">Juara 3 - Karya Ilmiah Nasional<span>2019</span></div>

		                </div>

	              	</div>

	            </div>

	            <div class="col-md-4 col-sm-6 col-xs-12" style="margin-top:20px; ">

		            <div class="team-member">

		                <div class="member-photo">

		                  	<img alt="" style="width:400px; height:250px" src="<?php echo base_url();?>assets/images/prestasi/duta-mixagrip.jpg" />

		                  	<div class="member-name">Juara 1 - Duta Mixagrip <span>2019</span></div>

		                </div>

	              	</div>

	            </div>

	            <div class="col-md-4 col-sm-6 col-xs-12" style="margin-top:20px; ">

		            <div class="team-member">

		                <div class="member-photo">

		                  	<img alt="" style="width:400px; height:250px" src="<?php echo base_url();?>assets/images/prestasi/fotokontes-mixagrip.jpg" />

		                  	<div class="member-name">Juara 1 - Foto Kontes Mixagrip<span>2019</span></div>

		                </div>

	              	</div>

	            </div>

	            <div class="col-md-4 col-sm-6 col-xs-12" style="margin-top:20px; ">

		            <div class="team-member">

		                <div class="member-photo">

		                  	<img alt="" style="width:400px; height:250px" src="<?php echo base_url();?>assets/images/prestasi/webdesign.jpg" />

		                  	<div class="member-name">Juara 2 - Web Design<span>2018</span></div>

		                </div>

	              	</div>

	            </div>

	            <div class="col-md-4 col-sm-6 col-xs-12" style="margin-top:20px; ">

		            <div class="team-member">

		                <div class="member-photo">

		                  	<img alt="" style="width:400px; height:250px" src="<?php echo base_url();?>assets/images/prestasi/gemaramadhan.jpg" />

		                  	<div class="member-name">Juara 2 - Gema Ramadhan<span>2019</span></div>

		                </div>

	              	</div>

	            </div>

	            <div class="col-md-4 col-sm-6 col-xs-12" style="margin-top:20px; ">

		            <div class="team-member">

		                <div class="member-photo">

		                  	<img alt="" style="width:400px; height:250px" src="<?php echo base_url();?>assets/images/prestasi/webdevelopment.png" />

		                  	<div class="member-name">Juara 2 - Web Development<span>2019</span></div>

		                </div>

	              	</div>

	            </div>

	        </div>


		</div>
	</div>
</div>