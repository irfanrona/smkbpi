<!doctype html>

	<!-- Start Content -->
	<div id="content">
		<div class="container">
		<div class="page-content">
			<!-- Start Full Width Section 3 -->
			<div class="section" style="padding-top:20px; padding-bottom:60px; border-top:0; border-bottom:0; background:#fff;">
			<div class="container">

				<!-- Start Big Heading -->
				<div class="big-title text-center">
				<h1>VISI & MISI <strong>SMK BPI Bandung</strong></h1>
				</div>
				<!-- End Big Heading -->

				<!-- Some Text -->
				<p class="text-center">Visi dan Misi SMK demi terwujudnya Sekolah yang Bermartabat, Berkualitas dan Terpercaya.</p>
				
				<!-- Divider -->
				<div class="hr1" style="margin-bottom:15px;"></div>
				
				
				
				<!-- Start Recent Projects Carousel -->
				<ul id="portfolio-list" data-animated="fadeIn">
				<li>
					<img src="<?php echo base_url();?>assets/images/smk2.jpg" alt="" />
					<div class="portfolio-item-content">
					<span class="header">Visi SMK BPI Bandung</span>
					<p class="body">Menjadikan penyelenggara pendidikan Bermartabat, berkualitas dan terpercaya.</p>
					</div>
					<a href="#"></a>

				</li>
				<li>
					<img src="<?php echo base_url();?>assets/images/smk3.jpg" alt="" />
					<div class="portfolio-item-content">
					<span class="header">Misi SMK BPI Bandung</span>
					<p class="body">Menyalurkan dan Mendukung kreativitas peserta didik dengan sarana dan prasarana yang lengkap.</p>
					</div>
					<a href="#"></a>

				</li>
				<li>
					<img src="<?php echo base_url();?>assets/images/smk4.jpg" alt="" />
					<div class="portfolio-item-content">
					<span class="header">Misi SMK BPI Bandung</span>
					<p class="body">Mewujudkan budaya religi, jujur, disiplin, beretika, berestetika, pekerja keras, kreatif, inovatif, komptetitif, dan berkualitas</p>
					</div>
					<a href="#"></a>

				</li>
			</ul>


			</div>
			</div>
			
			<!-- Classic Heading -->
			<h4 class="classic-title"><span><i class="fa fa-th-list"></i> Visi dan Misi SMK BPI Bandung</span></h4>

			<!-- Keunggulan -->
				<p>
					<div class="big-title">
						<h1>Visi <strong>SMK BPI Bandung</strong></h1>
					</div>
					<div class="row">
						<div class="col-md-6">
							<ul class="icons-list" >
							<li><i class="fa fa-check-circle"></i> Menjadikan penyelenggara pendidikan berkualitas dan terpercaya.</li>
							</ul>
						</div>
					</div>
				</p>
				
				<!-- Classic Heading -->
			<h4 class="classic-title"></h4>
			
				<p>
					<div class="big-title">
						<h1>Misi <strong>SMK BPI Bandung</strong></h1>
					</div>
					<div class="row">
						<div class="col-md-6">
							<ul class="icons-list" >
							<li><i class="fa fa-check-circle"></i> Mewujudkan tata kelola, sistem pengendalian manajemen, dan sistem.pengawasan internal yang modern, efektif, dan efesien</li>
							<li><i class="fa fa-check-circle"></i> Menyalurkan dan Mendukung kreativitas peserta didik dengan sarana dan prasarana yang lengkap.</li>
							<li><i class="fa fa-check-circle"></i> Mewujudkan budaya religi, jujur, disiplin, beretika, berestetika, pekerja keras, kreatif, inovatif, komptetitif, dan berkualitas.</li>
							<li><i class="fa fa-check-circle"></i> Mewujudkan dinamisasi peningkatan kualitas pendidikan barkarakter yang berkesinambungan dan berkelanjutan.</li>
							<li><i class="fa fa-check-circle"></i> Mewujudkan produk kompetensi keahlian bernilai Jual Pasar Global.</li>
							<li><i class="fa fa-check-circle"></i> Memperluas akses kemitraan dunia kerja yang menjamin lapangan kerja dan prakerin bagi peserta didik dan lulusan SMK BPI.</li>
							<li><i class="fa fa-check-circle"></i> Mewujudkan lulusan yang handal di bidangnya dan fasih berbahasa Inggris sehingga dipercaya oleh segenap dunia kerja pemerintah maupun swasata.</li>
							<li><i class="fa fa-check-circle"></i> Mewujudkan jiwa entrepreneurshing kuat yang mampu meningkatkan kualitas hidup civitas akademika SMK BPI Bandung.</li>
							</ul>
						</div>
					</div>
				</p>
		</div>
		</div>
	</div>
	<!-- End Content -->