<body>

    <div id="container">

    <div class="hidden-header"></div>

    <header class="clearfix">

        <!-- Tampilan Atas Bar -->

        <div class="top-bar">

        <div class="container">

            <div class="row">

            <div class="col-md-7">

                <!-- Tampilan Info Kontak -->

                <ul class="contact-details">

                <li><i class="fa fa-map-marker"></i> Jalan Burangrang No. 8 Bandung 40262

                </li>

                <li><a href="#" style="color:black;" ><i class="fa fa-envelope-o"></i> info@smkbpi.sch.id</a>

                </li>

                <li><a href="#" style="color:black;"><i class="fa fa-phone"></i> 022 7301739 - 7305735</a>

                </li>

                </ul>

            </div>

            <div class="col-md-5">

                <!-- Media Sosial Sekolah -->

                <ul class="social-list">

                <li>

                    <a class="facebook itl-tooltip" data-placement="bottom" title="Facebook" href="http://www.facebook.com/SmkBpiBandung"><i class="fa fa-facebook"></i></a>

                </li>

                <li>

                    <a class="twitter itl-tooltip" data-placement="bottom" title="Twitter" href="http://www.twitter.com/smkbpibdg"><i class="fa fa-twitter"></i></a>

                </li>


                <li>

                    <a class="instgram itl-tooltip" data-placement="bottom" title="Instagram" href="http://www.instagram.com/smkbpibandung"><i class="fa fa-instagram"></i></a>

                </li>

                <li>

                    <a class="youtube itl-tooltip" data-placement="bottom" title="Youtube" href="https://www.youtube.com/channel/UCVNA2DSrZSwGBdFrxouP5pQ"><i class="fa fa-youtube"></i></a>

                </li>

                <?php if($this->session->userdata('is_logged_in') != TRUE){ ?>

                    <li>

                        <a data-placement="bottom" title="Login" href="<?= base_url('home/login') ?>"><i class="fa fa-sign-in"></i></a>

                    </li>

                <?php }else{ ?>

                    <li>

                        <a data-placement="bottom" title="Logout" href="javascript:;" onClick="logout()"><i class="fa fa-sign-in"></i></a>

                    </li>

                <?php } ?>

                </ul>

            </div>

            </div>

        </div>

        </div>



        <!-- Start  Logo Sekolah dan Menu  -->

        <div class="navbar navbar-default navbar-top">

        <div class="container">

            <div class="navbar-header">

            <!-- Untuk Tampilan Mobile/HP -->

            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">

                <i class="fa fa-bars"></i>

            </button>

            <?php if($this->session->userdata('is_logged_in') != TRUE){ ?>

                <a class="navbar-brand" href="<?php echo site_url();?>">

                        <div class="imgR">

                            <img alt=""  src="<?php echo base_url();?>assets/images/smk.png">

                        </div>

                    </a>

            <?php }else{ ?>

                <a class="navbar-brand" href="<?php echo site_url('adminController');?>">

                        <div class="imgR">

                            <img alt=""  src="<?php echo base_url();?>assets/images/smk.png">

                        </div>

                    </a>

            <?php } ?>

            </div>

            <div class="navbar-collapse collapse">

            <!-- Tampilan Search 

            <div class="search-side">

                <a class="show-search"><i class="fa fa-search"></i></a>

                <div class="search-form">

                <form autocomplete="off" role="search" method="get" class="searchform" action="#">

                    <input type="text" value="" name="s" id="s" placeholder="Silahkan Cari...">

                </form>

                </div>

            </div>-->

            <!-- Tampilan Menu -->

            <?php $uri=$this->uri->segment(2);

             if($this->session->userdata('level_user') == 1){ ?>

                <ul class="nav navbar-nav navbar-right">

                    <li>

                        <a class="active" href="<?php echo site_url('adminController/dashboard')?>">Dashboard</a>

                    </li>

                    <li>

                        <a class="<?php if($uri=='logout'){echo 'active';}?>" href="javascript:;" onClick="logout()">Logout</a>

                    </li>

                </ul>

                <?php }else{ ?>

                    <ul class="nav navbar-nav navbar-right">

                        <li>

                            <a class="<?php if($uri==''){echo 'active';}?>" href="<?php echo site_url()?>">Beranda</a>

                        </li>

                        <li>

                            <a class="<?php if($uri=='profil' or $uri=='visi' or $uri=='osis' or $uri=='prestasi' or $uri=='ekskul' or $uri=='up'){echo 'active';}?>" href="<?php echo site_url('home/profil')?>">Profil</a>

                            <ul class="dropdown">

                                <li><a href="<?php echo site_url('home/profil')?>">Profil Umum SMK BPI</a></li>

                                <li><a href="<?php echo site_url('home/visi')?>">Visi Misi SMK BPI</a></li>

                                <li><a href="<?php echo site_url('home/osis')?>">OSIS SMK BPI</a></li>

				                <li><a href="<?php echo site_url('home/prestasi')?>">Prestasi</a></li>

                                <li><a href="<?php echo site_url('home/ekskul')?>">Ekstrakurikuler</a></li>
                                
                                <li><a href="http://www.bpiedu.id/" target="_blank" rel="nofollow">Yayasan BPI Bandung</a></li>

                                <!--
                                <li><a href="<?php echo site_url('home/improvement')?>">Unit Produksi</a></li>
                                -->

                            </ul>

                        </li>

                        <li>

                        <a class="<?php if($uri=='program' or $uri=='otkp' or $uri=='tkj' or $uri=='rpl'){echo 'active';}?>" href="#">Program Studi</a>

                        <ul class="dropdown">

                            <li><a href="<?php echo site_url('home/otkp')?>">Otomatisasi dan Tata Kelola Perkantoran</a>

                            </li>

                            <li><a href="<?php echo site_url('home/rpl')?>">Rekayasa Perangkat Lunak</a>

                            </li>

                            <li><a href="<?php echo site_url('home/tkj')?>">Teknik Komputer dan Jaringan</a>

                            </li>

                        </ul>

                        </li>

                        <li>

                        <a class="<?php if($uri=='agenda' or $uri=='galeri'){echo 'active';}?>" href="<?php echo site_url('home/agenda')?>">Agenda</a>

                        <ul class="dropdown">

                            <li><a href="<?php echo site_url('home/agenda')?>">Agenda Acara</a>

                            </li>

                            <li><a href="<?php echo site_url('home/galeri')?>">Galeri</a>

                            </li>

                        </ul>

                        </li>

                        <li>

                        <a class="<?php if($uri=='pegawai'){echo 'active';}?>" href="<?php echo site_url('home/pegawai')?>">Kepegawaian</a>

                        </li>
                        
                         <li>

                            <a class="<?php if($uri=='ppdb'){echo 'active';}?>" href="<?php echo site_url('home/ppdb')?>">PPDB Online</a>
                            
                            <!-- <ul class="dropdown">
    
                                <li><a href="<?php echo site_url('home/daftar')?>">Formulir Pendaftaran</a>
    
                                </li>
    
                            </ul> -->

                        </li>
                       

                        <!-- <li><a href="http://tryoutsmp.smkbpi.sch.id/">Try Out Online</a>

                        </li> -->

                    </ul>

            <?php } ?>

            <!-- End Navigation List -->

            </div>

        </div>



        <!-- Mobile Menu Start -->

        <?php if($this->session->userdata('level_user') == 1){?>

        <ul class="wpb-mobile-menu">

            <li>

                <a class="active" href="<?= site_url('adminController/dashboard');?>">Dashboard</a>

            </li>

            <li>

                <a href="javascript:;" onClick="logout()">Logout</a>

            </li>

        </ul>

        <?php }else{ ?>

        <!-- Mobile Menu Start -->

        <ul class="wpb-mobile-menu">

            <li>

            <a class="<?php if($uri==''){echo 'active';}?>" href="<?php echo site_url();?>">Beranda</a>

            </li>

            <li>

            <a class="<?php if($uri=='profil' or $uri=='visi' or $uri=='osis' or $uri=='prestasi' or $uri=='ekskul' or $uri=='up'){echo 'active';}?>" href="<?php echo site_url('home/profil');?>">Profil</a>

            <ul class="dropdown">

                <li><a href="<?php echo site_url('home/profil');?>">Profil Umum</a>

                </li>

                <li><a href="<?php echo site_url('home/visi');?>">Visi Misi</a>

                </li>

                <li><a href="<?php echo site_url('home/osis');?>">OSIS SMK BPI</a>

                </li>

		        <li><a href="<?php echo site_url('home/prestasi');?>">Prestasi</a>

                </li>

                <li><a href="<?php echo site_url('home/ekskul');?>">Ekstrakurikuler</a>

                </li>
                
                <li><a href="http://www.bpiedu.id/" target="_blank" rel="nofollow">Yayasan BPI Bandung</a>
                
                </li>

                <!--
                <li><a href="<?php echo site_url('home/up');?>">Unit Produksi</a>

                </li>
                -->

            </ul>

            </li>

            <li>

            <a class="<?php if($uri=='program' or $uri=='otkp' or $uri=='tkj' or $uri=='rpl'){echo 'active';}?>" href="#">Program Studi</a>

            <ul class="dropdown">

                <li><a href="<?php echo site_url('home/otkp');?>">Otomatisasi dan Tata Kelola Perkantoran</a>

                </li>

                <li><a href="<?php echo site_url('home/rpl');?>">Rekayasa Perangkat Lunak</a>

                </li>

                <li><a href="<?php echo site_url('home/tkj');?>">Teknik Komputer Jaringan</a>

                </li>

            </ul>

            </li>

            <li>

                <a class="<?php if($uri=='agenda' or $uri=='galeri'){echo 'active';}?>" href="<?php echo site_url('home/agenda')?>">Agenda</a>

                <ul class="dropdown">

                    <li><a href="<?php echo site_url('home/agenda')?>">Agenda Acara</a>

                    </li>

                    <li><a href="<?php echo site_url('home/galeri')?>">Galeri</a>

                    </li>

                </ul>

            </li>

            <li>

            <a class="<?php if($uri=='pegawai'){echo 'active';}?>" href="<?php echo site_url('home/pegawai');?>">Kepegawaian</a>

            </li>
            
            <li>
            
                <a class="<?php if($uri=='ppdb'){echo 'active';}?>" href="<?php echo site_url('home/ppdb')?>">PPDB Online</a>
                
                <ul class="dropdown">
    
                    <li><a href="<?php echo site_url('home/daftar');?>">Formulir Pendaftaran</a>
    
                    </li>
    
                </ul>

            </li>

            <!--<li>

            <a href="http://tryoutsmp.smkbpi.sch.id/" target="_blank">Try Out Online</a>

            </li>-->

        </ul>

        <?php }?>

        </div>

    </header>

    <script>

        function logout() {

            Swal.fire({

                title: 'Anda yakin ingin keluar?',

                type: 'warning',

                showCancelButton: true,

                confirmButtonColor: '#3085d6',

                cancelButtonColor: '#d33',

                confirmButtonText: 'Keluar!'

            }).then((result) => {

                if (result.value) {

                    window.location = base_url + 'adminController/logout'

                }

            })

        }
        
        
    </script>
    