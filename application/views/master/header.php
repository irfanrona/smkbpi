<!doctype html>

<html lang="en">

	<head>

	  <!-- Judul Website -->

	  <title><?= $title ?></title>



	  <!-- Charset -->

	  <meta charset="utf-8 ">

	  

	  <!-- Logo Icon Website -->

	  <link href="<?php echo base_url('assets/images/logo.png'); ?>" rel='icon' type='image/x-icon'/>

	  

	  <!-- Responsive Metatag -->

	  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">



	  <!-- Deskripsi -->

	  <meta name="deskripsi" content="SMK BPI Bandung">



	  <!-- Bootstrap CSS  -->

	  <link rel="stylesheet" href="<?php echo base_url();?>assets/asset/css/bootstrap.min.css" type="text/css" media="screen">



	  <!-- CSS untuk Font -->

	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css" media="screen">



	  <!-- CSS untuk Slicknav -->

	  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/slicknav.css" media="screen">



	  <!-- CSS style dari MARGO  -->

	  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css" media="screen">



	  <!-- Responsive CSS -->

	  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/responsive.css" media="screen">



	  <!-- Css3 Transitions Styles  -->

	  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/animate.css" media="screen">



	  <!-- Color CSS Styles  -->

	  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/colors/yellow.css" title="yellow" media="screen" />

	  

	  <!-- Custom CSS Styles  -->

	  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/custom.css">

	  <style>

		  /* Icon next&prev datepicker */

			.ui-datepicker-prev span.ui-icon,

			.ui-datepicker-next span.ui-icon {

				background-image: url("https://cdn0.iconfinder.com/data/icons/flat-round-arrow-arrow-head/512/Red_Arrow_Right-512.png");

				background-size: contain;

				background-position: center;

			}



			.ui-datepicker-prev span.ui-icon {

				transform: rotate(180deg);

			}



			.ui-datepicker-prev span.ui-icon:focus,

			.ui-datepicker-next span.ui-icon:focus {

				outline: none !important;

			}

	  </style>

	  <!-- dataTables CSS Styles  -->

	  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/dataTables.bootstrap4.min.css">

	  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/jquery-ui.css">

	  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/sweetalert2.min.css">

	  



	  <!-- Margo JS  -->

	  <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-2.1.4.min.js"></script>

	  <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.migrate.js"></script>

	  <script type="text/javascript" src="<?php echo base_url();?>assets/js/modernizrr.js"></script>

	  <script type="text/javascript" src="<?php echo base_url();?>assets/asset/js/bootstrap.min.js"></script>

	  <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.fitvids.js"></script>

	  <script type="text/javascript" src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script>

	  <script type="text/javascript" src="<?php echo base_url();?>assets/js/nivo-lightbox.min.js"></script>

	  <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.isotope.min.js"></script>

	  <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.appear.js"></script>

	  <script type="text/javascript" src="<?php echo base_url();?>assets/js/count-to.js"></script>

	  <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.textillate.js"></script>

	  <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.lettering.js"></script>

	  <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.easypiechart.min.js"></script>

	  <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.nicescroll.js"></script>

	  <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.parallax.js"></script>

	  <script type="text/javascript" src="<?php echo base_url();?>assets/js/mediaelement-and-player.js"></script>

	  <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.slicknav.js"></script>

	  <!-- <script type="text/javascript" src="<?php echo base_url();?>assets/js/angularjs.js"></script> -->

	  <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-xpander.plugin.js"></script>

	  <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.dataTables.min.js"></script>

	  <script type="text/javascript" src="<?php echo base_url();?>assets/js/sweetalert2.min.js"></script>

		<script>

        var base_url = '<?= base_url() ?>'

    </script>

	</head>