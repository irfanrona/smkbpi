</body>

<!-- Tampilan Bawah/ Footer -->

    <footer>

        <div class="container">

            <div class="row footer-widgets">

                

                <!-- Footer Info Sekolah -->

                <div class="col-md-3 col-xs-12">

                    <div class="footer-widget contact-widget">

                        <h4><img src="<?= base_url(); ?>assets/images/smk_footer.png" class="img-responsive" alt="Footer Logo" style="height:110px; width:220px; margin-bottom:-35px; margin-top:-20px;"/></h4>

                        <p><i class="fa fa-map-marker"></i> Jl. Burangrang No. 8 Bandung</p>

                        <ul>

                            <li><span>Telepon:</span> (022) 7301739 - 7305735</li>

                            <li><span>Email:</span> info@smkbpi.sch.id</li>

                            <li><span>Fax:</span> (022) 7305835</li>

                            <li><span>Website:</span> www.smkbpi.sch.id</li>

                        </ul>

                    </div>

                </div>

                

                <!-- Media Sosial Widget -->

                <div class="col-md-3 col-xs-12">

                    <div class="footer-widget social-widget">

                        <h4>Media Sosial SMK BPI <i class="fa fa-comments-o"></i><span class="head-line"></span></h4>

                        <ul class="social-icons">

                            <li>

                                <a class="facebook" href="http://www.facebook.com/SmkBpiBandung"><i class="fa fa-facebook"></i></a>

                            </li>

                            <li>

                                <a class="twitter" href="http://www.twitter.com/smkbpibdg"><i class="fa fa-twitter"></i></a>

                            </li>

                            <li>

                                <a class="instgram" href="http://www.instagram.com/smkbpibandung"><i class="fa fa-instagram"></i></a>

                            </li>

                            <li>

                                <a class="youtube" href="https://www.youtube.com/channel/UCVNA2DSrZSwGBdFrxouP5pQ"><i class="fa fa-youtube"></i></a>

                            </li>

                        </ul>

                    </div>

                </div>

                

                <div class="col-md-3 col-xs-12">

                    <div class="footer-widget twitter-widget">

                        <h4>Download  <i class="fa fa-download"></i><span class="head-line"></span></h4>

                        <ul>
                            
                            <!--
                            <li>

                                <a href="#" style="color:#000080;">Kurikulum Administrasi Perkantoran</a> 

                            </li>

                            <li>

                                <a href="#" style="color:#000080;">Kurikulum Rekayasa Perangkat Lunak</a>

                            </li>

                            <li>

                                <a href="#" style="color:#000080;">Silabus Pemrograman Web Dinamis</a>

                            </li>  
                            -->
                            
                            <li>

                                <a href="<?php echo base_url();?>assets/file/bootstrap.rar" style="color:#000080; font-size:14px;;">Pembelajaran Bootstrap</a> 

                            </li>

                            <li>

                                <a href="<?php echo base_url();?>assets/file/xampp.exe" style="color:#000080; font-size:14px;;">XAMPP</a> 

                            </li>

                            <!-- <li>

                                <a href="<?php echo base_url();?>assets/file/framework_login.rar" style="color:#000080; font-size:14px;;">Pembelajaran Login</a> 

                            </li> -->

                            <!-- <li>

                                <a href="<?php echo base_url();?>assets/file/Soal_UAS_PWD.pdf" style="color:#000080; font-size:14px;;">UAS PWD XI RPL</a> 

                            </li> -->

                        </ul>

                    </div>

                </div>

                

                <div class="col-md-3 col-xs-12">

                    <div class="footer-widget twitter-widget">

                        <h4>Lokasi  <i class="fa fa-map-marker"></i><span class="head-line"></span></h4>

                        

                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3960.71869822923!2d107.6176261!3d-6.9241913!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e68e62b720d1d17%3A0xe06423459ac57fad!2sJl.+Burangrang+No.8%2C+Burangrang%2C+Lengkong%2C+Kota+Bandung%2C+Jawa+Barat+40262!5e0!3m2!1sid!2sid!4v1480509908449" width="260px" height="200px" frameborder="0" style="border:0" allowfullscreen></iframe>

                        

                    </div>

                </div>



            </div>



            <!-- Tampilan Copyright -->

            <div class="copyright-section">

                <div class="row">

                    <div class="col-md-6">

                        <p>&copy; TIM ICT SMK BPI - All Rights Reserved <a href="http://smkbpi.sch.id">SMK BPI Bandung</a> </p>

                    </div>

                </div>

            </div>



        </div>




    <a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>



    <div id="loader">

        <div class="spinner">

            <div class="dot1"></div>

            <div class="dot2"></div>

        </div>

    </div>
    



    <script type="text/javascript" src="<?php echo base_url();?>assets/js/script.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-ui.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/js/dataTables.bootstrap.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/js/custom.js"></script>

    <script>
	$('#StartDate, #EndDate, .date').datepicker({
        	// minDate: -60,
        	maxDate: "tomorrow",
		changeMonth: true,
        	changeYear: true
	});
    </script>
    
    <!--Start of Tawk.to Script
    <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5e8492c135bcbb0c9aac9c97/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
        })();
    </script>
    End of Tawk.to Script-->
    </footer>


    </html>