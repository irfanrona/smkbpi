<!doctype html>

			<!-- Start Content -->

			<div id="content">

			  <div class="container">

				<div class="page-content">

				   <div class="row">

						<div class="col-md-7">



						  <!-- OTKP Heading -->

						  <h4 class="classic-title"><span><i class="fa fa-th-list"></i> Otomatisasi dan Tata Kelola Perkantoran </span></h4>



						  <!-- Tentang OTKP -->

						  <p style="text-align:justify"><img style="float:left; width:150px; margin-right:30px;" alt="" src="<?php echo base_url();?>assets/images/logo_otkp.png" /><STRONG>Otomatisasi dan Tata Kelola Perkantoran</STRONG> adalah kompetensi keahlian yang membekali siswa pada kegiatan perkantoran dan ketata usahaan berbasis ICT. Menekankan kegiatan administrasi yang mahir dalam melaksanakan tugas : Sekertariat junior, Public Relation, Arsiparis, Typing, Resepsionis. dan Wirausahawan yang tangguh dengan sikap Bermartabat, Berkualitas dan Terpercaya.</p>

						  <p style="text-align:justify">Otomatisasi dan Tata Kelola Perkantoran SMK BPI merupakan salah satu Kompetensi Keahlian pada Kelompok Bidang Bisnis dan Manajemen yang membekali siswa dalam kegiatan perkantoran dan ketata usahaan berbasis ICT.  </p>


						</div>



						<div class="col-md-5" style="margin-top:37px;">



						  <!-- Start Touch Slider -->

						  <div class="touch-slider" data-slider-navigation="true" data-slider-pagination="true">

							<div class="item"><img alt="" src="<?php echo base_url();?>assets/images/otkp1.jpg"></div>
							
							<div class="item"><img alt="" src="<?php echo base_url();?>assets/images/otkp2.png"></div>
							
							<div class="item"><img alt="" src="<?php echo base_url();?>assets/images/otkp3.png"></div>

						  </div>

						  <!-- End Touch Slider -->



						</div>

						

					  </div>



					  <!-- Divider -->

					  <div class="hr1" style="margin-bottom:50px;"></div>



					  <div class="row">



						<div class="col-md-6">



						  <!-- Classic Heading -->

						  <h4 class="classic-title"><span>Kompetensi Keahlian OTKP</span></h4>



						  <div class="skill-shortcode">

								<p><b>Kompetensi keahlian OTKP SMK BPI sebagai Jurusan yang bergerak di bidang Bisnis dan Manajemen</b> yang membekali siswa dalam kegiatan perkantoran dan ketata usahaan berbasis ICT. Kompetensi ini menekankan pada kegiatan dan ruang lingkup pekerjaan administrasi meliputi :

								<div class="row">

									<div class="col-md-6">

									  <ul class="icons-list" style="margin-left:30px;">

										<li><i class="fa fa-check-circle"></i> Sekretaris Junior.</li>

										<li><i class="fa fa-check-circle"></i> Public Relations.</li>
										
										<li><i class="fa fa-check-circle"></i> Arsiparis.</li>
										
										<li><i class="fa fa-check-circle"></i> Resepsionis.</li>
										
										<li><i class="fa fa-check-circle"></i> Wirausahawan.</li>

									  </ul>

									</div>

								</div>

							  </p>

							  <p><b>Kurikulum dan Materi pembelajaran</b> pada kompetensi ini diselaraskan dengan kebutuhan Dunia Usaha dan Dunia Industri, seperti :

								<div class="row">

									<div class="col-md-8">

									  <ul class="icons-list" style="margin-left:30px;">

										<li><i class="fa fa-check-circle"></i> Penerapan Teknologi Perkantoran.</li>

										<li><i class="fa fa-check-circle"></i> Typing.</li>

										<li><i class="fa fa-check-circle"></i> Kehumasan.</li>

										<li><i class="fa fa-check-circle"></i> Penataan Surat Menyurat dan Kearsipan </li>
										
										<li><i class="fa fa-check-circle"></i> Tata Kelola Keuangan</li>

									  </ul>

									</div>

								</div>

							  </p>

							  <p><b>" Untuk keterserapan lulusan, memiliki peluang yang luas untuk dapat Bekerja, Melanjutkan Kuliah, dan Wirausaha. "</b></p>

						  </div>

						</div>



						<div class="col-md-6">



						  <!-- Classic Heading -->

						  <h4 class="classic-title"><span>Fasilitas OTKP SMK BPI</span></h4>



						  <!-- Accordion -->

						  <div class="panel-group" id="accordion">



							<!-- Start Accordion 1 -->

							<div class="panel panel-default">

							  <!-- Toggle Heading -->

							  <div class="panel-heading">

								<h4 class="panel-title">

								  <a data-toggle="collapse" data-parent="#accordion" href="#collapse-1">

									<i class="fa fa-angle-up control-icon"></i>

									<i class="fa fa-desktop"></i> Fasilitas OTKP

								  </a>

								</h4>

							  </div>

							  <!-- Toggle Content -->

							  <div id="collapse-1" class="panel-collapse collapse in">

								<div class="panel-body"><img class="img-thumbnail image-text" style="float:left; width:150px;" alt="" src="<?php echo base_url();?>assets/images/AP/fasilitasap1.jpg" />

									<p>Adapun <b>Fasilitas</b> yang ada di Jurusan Otomatisasi dan Tata Kelola Perkantoran adalah :

										<div class="row">

											<div class="col-md-6">

											  <ul class="icons-list" >

												<li><i class="fa fa-check-circle"></i> Lab Komputer.</li>

												<li><i class="fa fa-check-circle"></i> Hardware Pendukung.</li>

												<li><i class="fa fa-check-circle"></i> Alat Tulis Kantor Lengkap.</li>

												<li><i class="fa fa-check-circle"></i> Peralatan Pembelajaran Lengkap.</li>

											  </ul>

											</div>

										</div>

									</p>

								</div>

							  </div>

							</div>

							<!-- End Accordion 1 -->



						  </div>

						</div>



					  </div>



					  <!-- Divider -->

					  <div class="hr1" style="margin-bottom:50px;"></div>



					  <!-- Classic Heading -->

					  <h4 class="classic-title"><span>Keunggulan Kompetensi Keahlian OTKP SMK BPI</span></h4>



					  <!-- Keunggulan -->

							<p>

								<b>Dari segi pendidikan</b>

								<div class="row">

									<div class="col-md-6">

									  <ul class="icons-list" >

										<li><i class="fa fa-check-circle"></i> Pembelajaran mudah dipahami.</li>

										<li><i class="fa fa-check-circle"></i> Dibekali kemampuan mengetik cepat dengan menggunakan 10 jari.</li>

										<li><i class="fa fa-check-circle"></i> Dibekali cara administrasi yang baik dan benar.</li>

										<li><i class="fa fa-check-circle"></i> Terbiasa dengan susunan bahasa dan kata yang baik dan benar.</li>

										<li><i class="fa fa-check-circle"></i> Dibekali keterampilan pembawa acara, jurnalistik table manner, dan merias diri.</li>

										<li><i class="fa fa-check-circle"></i> Lulusan mendapatkan Sertifikat Kompetensi, bekerjasama dengan Asosiasi Sarjana dan Praktisi Administrasi Perkantoran (ASPAPI)</li>

									  </ul>

									</div>

								</div>

							</p>

							<p>

								<b>Dari segi pekerjaan</b>

								<div class="row">

									<div class="col-md-6">

									  <ul class="icons-list" >

										<li><i class="fa fa-check-circle"></i> Dapat menjadi sekretaris yang handal dan ideal.</li>
										
										<li><i class="fa fa-check-circle"></i> Dapat menjadi Resepsionis, Publik Relasi dan Customer service.</li>

										<li><i class="fa fa-check-circle"></i> Dapat melakukan pekerjaan di bidang administrasi dan keuangan.</li>

										<li><i class="fa fa-check-circle"></i> Dapat melanjutkan kuliah dengan jurusan yang berkaitan.</li>

									  </ul>

									</div>

								</div>

							</p>

							

					  <!-- End Keunggulan 

					  

					  <!-- Divider

					  <div class="hr1" style="margin-bottom:50px;"></div>



					  <!-- Classic Heading

					  <h4 class="classic-title"><span>Prestasi Administrasi Perkantoran SMK BPI</span></h4>

					  

					  <!-- Divider

					  <div class="hr1"></div>

					  

					  <table border="1" style="text-align:center;margin-top:10px;">

						<tr style="background-color:#000080;color:#f8ba01">

							<th width="40px" height="30px;" style="text-align:center;">No.</th>

							<th width="290px" style="text-align:center;">Prestasi</th>

							<th style="text-align:center;">Penyelenggara</th>

							<th style="text-align:center;">Tahun</th>

						</tr>

						<tr>

							<td>1.</td>

							<td>Instalasi Jaringan Komputer di SMK BPI Bandung dan Perbaikan PC</td>

							<td>SMK BPI Bandung</td>

							<td>2011</td>

						</tr>

						<tr>

							<td>2.</td>

							<td>Peserta lomba Perakitan PC se Jawa Barat dan Jakarta</td>

							<td>Ilmu Komputer UPI Bandung</td>

							<td>2012</td>

						</tr>

						<tr>

							<td>3.</td>

							<td>Konfigurasi server data menggunakan Linux Fedora</td>

							<td>ISMK BPI Bandung</td>

							<td>2011</td>

						</tr>

						<tr>

							<td>4.</td>

							<td>Peserta LKS Kota Bandung</td>

							<td>Dinas Kota Bandung</td>

							<td>2011, 2012, 2013</td>

						</tr>

						<tr>

							<td>5.</td>

							<td>Juara Lomba IT Networking Mikrotik</td>

							<td>UIN Sunan Gunung Djati</td>

							<td>2014</td>

						</tr>

						<tr>

							<td>6.</td>

							<td>Juara Lomba IT Networking Mikrotik</td>

							<td>UIN Sunan Gunung Djati</td>

							<td>2015</td>

						</tr>

						<tr>

							<td>7.</td>

							<td>Tim IT Konfigurasi UNBK 2016</td>

							<td>SMK BPI Bandung</td>

							<td>2016</td>

						</tr>

						<tr>

							<td>8.</td>

							<td>Tim IT Konfigurasi USBK 2016</td>

							<td>SMK BPI Bandung</td>

							<td>2016</td>

						</tr>

					  </table>
                    -->
						

					  <!-- Start Recent Projects Carousel -->

					<div class="recent-projects" style="margin-top:40px;">

					  <h4 class="classic-title"><span>Tentang Otomatisasi dan Tata Kelola Perkantoran SMK BPI</span></h4>

					  <div class="projects-carousel touch-carousel">



						<div class="portfolio-item item">

						  <div class="portfolio-border">

							<div class="portfolio-thumb">

							  <a class="lightbox" data-lightbox-type="ajax" href="<?php echo base_url();?>assets/images/AP/otkp2.png">

								<div class="thumb-overlay"><i class="fa fa-play"></i></div>

								<img alt="" src="<?php echo base_url();?>assets/images/AP/otkp2.png" />

							  </a>

							</div>

							<div class="portfolio-details">

							  <a href="#">

								<h4>Otomatisasi dan Tata Kelola Perkantoran</h4>

								<span>Siswi OTKP SMK BPI</span>

							  </a>

							</div>

						  </div>

						</div>



						<div class="portfolio-item item">

						  <div class="portfolio-border">

							<div class="portfolio-thumb">

							  <a class="lightbox" title="" href="<?php echo base_url();?>assets/images/AP/otkp3.png">

								<div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>

								<img alt="" src="<?php echo base_url();?>assets/images/AP/otkp3.png" />

							  </a>

							</div>

							<div class="portfolio-details">

							  <a href="#">

								<h4>Otomatisasi dan Tata Kelola Perkantoran</h4>

								<span>Siswi OTKP SMK BPI</span>

							  </a>

							</div>

						  </div>

						</div>



						<div class="portfolio-item item">

						  <div class="portfolio-border">

							<div class="portfolio-thumb">

							  <a class="lightbox" title="" href="<?php echo base_url();?>assets/images/AP/otkp4.png">

								<div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>

								<img alt="" src="<?php echo base_url();?>assets/images/AP/otkp4.png" />

							  </a>

							</div>

							<div class="portfolio-details">

							  <a href="#">

								<h4> Table Manner </h4>

								<span> Acara table manner SMK BPI </span>

							  </a>

							</div>

						  </div>

						</div>
						
						
						<div class="portfolio-item item">

						  <div class="portfolio-border">

							<div class="portfolio-thumb">

							  <a class="lightbox" title="" href="<?php echo base_url();?>assets/images/AP/otkp5.png">

								<div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>

								<img alt="" src="<?php echo base_url();?>assets/images/AP/otkp5.png" />

							  </a>

							</div>

							<div class="portfolio-details">

							  <a href="#">

								<h4> Table Manner </h4>

								<span> Acara table manner SMK BPI </span>

							  </a>

							</div>

						  </div>

						</div>
						
						
						<div class="portfolio-item item">

						  <div class="portfolio-border">

							<div class="portfolio-thumb">

							  <a class="lightbox" title="" href="<?php echo base_url();?>assets/images/AP/otkp6.png">

								<div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>

								<img alt="" src="<?php echo base_url();?>assets/images/AP/otkp6.png" />

							  </a>

							</div>

							<div class="portfolio-details">

							  <a href="#">

								<h4>Beauty Class</h4>

								<span>Beauty Class OTKP SMK BPI</span>

							  </a>

							</div>

						  </div>

						</div>
						

						<div class="portfolio-item item">

						  <div class="portfolio-border">

							<div class="portfolio-thumb">

							  <a class="lightbox" title="" href="<?php echo base_url();?>assets/images/AP/otkp7.png">

								<div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>

								<img alt="" src="<?php echo base_url();?>assets/images/AP/otkp7.png" />

							  </a>

							</div>

							<div class="portfolio-details">

							  <a href="#">

								<h4>Suasana Belajar</h4>

								<span>Suasana Pembelajaran di Sekolah</span>

							  </a>

							</div>

						  </div>

						</div>
						

                    	<div class="portfolio-item item">

						  <div class="portfolio-border">

							<div class="portfolio-thumb">

							  <a class="lightbox" title="" href="<?php echo base_url();?>assets/images/AP/otkp8.png">

								<div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>

								<img alt="" src="<?php echo base_url();?>assets/images/AP/otkp8.png" />

							  </a>

							</div>

							<div class="portfolio-details">

							  <a href="#">

								<h4>Fasilitas Lab</h4>

								<span>Laboratorium Komputer OTKP</span>

							  </a>

							</div>

						  </div>

						</div>


                        <div class="portfolio-item item">

						  <div class="portfolio-border">

							<div class="portfolio-thumb">

							  <a class="lightbox" title="" href="<?php echo base_url();?>assets/images/AP/otkp9.png">

								<div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>

								<img alt="" src="<?php echo base_url();?>assets/images/AP/otkp9.png" />

							  </a>

							</div>

							<div class="portfolio-details">

							  <a href="#">

								<h4>Fasilitas OTKP</h4>

								<span>Fasilitas Pembelalajaran</span>

							  </a>

							</div>

						  </div>

						</div>


					  </div>

					</div>

				</div>

			  </div>

			</div>

			<!-- End Content -->