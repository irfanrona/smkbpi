<!doctype html>

	<!-- Start Content -->
    <div id="content">
      <div class="container">
        <div class="page-content">


          <div class="row">

            <div class="col-md-7">

              <!-- TKJ Heading -->
              <h4 class="classic-title"><span><i class="fa fa-wrench"></i> Teknik Komputer Jaringan</span></h4>

              <!-- Tentang TKJ -->
              <p style="text-align:justify"><img style="float:left; width:150px; margin-right:30px;" alt="" src="<?php echo base_url();?>assets/images/tkj.png" />Teknologi Informasi dan Komputer telah menjadi bagian dalam kehidupan masyarakat di era globalisasi dan pasar bebas. Masyarakat saat ini tidak lagi bisa menghindar dari zaman digital. Sebagai jembatan menuju penguasaan teknologi Informasi dan Komunikasi.</p>
              <p style="text-align:justify">Program Studi Teknik Komputer dan Jaringan di BPI Bandung telah dirancang untuk selalu Up To date. Didukung oleh pengajar yang telah memenuhi Standar Kompetensi di bidang IT dan sarana/ prasarana di atas rata-rata serta model pembelajaran berbasis IT (media Online) siap mengantar anda menuju pintu gerbang zona digital.</p>
			  <p style="text-align:justify">Salah satu keunggulan TKJ adalah penggunaan Metode Praktik Langsung sehingga mampu menyediakan sumber belajar yang tidak terbatas, penilaian transparan serta mendidik siswa untuk berfikir ilmiah dengan memperhatikan potensi masing-masing individu. Dengan dukungan fasilitas belajar yang memadai dan dukungan ruang praktik yang cukup luas untuk memfasilitasi keinginan siswa/siswi dalam mengembangkan ilmu komputer.</p>
            </div>

            <div class="col-md-5" style="margin-top:37px;">

              <!-- Start Touch Slider -->
              <div class="touch-slider" data-slider-navigation="true" data-slider-pagination="true">
                <div class="item"><img alt="" src="<?php echo base_url();?>assets/images/tkj1.jpg"></div>
                <div class="item"><img alt="" src="<?php echo base_url();?>assets/images/tkj2.jpg"></div>
                <div class="item"><img alt="" src="<?php echo base_url();?>assets/images/tkj3.jpg"></div>
              </div>
              <!-- End Touch Slider -->

            </div>
			
          </div>

          <!-- Divider -->
          <div class="hr1" style="margin-bottom:50px;"></div>

          <div class="row">

            <div class="col-md-6">

              <!-- Classic Heading -->
              <h4 class="classic-title"><span>Kompetensi Keahlian TKJ</span></h4>

              <div class="skill-shortcode">
					<p><b>Kompetensi keahlian TKJ SMK BPI sebagai Jurusan yang bergerak di bidang Hardware dan Networking</b> juga memberikan ruang dan fasilitas yang baik bagi siswa yang memiliki minat di bidang  Administrator Jaringan Komputer, Technical Support,  Computer Network Maintenance/teknisi  maupun Keamanan Jaringan Komputer. Dengan didukung berbagai Fasilitas Online yang dapat diakses di Lokal maupun di Luar Sekolah /Internet, diantaranya adalah :
					<div class="row">
						<div class="col-md-6">
						  <ul class="icons-list" style="margin-left:30px;">
							<li><i class="fa fa-check-circle"></i> Akses Internet cepat.</li>
							<li><i class="fa fa-check-circle"></i> Hotspot Area di seluruh wilayah sekolah.</li>
						  </ul>
						</div>
					</div>
				  </p>
				  <p><b>Materi Pembelajaran</b> menyesuaikan dengan teknologi terkini berbasis pada :
					<div class="row">
						<div class="col-md-6">
						  <ul class="icons-list" style="margin-left:30px;">
							<li><i class="fa fa-check-circle"></i> Open Source.</li>
							<li><i class="fa fa-check-circle"></i> Mikrotik.</li>
							<li><i class="fa fa-check-circle"></i> Web Design.</li>
							<li><i class="fa fa-check-circle"></i> LSP Telematika.</li>
						  </ul>
						</div>
					</div>
				  </p>
				  <p><b>" Dari segi peluang kerja setelah lulus sangat banyak peluangnya. Mulai dari menjadi teknisi komputer, teknisi jaringan, membuka toko komputer, atau bisa juga membuka warnet sendiri dan melanjutkan ke Perguruan Tinggi ."</b></p>
              </div>
            </div>

            <div class="col-md-6">

              <!-- Classic Heading -->
              <h4 class="classic-title"><span>Fasilitas TKJ SMK BPI</span></h4>

              <!-- Accordion -->
              <div class="panel-group" id="accordion">

                <!-- Start Accordion 1 -->
                <div class="panel panel-default">
                  <!-- Toggle Heading -->
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapse-1">
                        <i class="fa fa-angle-up control-icon"></i>
                        <i class="fa fa-desktop"></i> Fasilitas TKJ
                      </a>
                    </h4>
                  </div>
                  <!-- Toggle Content -->
                  <div id="collapse-1" class="panel-collapse collapse in">
                    <div class="panel-body"><img class="img-thumbnail image-text" style="float:left; width:150px;" alt="" src="<?php echo base_url();?>assets/images/fas-tkj.jpg" />
						<p>Adapun <b>Fasilitas</b> yang ada di Jurusan Teknik Komputer dan Jaringan adalah :
							<div class="row">
								<div class="col-md-6">
								  <ul class="icons-list" >
									<li><i class="fa fa-check-circle"></i> Lab Komputer TKJ.</li>
									<li><i class="fa fa-check-circle"></i> Lab Komputer Software.</li>
									<li><i class="fa fa-check-circle"></i> Lab Komputer Hardware.</li>
									<li><i class="fa fa-check-circle"></i> Tower Wireless.</li>
									<li><i class="fa fa-check-circle"></i> Peralatan Pembelajaran Lengkap.</li>
								  </ul>
								</div>
							</div>
						</p>
					</div>
                  </div>
                </div>
                <!-- End Accordion 1 -->

              </div>
            </div>

          </div>

          <!-- Divider -->
          <div class="hr1" style="margin-bottom:50px;"></div>

          <!-- Classic Heading -->
          <h4 class="classic-title"><span>Keunggulan Kompetensi Keahlian TKJ SMK BPI</span></h4>

          <!-- Keunggulan -->
				<p>
					<b>Dari segi pendidikan</b>
					<div class="row">
						<div class="col-md-6">
						  <ul class="icons-list" >
							<li><i class="fa fa-check-circle"></i> Mudah Dipahami.</li>
							<li><i class="fa fa-check-circle"></i> Masih Kelas XI,, sudah bisa menghasilkan uang (dengan hanya berbekal kompetensi Instalasi OS dan perakitan PC .</li>
							<li><i class="fa fa-check-circle"></i> Dapat memperdalam jurusan lain (seperti Animasi, Multimedia, Desain grafis, Pemrograman).</li>
							<li><i class="fa fa-check-circle"></i> Menjadi jurusan yang memperdalam ilmu Teknologi, baik Software ataupun Hardware.</li>
							<li><i class="fa fa-check-circle"></i> Bekerjasama dengan beberapa Perusahaan IT ( Axioo, Netkrom).</li>
							<li><i class="fa fa-check-circle"></i> Lulusan Selain dapat Ijazah juga dapat Sertifikat yang diakui oleh Internasional. Dalam hal ini Sertifikasinya  IT Networking dari MIKROTIK.</li>
						  </ul>
						</div>
					</div>
				</p>
				<p>
					<b>Dari segi pekerjaan</b>
					<div class="row">
						<div class="col-md-6">
						  <ul class="icons-list" >
							<li><i class="fa fa-check-circle"></i> Dapat menjadi seorang ahli IT Networking.</li>
							<li><i class="fa fa-check-circle"></i> Dapat membuka usaha jaringan.</li>
							<li><i class="fa fa-check-circle"></i> Dapat melanjutkan kuliah dengan jurusan berbeda.</li>
							<li><i class="fa fa-check-circle"></i> Dapat menjadi teknisi baik jaringan  internet maupun PC/Laptop.</li>
						  </ul>
						</div>
					</div>
				</p>
				
          <!-- End Keunggulan -->
		  
		  <!-- Divider -->
          <div class="hr1" style="margin-bottom:50px;"></div>

          <!-- Classic Heading -->
          <h4 class="classic-title"><span>Prestasi TKJ SMK BPI</span></h4>
		  
		  <!-- Divider -->
          <div class="hr1"></div>
		  
		  <table border="1" style="text-align:center;margin-top:10px;">
			<tr style="background-color:#000080;color:#f8ba01">
				<th width="40px" height="30px;" style="text-align:center;">No.</th>
				<th width="290px" style="text-align:center;">Prestasi</th>
				<th style="text-align:center;">Penyelenggara</th>
				<th style="text-align:center;">Tahun</th>
			</tr>
			<tr>
				<td>1.</td>
				<td>Instalasi Jaringan Komputer di SMK BPI Bandung dan Perbaikan PC</td>
				<td>SMK BPI Bandung</td>
				<td>2011</td>
			</tr>
			<tr>
				<td>2.</td>
				<td>Peserta lomba Perakitan PC se Jawa Barat dan Jakarta</td>
				<td>Ilmu Komputer UPI Bandung</td>
				<td>2012</td>
			</tr>
			<tr>
				<td>3.</td>
				<td>Konfigurasi server data menggunakan Linux Fedora</td>
				<td>ISMK BPI Bandung</td>
				<td>2011</td>
			</tr>
			<tr>
				<td>4.</td>
				<td>Peserta LKS Kota Bandung</td>
				<td>Dinas Kota Bandung</td>
				<td>2011, 2012, 2013</td>
			</tr>
			<tr>
				<td>5.</td>
				<td>Juara Lomba IT Networking Mikrotik</td>
				<td>UIN Sunan Gunung Djati</td>
				<td>2014</td>
			</tr>
			<tr>
				<td>6.</td>
				<td>Juara Lomba IT Networking Mikrotik</td>
				<td>UIN Sunan Gunung Djati</td>
				<td>2015</td>
			</tr>
			<tr>
				<td>7.</td>
				<td>Tim IT Konfigurasi UNBK 2016</td>
				<td>SMK BPI Bandung</td>
				<td>2016</td>
			</tr>
			<tr>
				<td>8.</td>
				<td>Tim IT Konfigurasi USBK 2016</td>
				<td>SMK BPI Bandung</td>
				<td>2016</td>
			</tr>
			<tr>
				<td>9.</td>
				<td>Juara 1 Lomba Karya Ilmiah bidang Sumber Daya Air 2018</td>
				<td>Kementerian Pekerjaan Umum dan Perumahan Rakyat</td>
				<td>2018</td>
			</tr>
			<tr>
				<td>10.</td>
				<td>Juara 3 Lomba Karya Ilmiah bidang Sumber Daya Air 2019</td>
				<td>Kementerian Pekerjaan Umum dan Perumahan Rakyat</td>
				<td>2019</td>
			</tr>
		  </table>
		    
          <!-- Start Recent Projects Carousel -->
        <div class="recent-projects" style="margin-top:40px;">
          <h4 class="classic-title"><span>Tentang TKJ SMK BPI</span></h4>
          <div class="projects-carousel touch-carousel">

            <div class="portfolio-item item">
              <div class="portfolio-border">
                <div class="portfolio-thumb">
                  <a class="lightbox" data-lightbox-type="ajax" href="<?php echo base_url();?>assets/images/portfolio-1/tkj1_open.jpg">
                    <div class="thumb-overlay"><i class="fa fa-play"></i></div>
                    <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/tkj1.jpg" />
                  </a>
                </div>
                <div class="portfolio-details">
                  <a href="#">
                    <h4>Teknik Komputer Jaringan</h4>
                    <span>by: TIM ICT SMK BPI Bandung</span>
                  </a>
                </div>
              </div>
            </div>

            <div class="portfolio-item item">
              <div class="portfolio-border">
                <div class="portfolio-thumb">
                  <a class="lightbox" title="Profil TKJ SMK BPI" href="<?php echo base_url();?>assets/images/portfolio-1/tkj2_open.jpg">
                    <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
                    <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/tkj2.jpg" />
                  </a>
                </div>
                <div class="portfolio-details">
                  <a href="#">
                    <h4>Hasil Karya Anak TKJ</h4>
                    <span>By : TIM ICT SMK BPI Bandung</span>
                  </a>
                </div>
              </div>
            </div>

            <div class="portfolio-item item">
              <div class="portfolio-border">
                <div class="portfolio-thumb">
                  <a class="lightbox" title="Profil TKJ SMK BPI" href="<?php echo base_url();?>assets/images/portfolio-1/tkj3_open.jpg">
                    <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
                    <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/tkj3.jpg" />
                  </a>
                </div>
                <div class="portfolio-details">
                  <a href="#">
                    <h4>Hasil Karya Anak TKJ</h4>
                    <span>By : TIM ICT SMK BPI Bandung</span>
                  </a>
                </div>
              </div>
            </div>
			
			<div class="portfolio-item item">
              <div class="portfolio-border">
                <div class="portfolio-thumb">
                  <a class="lightbox" title="Profil TKJ SMK BPI" href="<?php echo base_url();?>assets/images/portfolio-1/tkj4_open.jpg">
                    <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
                    <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/tkj4.jpg" />
                  </a>
                </div>
                <div class="portfolio-details">
                  <a href="#">
                    <h4>Hasil Karya Anak TKJ</h4>
                    <span>By : TIM ICT SMK BPI Bandung</span>
                  </a>
                </div>
              </div>
            </div>
			
			<div class="portfolio-item item">
              <div class="portfolio-border">
                <div class="portfolio-thumb">
                  <a class="lightbox" title="Profil TKJ SMK BPI" href="<?php echo base_url();?>assets/images/portfolio-1/tkj5_open.jpg">
                    <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
                    <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/tkj5.jpg" />
                  </a>
                </div>
                <div class="portfolio-details">
                  <a href="#">
                    <h4>Hasil Karya Anak TKJ</h4>
                    <span>Sistem Operasi</span>
                    <span>By : TIM ICT SMK BPI Bandung</span>
                  </a>
                </div>
              </div>
            </div>
			
          </div>
        </div>
	</div>
  </div>
</div>