<!-- Start Content -->

<div id="content">

    <div class="container">

        <div class="page-content">

            <div class="row">

                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-8">

                </div>

                <div id="wrap">

                        <div id="konten">

                            <div class="posttitle">

                            	<div class="row">

                            		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            		
                                		<h1 id="title" style="font-size: 30px;"> PENDAFTARAN PESERTA DIDIK BARU SMK BPI BANDUNG <br> TAHUN AJARAN 2021/2022</h1>

                                		<p id="postdate" style="font-size: 13px;">Diunggah pada tanggal 27 Februari 2021</p><br><br>

                            		</div>

                            	</div>

                            </div>

                            <!-- <div class="post">

                                <h4 style="font-weight: bold">Alur pendaftaran peserta didik baru SMK BPI Bandung</h4><br>

                                <img id="alur" class="img-responsive" alt="" src="<?php echo base_url();?>assets/images/alur-ppdb.png" />

                            </div><br><br><br> -->

                            <!-- brosur -->
                            <!-- <div class="post">

                                <div class="row">

	                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

	                                <h4 style="font-weight: bold">Brosur pendaftaran peserta didik baru SMK BPI Bandung </h4><br>

	                                </div>
	                                
	                                <iframe class="col-lg-12 col-md-12 col-sm-12 col-xs-12 iframe height-brosur" src="https://drive.google.com/file/d/1UH3_ZuIqJ5BzMmb88oNzj05Ba62sF6FR/preview"></iframe>

                                </div><br>

                            </div> -->
                            
                            <div class="post">

                                <br><h4 style="font-weight: bold">Informasi selengkapnya</h4><br>
                                <p>Situs Web Yayasan BPI : <a href="https://ppdb.bpi.web.id" target="_blank" rel="nofollow"><b>PPDB BPI</b></a></p>
                                <h5 style="font-weight: bold">Kontak:</h5>
                                <ul>
                                    <li>Ade Aso, S.Pd.              | 0821 1635 9966</li>
                                    <li>Acep Komarudin, S.Si        | 0838 2108 1572</li>
                                    <li>Wahyu Sinarningsih, S.Pd.   | 0838 2058 5258</li>
                                    <li>Fani Setiani, S. Pd.        | 0812 2026 0667</li>
                                    <li>Dra. Anggani                | 0896 1009 3005</li>
                                    <li>Yayan Himawan, S.Pd., M.MPd. | 0821 1635 9966</li>
                                </ul>

                            </div><br><br><br>

                            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-4 text-right">

                                <!-- <a href="<?php echo base_url();?>assets/file/Brosur PPDB SMK BPI.pdf"><button class="btn btn-warning" type="button">

                                  <b>UNDUH BROSUR</b>

                                </button></a> -->

                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-1 col-xs-3">
                                <a href="https://ppdb.bpi.web.id" target="_blank"><button class="btn btn-warning" type="button">

                                    <b>DAFTAR DISINI!</b>

                                    </button></a>
                            </div>    

                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3 text-right">

                                <!-- <a href="<?php echo site_url('home/daftar')?>"><button class="btn btn-warning" type="button">

                                  <b>DAFTAR DISINI!</b>

                                </button></a> -->

                            </div>  

                        </div> 

                </div><br>

            </div>

        </div>

    </div>

</div>





