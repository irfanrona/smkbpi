<?php if (! defined('BASEPATH')) exit ('No direct script access allowed');

	

	class Home extends CI_Controller{

		public function __construct(){

			parent:: __construct();

			$this->load->model('admin');

		}



		public function index(){

			$this->tampil_home();

		}

		

		public function tampil_home(){

			$data['berita'] = $this->admin->berita();

			$data['acara'] = $this->admin->acara();

			$data['title'] = 'SMK BPI Bandung';

			

			$this->load->view('master/header', $data);

			$this->load->view('master/navbar');

			$this->load->view('view_home', $data);
			
			//$this->load->view('improvement', $data);

			$this->load->view('master/footer');

		}



		public function login(){

			$data['title'] = 'Login Website SMK BPI Bandung';

        

			$this->load->view('master/header', $data);

			$this->load->view('master/navbar');

			$this->load->view('admin/login');

			$this->load->view('master/footer');

		}

		public function auth(){

			$username = $this->input->post('username');

			$password = $this->input->post('password');

			$pass = md5($password);

			$user = $this->db->get_where('user', ['username' => $username])->row_array();



			//jika user ada

			if ($user) {

				//jika password

				if ($pass == $user['password']) {

					$data = [

						'username' => $user['username'],

						'level_user' => $user['level_user'],

						'is_logged_in' => TRUE

					];

					$this->session->set_userdata($data);

					redirect('adminController/dashboard');

				} else {

					$this->session->set_flashdata('pesan', '<div class="alert alert-danger" role="alert">

					Password Anda Salah!

					</div>');

					redirect('home/login');

				}

			} else {

				$this->session->set_flashdata('pesan', '<div class="alert alert-danger" role="alert">

					Akun tidak terdaftar!

					</div>');

				redirect('home/login');

			}

		}

		public function improvement(){
			$data['title'] = 'SMK BPI Bandung';



			$this->load->view('master/header', $data);

			$this->load->view('master/navbar');

			$this->load->view('improvement');

			$this->load->view('master/footer');

		}		

		public function profil(){

			$data['title'] = 'Profil SMK BPI Bandung';



			$this->load->view('master/header', $data);

			$this->load->view('master/navbar');

			$this->load->view('profil/profil');

			$this->load->view('master/footer');

		}

		

		public function visi(){

			$data['title'] = 'Visi SMK BPI Bandung';



			$this->load->view('master/header', $data);

			$this->load->view('master/navbar');

			$this->load->view('profil/visi');

			$this->load->view('master/footer');

		}

		

		public function osis(){

			$data['title'] = 'OSIS SMK BPI Bandung';



			$this->load->view('master/header', $data);

			$this->load->view('master/navbar');

			$this->load->view('profil/osis');

			$this->load->view('master/footer');

		}

		

		public function ekskul(){

			$data['title'] = 'Ekskul SMK BPI Bandung';



			$this->load->view('master/header', $data);

			$this->load->view('master/navbar');

			$this->load->view('profil/ekskul');

			$this->load->view('master/footer');

		}

		public function prestasi(){

			$data['title'] = 'Prestasi SMK BPI Bandung';



			$this->load->view('master/header', $data);

			$this->load->view('master/navbar');

			$this->load->view('profil/prestasi');

			$this->load->view('master/footer');

		}


		public function up(){

			$data['title'] = 'Unit Produksi SMK BPI Bandung';



			$this->load->view('master/header', $data);

			$this->load->view('master/navbar');

			$this->load->view('profil/up');

			$this->load->view('master/footer');

		}

		

		public function program(){

			$data['title'] = 'Program Keahlian SMK BPI Bandung';



			$this->load->view('master/header', $data);

			$this->load->view('master/navbar');

			$this->load->view('program/program');

			$this->load->view('master/footer');

		}

		

		public function otkp(){

			$data['title'] = 'OTKP SMK BPI Bandung';



			$this->load->view('master/header', $data);

			$this->load->view('master/navbar');

			$this->load->view('program/otkp');

			$this->load->view('master/footer');

		}

		

		public function rpl(){

			$data['title'] = 'Rekayasa Perangkat Lunak SMK BPI Bandung';



			$this->load->view('master/header', $data);

			$this->load->view('master/navbar');

			$this->load->view('program/rpl');

			$this->load->view('master/footer');

		}

		

		public function tkj(){

			$data['title'] = 'Profil TKJ SMK BPI Bandung';



			$this->load->view('master/header', $data);

			$this->load->view('master/navbar');

			$this->load->view('program/tkj');

			$this->load->view('master/footer');

		}

		

		public function agenda(){

			$data['title'] = 'Agenda Acara SMK BPI Bandung';



			$this->load->view('master/header', $data);

			$this->load->view('master/navbar');

			$this->load->view('agenda/agenda');

			$this->load->view('master/footer');

		}

		

		public function galeri(){

			$data['model'] = $this->admin->galeri();

			$data['title'] = 'Galeri SMK BPI Bandung';



			$this->load->view('master/header', $data);

			$this->load->view('master/navbar');

			$this->load->view('agenda/galeri', $data);

			$this->load->view('master/footer');

		}

		

		public function pegawai(){

			$data['title'] = 'Pegawai SMK BPI Bandung';



			$this->load->view('master/header', $data);

			$this->load->view('master/navbar');

			$this->load->view('pegawai');

			$this->load->view('master/footer');

		}

		

		public function ppdb(){

			$data['title'] = 'Pendaftaran SMK BPI Bandung';

			$this->load->view('master/header', $data);

			$this->load->view('master/navbar');

			$this->load->view('ppdb');

			$this->load->view('master/footer');

		}

		public function daftar(){

			$data['title'] = 'Pendaftaran SMK BPI Bandung';

			$this->load->view('master/header', $data);

			$this->load->view('master/navbar');

			$this->load->view('form_ppdb');

			$this->load->view('master/footer');

		}
		
		public function pendaftaran(){
		    
		    $nisn = html_escape($this->input->post('nisn'));

	        $data = array(
	            "nama_pendaftar" => html_escape($this->input->post('name')),
	            "nisn" => $nisn,
	            "jenis_kelamin" => $this->input->post('gender'),
	            "tempat_lahir" => html_escape($this->input->post('birthplace')),
	            "tanggal_lahir" => date('y-m-d', strtotime($_POST['birthdate'])),
	            "agama" => html_escape($this->input->post('religion')),
	            "alamat" => html_escape($this->input->post('address')),
	            "no_telepon" => html_escape($this->input->post('notelp')),
	            "no_handphone" => html_escape($this->input->post('nohp')),
	            "email" => html_escape($this->input->post('email')),
	            "nama_ortu" => html_escape($this->input->post('parentname')),
	            "asal_sekolah" => html_escape($this->input->post('school')),
	            "jurusan" => $this->input->post('major'),
	            "asal_informasi" => implode(",", $_POST['information'])
	        );
            
            $cek = $this->db->where('nisn',$nisn)->get('pendaftaran');

            if($cek->num_rows()>0){
                
                echo "
    	        	<script>
    	        		alert('Anda sudah pernah melakukan pendaftaran'); 
    	        		history.go(-1);
    	        	</script>";
    
            } else{
                
                $query = $this->db->insert('pendaftaran', $data);
    	        
    	        if ($query == 1) {
    	        	echo "
    	        	<script> 
    	        		alert('Terimakasih pendaftaran telah berhasil, mohon tunggu informasi selanjutnya');
    	        		window.location.href='home/index';
    	        	</script>";
    	        
    	        } else {
    	        	echo "
    	        	<script>
    	        		alert('Mohon maaf terdapat kesalahan, silahkan coba kembali'); 
    	        		history.go(-1);
    	        	</script>";
    	        }
    	        
            }
	        
	        
	        
    	}


		// public function to(){

		// 	$this->load->view('to');

		// }

	}

?>