<?php if (! defined('BASEPATH')) exit ('No direct script access allowed');

	

class AdminController extends CI_Controller{

    public function __construct()

    {

        parent::__construct();

        $this->load->model('admin');

        

        if($this->session->userdata('is_logged_in') != TRUE){

         redirect("home/login");

        }

    }



    public function index(){

        $this->dashboard();

    }

    

    public function dashboard(){

        $data['title'] = 'Dashboard Admin SMK BPI Bandung';

        

        $this->load->view('master/header', $data);

        $this->load->view('master/navbar');

        $this->load->view('admin/dashboard');

        $this->load->view('master/footer');

    }

    public function acara(){

        $data['model'] = $this->admin->acara();

        $data['title'] = 'Acara';

        

        $this->load->view('master/header', $data);

        $this->load->view('master/navbar');

        $this->load->view('admin/acara', $data);

        $this->load->view('master/footer');

    }



    public function save_acara()

    {

        $config = array(

			'upload_path' => './assets/images/events',

            'allowed_types' => 'gif|png|jpeg|jpg',

            'max_size' => '200000'

		);

        $this->load->library('upload', $config);

        $this->upload->do_upload('image');



        $upload_data = $this->upload->data();

        $image = $upload_data['file_name'];



        $data = array(

            "title" => $this->input->post('title'),

            "description" => $this->input->post('description'),

            "image" => $image,

            "start" => date('y-m-d', strtotime($_POST['start'])),

            "end" => date('y-m-d', strtotime($_POST['end']))

        );

        $this->db->insert('events', $data);

        $this->session->set_flashdata('notif', 'ditambahkan');

        redirect('adminController/acara');

    }

    public function update_acara()

    {

        $id = $this->input->post('id');

        $config = array(

			'upload_path' => './assets/images/events',

			'allowed_types' => 'gif|png|jpeg|jpg',

			'max_size' => '200000'

		);

        $this->load->library('upload', $config);

        

        if ($this->upload->do_upload('image')) {

            $img = $this->db->where('id',$id)->get('events')->row();

            if($img->image != ''){

                unlink('./assets/images/events/'.$img->image);

            }

			$upload_data = $this->upload->data();

            $image = $upload_data['file_name'];

			$data = array(

                "title" => $this->input->post('title'),

                'image' => $image,

                "description" => $this->input->post('description'),

                "start" => date('y-m-d', strtotime($_POST['start'])),

                "end" => date('y-m-d', strtotime($_POST['end'])),

			);

		} else {

			$data = array(

				"title" => $this->input->post('title'),

                "description" => $this->input->post('description'),

                "start" => date('y-m-d', strtotime($_POST['start'])),

                "end" => date('y-m-d', strtotime($_POST['end'])),

			);

        }

        $this->db->where('id', $id);

        $this->db->update('events', $data);

        $this->session->set_flashdata('notif', 'diubah');

        redirect('adminController/acara');

    }

    public function delete_acara($id)

    {

        $image = $this->db->where('id',$id)->get('events')->row();

        if($image->image != ''){

            unlink('./assets/images/events/'.$image->image);

        }

        $this->db->where('id', $id);

        $this->db->delete('events');

        $this->session->set_flashdata('notif', 'dihapus');

        redirect('adminController/acara');

    }

    public function berita(){

        $data['model'] = $this->admin->berita();

        $data['title'] = 'Berita';

        

        $this->load->view('master/header', $data);

        $this->load->view('master/navbar');

        $this->load->view('admin/berita', $data);

        $this->load->view('master/footer');

    }

    public function save_berita()

    {

        $data = array(

            "judul_berita" => $this->input->post('judul_berita'),

            "berita" => $this->input->post('berita'),

            "create_by" => $this->session->userdata('username')

        );



        $this->db->insert('berita', $data);

        $this->session->set_flashdata('notif', 'ditambahkan');

        redirect('adminController/berita');

    }

    public function update_berita()

    {

        $id = $this->input->post('id_berita');

        $data = array(

            "judul_berita" => $this->input->post('judul_berita'),

            "berita" => $this->input->post('berita'),

            "create_dt" => date('y-m-d', strtotime($_POST['create_dt'])),

            "modify_by" => $this->session->userdata('username')

        );



        $this->db->where('id_berita', $id);

        $this->db->update('berita', $data);

        $this->session->set_flashdata('notif', 'diubah');

        redirect('adminController/berita');

    }

    public function delete_berita($id)

    {

        $this->db->where('id_berita', $id);

        $this->db->delete('berita');

        $this->session->set_flashdata('notif', 'dihapus');

        redirect('adminController/berita');

    }

    public function gallery(){

        $data['model'] = $this->admin->galeri();

        $data['title'] = 'Gallery';

        

        $this->load->view('master/header', $data);

        $this->load->view('master/navbar');

        $this->load->view('admin/gallery', $data);

        $this->load->view('master/footer');

    }

    public function save_galeri(){

        $config = array(

			'upload_path' => './assets/images/galeri',

            'allowed_types' => 'gif|png|jpeg|jpg',

            'max_size' => '200000'

		);

        $this->load->library('upload', $config);

        $this->upload->do_upload('foto');



        $upload_data = $this->upload->data();

        $foto = $upload_data['file_name'];



        $data = array(

            "judul_galeri" => $this->input->post('judul_galeri'),

            "foto" => $foto,

            "deskripsi" => $this->input->post('deskripsi')

        );



        $this->db->insert('galeri', $data);

        $this->session->set_flashdata('notif', 'ditambahkan');

        redirect('adminController/gallery');

    }

    public function update_galeri(){

        $id = $this->input->post('id_galeri');

        $config = array(

			'upload_path' => './assets/images/galeri',

            'allowed_types' => 'gif|png|jpeg|jpg',

            'max_size' => '200000'

		);

        $this->load->library('upload', $config);



        if($this->upload->do_upload('foto')){

            $image = $this->db->where('id_galeri',$id)->get('galeri')->row();

            if($image->foto != ''){

                unlink('./assets/images/galeri/'.$image->foto);

            }

            $upload_data = $this->upload->data();

            $foto = $upload_data['file_name'];

            $data = array(

                "judul_galeri" => $this->input->post('judul_galeri'),

                "foto" => $foto,

                "deskripsi" => $this->input->post('deskripsi')

            );

        }else{

            $data = array(

                "judul_galeri" => $this->input->post('judul_galeri'),

                "deskripsi" => $this->input->post('deskripsi')

            );

        }

        $this->db->where('id_galeri', $id);

        $this->db->update('galeri', $data);

        $this->session->set_flashdata('notif', 'diubah');

        redirect('adminController/gallery');

    }

    public function delete_galeri($id)

    {

        $image = $this->db->where('id_galeri',$id)->get('galeri')->row();

        if($image->foto != ''){

            unlink('./assets/images/galeri/'.$image->foto);

        }

        $this->db->where('id_galeri', $id);

        $this->db->delete('galeri');

        $this->session->set_flashdata('notif', 'dihapus');

        redirect('adminController/gallery');

    }

    public function setting(){

        $data['title'] = 'Setting';

        

        $this->load->view('master/header', $data);

        $this->load->view('master/navbar');

        $this->load->view('improvement');

        $this->load->view('master/footer');

    }

    function logout()

    {

        $this->session->sess_destroy();

        redirect('home/login');

    }

}

?>