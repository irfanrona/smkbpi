-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 07, 2020 at 09:14 PM
-- Server version: 5.7.32-cll-lve
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smkbisch_update`
--

-- --------------------------------------------------------

--
-- Table structure for table `absensi`
--

CREATE TABLE `absensi` (
  `id_absensi` int(11) NOT NULL,
  `id_keterangan_absensi` int(11) DEFAULT NULL,
  `nis` varchar(10) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `detail` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `absensi`
--

INSERT INTO `absensi` (`id_absensi`, `id_keterangan_absensi`, `nis`, `tanggal`, `detail`) VALUES
(1, 1, '151610001', '2016-03-29 16:43:31', NULL),
(2, 1, '151610001', '2016-08-29 00:00:00', NULL),
(3, 1, '1006441', '2016-08-29 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `artikel`
--

CREATE TABLE `artikel` (
  `id_artikel` int(11) NOT NULL,
  `judul_artikel` varchar(150) DEFAULT NULL,
  `gambar_utama` varchar(150) NOT NULL,
  `artikel` text,
  `id_kategori` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `tgl_posting` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artikel`
--

INSERT INTO `artikel` (`id_artikel`, `judul_artikel`, `gambar_utama`, `artikel`, `id_kategori`, `id_user`, `tgl_posting`) VALUES
(4, 'Pengertian Administrasi Perkantoran dan Ruang Lingkupnya', 'Artikel_4_PengertianAdministrasiPerkantorandanRuangLingkupnya.jpg', '<p><strong>Pengertian Administrasi Perkantoran, Karakteristik, &amp; Ruang Lingkupnya&nbsp;</strong><strong>| </strong>Administrasi perkantoran yang akan kami bahas habis baik itu pengertian, karakteristik, ruang lingkup, unsur-unsur, tujuan dan fungsi dari administrasi perkantoran. Langsung saja, secara umum pengertian administrasi perkantoran&nbsp;adalah suatu kegiatan perencanaan keuangan, penagihan dan pencatatan, personalia, dan distribusi barang serta logistik di sebuah organisasi. Biasanya seorang karyawan yang bertugas dalam hal ini disebut dengan administrator kantor atau manajer kantor.<br /><br /><strong>Pengertian Administrasi Perkantoran</strong> terbagi atas dua yaitu pengertian administrasi perkantoran secara luas dan administrasi perkantoran secara sempit. Pengertian administrasi perkantoran secara luas adalah suatu kerja sama secara sistematis dan teroordinasi menurut pembagian tugas sesuai dengan struktur organisasi dalam mencapai tujuan secara efektif dan efisien. Sedangkan pengertian administrasi perkantoran dalam arti sempit adalah semua kegiatan yang bersifat teknis ketatausahaan dalam pelaksanaan pekerjaan operatif, penyediaan keterangan bagi pimpinan, dan membantu kelancaran perkembangan organisasi.<br /><br /><br /></p>\r\n<h3><strong>Karakteristik Administrasi Perkantoran</strong></h3>\r\n<p><strong>Karakteristik Administrasi Perkantoran - </strong>Administrasi perkantoran memiliki perbedaan dengan tata usaha perkantoran. Terdapat faktor pembeda, mulai dari jenis kegiatan dan luas lingkup unit kerja sebagai tempat dalam pelaksanaan kegiatan. Untuk lebih jelasnya berikut karakteristik/ciri-ciri administrasi perkantoran...<br /><strong>a. Bersifat pelayanan (Service) pada semua pihak atau orang</strong><br />Hal ini bermakna bahwa pekerjaan kantor umumnya merupakan dari pelayanan dan support untuk kegiatan organisasi.<br /><strong>b. Merembes dan dilaksanakan oleh semua pihak&nbsp;</strong><br />Hal ini bermakna bahwa pekerjaan kantor berdampak pada unit-unit lain yang selalu hadir dan dilaksankan disetiap organisasi.<br /><strong>c. Hasil akhirnya berupa informasi&nbsp;</strong><br />Informasi adalah keterangan-keterangan yang berisi data yang dapat dipercaya dalam kepentingan pihak tertentu. Pihak-pihak yang berkepentingan terhadap informasi yang berada di kantor lain; pimpinan, pemegang saham, pemerintah, masyarakat, dan karyawan organisasi dsb.<br /><strong>d. Bersifat memudahkan&nbsp;</strong><br />Pekerjaan kantor merupakan alat katalisator yang memiliki bermacam-macam kegiatan dari setiap perusahaan dipersatukan.<br /><strong>e. Bersifat pengetikan dan penghitungan</strong><br />Susunan perkerjaan kantor lebih banyak yang terdiri dari pekerjaan mengetik&nbsp;<strong>&nbsp;</strong><br /><strong>f. Dilakukan oleh semua pihak</strong><br />Pekerjaan kantor tidak selalu dikerjakan dalam satu bagian yang beberapa kantor dikerjakan dalam tiap bagian perusahaan.</p>\r\n<h3><strong>Ruang Lingkup Administrasi Perkantoran</strong></h3>\r\n<p><strong>Ruang Lingkup Administrasi Perkantoran - </strong>Ruang lingkup administrasi perkantoran mencakup dari kegiatan kantor dan saranan fasilitas kerja kantoran. Ruang lingkup administrasi perakantoran adalah sebagai berikut...<br /><strong>1. Kegiatan Kantor&nbsp;</strong><br />Kegiatan kantor merupakan kegiatan yang selalu dilakukan dalam perkantoran dimana semakin luas tujuan perusahaan maka semakin besar pula kegiatan perkantoran yang dilakukan. Pada umumnya kegiatan perkantoran terdri dari beberapa kegiatan antara lain sebagai berikut..<br /><strong>a. Perencanaan Perkantoran (Office Planning), </strong>adalah proses menentukan arah kegiatan kantor, dengan peninjauan kembali terhadap faktor-faktor yang mempengaruhi tercapainya tujuan kantor. Perencanaan perkantoran terdiri dari<br />1). Perencanaan gedung<br />2). Tata ruang kantor<br />3). Penerangan/cahaya<br />4). Ventilasi<br />5). Perlengkapan peralatan dan perabotan kantor<br />6). Anggaran (budgetting) perkantoran<br />7). Standar kualitas kerja<br />8). Sistem informasi dan telekomuniktasi<br /><br /><strong>b. Pengorganisasian Perkantoran (Office Organizing), </strong>adalah pengaturan sejumlah fungsi organisasi dengan fungsi-fungsi meliputi</p>\r\n<ul>\r\n<li>Pembagian tugas dan pekerjaan untuk efisiensi dalam organisasi perusahaan&nbsp;</li>\r\n<li>Pemeliharaan hubungan kerja yang baik dengan atasan maupun dengan bawahan</li>\r\n<li>Penyediaan peralatan/perlengkapan yang tepat, sesuai dengan jenis pekerjaan untuk memudahkan karyawan dalam melakukan pekerjaan.</li>\r\n</ul>\r\n<div><strong>c. Pengarahan Perkantoran (Office Actuating),&nbsp;</strong>Pengarahan perkantoran adalah suatu kegaitan yang meningkatkan efektivitas dan efisiensi kerja secara maksimal sesuai dengan target dan sasaran yang telah ditentukan serta untuk menciptakan lingkungan kerja yang sehat dan dinamis. Pengarahan perkantoran adalah sebagai berikut.&nbsp;</div>\r\n<div>\r\n<ul>\r\n<li>Penggunaan teknik yang efektif dalam melakukan pengawasan terhadap bawahan</li>\r\n<li>Penggunaan teknik yang efektif dalam memberikan motivasi terhadap bawahan</li>\r\n<li>Pemberian bantuan kepada karyawan dalam memecahkan masalah ketika karyawan menghadapi kesulitan dalam pekerjaan&nbsp;</li>\r\n<li>Penyatuan visi misi karyawan dan organisasi</li>\r\n<li>Perancangan cara komunikasi yang efektif dengan karyawan, untuk komunikasi antara atasan dengan bawahan agar sedianya dapat berjalan lancar.&nbsp;</li>\r\n<li>Penggunaan tolak ukur yang benar dan adil dalam pemberian gaji karyawan.</li>\r\n</ul>\r\n</div>\r\n<p><strong>d. Pengawasan Perkantoran (Office Controlling),&nbsp;</strong>Pengawasan perkantoran adalah kegiatan yang mematikan bahwa sasaran dan perencanaan dapat berjalan sesuai dengan target yang ingin dicapai. Objek pengawasan perkantoran terdiri dari:</p>\r\n<ul>\r\n<li>Penggunaan peralatan dan perabot kantor</li>\r\n<li>Metode-metode dan standarisasi pekerjaan kantor</li>\r\n<li>Kualitas pekerjaan kantor&nbsp;</li>\r\n<li>Pelayanan kantor&nbsp;</li>\r\n<li>Waktu</li>\r\n<li>Biaya perkantoran</li>\r\n</ul>\r\n<p><strong>2. Saranan dan Fasilitas Kerja Kantoran</strong><br />Dari hasil uraian diatas, kantor merupakan keseluruhan ruang dalam suatu bangunan yang terdapat kegiatan tata usaha atau kegiatan manajemen maupun berbagai tugas lainnya. Perkantoran terikat dengan prasarana seperti:<br /><strong>a. Lokasi Kantor</strong><br />Faktor-faktor yang diperhatikan dalam penentuan lokasi kantor adalah sebagai berikut..</p>\r\n<ul>\r\n<li>Keamanan&nbsp;</li>\r\n<li>Lingkungan</li>\r\n<li>Harga</li>\r\n</ul>\r\n<div><strong>b. Gedung</strong></div>\r\n<div>Faktor-faktor yang menjadi perhatian dalam penentuan gedung adalah sebagai berikut..&nbsp;</div>\r\n<div>\r\n<ul>\r\n<li>Gedung menjamin keamanan dan kesehatan karyawan</li>\r\n<li>Gedung memiliki fasilitas yang memadai</li>\r\n<li>Harga gedung yang kompetitif seimbang dengan biaya dan keuntungannya.&nbsp;</li>\r\n</ul>\r\n</div>\r\n<div><strong>c. Peralatan&nbsp;</strong><br />Peralatan digolongkan kedalam dua kelompok antara lain sebagai berikut..<br />1). Perabotan kantor (office furniture), misalnya kursi, meja, laci, rak dan sebagainya yang terbuat dari besi, kayu, maupun bahan-bahan lainnya, yang memiliki peranan penting dalam kantor. <br />2). Perbekalan kantor (office suplies), misalnya kertas, penghapus, pena, tinta printer, dan peralatan habis pakai lainnya.<br /><br /><strong>d. Interior</strong><br />Interior adalah tatanan perabot atau perangkat yang menunjang pelaksanaan kerja dalam ruang kantor, misalnya plafon, penerangan, ventilisais, hiasan kantor dan jendela.<br /><br /><strong>e. Mesin-Mesin Kantor</strong><br />Dalam perencanaan kegiatan kantor memiliki rumus perencanaan mesin yang digunakan sesuai dengan prosedur kerja, metode kerja dan kebutuhan interior.<br />\r\n<h3><strong>Unsur-Unsur Administrasi Perkantoran</strong></h3>\r\n<strong>Unsur-Unsur Administrasi Perkantoran - </strong>Administrasi perkantoran memiliki beberapa unsur-unsur antara lain sebagai berikut...<br />\r\n<ul>\r\n<li><em>Organisasi, </em>ialah mengelompokkan dan menyusun kerangka kerja, jalinan hubungan kerjasama antara para pekerja dalam suatu wadah untuk mencapai tujuan tertentu.&nbsp;</li>\r\n<li><em>Manajemen, </em>berfungsi dalam merencanakan, mengorganisasikan, membina, membimbing, menggerakkan, dan mengawasi sekelompok orang serta mengerahkan segenap fasilitas untuk tujuan yang telah ditentukan dapat tercapai dengan baik.&nbsp;</li>\r\n<li><em>Komunikasi, </em>adalah kegiatan yang menyampaikan berita, pemberian ide, dan gagasan dari seseorang kepada orang lain, yang bersifat timbal balik antara pimpinan dengan pimpinan, pimpinan dengan bawahan, baik secara formal maupun yang nonformal.&nbsp;</li>\r\n<li><em>Informasi</em>&nbsp;adalah kegiatan yang menghimpun, mencatat, mengolah, menggandakan, menyebarkan, dan menyimpan berbagai informasi.&nbsp;</li>\r\n<li><em>Personalia, </em>ialah kegiatan yang mengatur dan mengolah penggunaan tenaga kerja</li>\r\n<li><em>Keuangan, </em>adalah kegiatan yang mengatur dan mengolah penggunaan sumber daya sekaligus yang dipertanggungjawaban penggunaan dana.</li>\r\n<li><em>Perbekalan, </em>adalah kegiatan yang merencanakan, mengurus dan mengatur dalam penggunaan peralatan kerja.&nbsp;</li>\r\n<li><em>Humas, </em>ialah kegiatan yang menciptakan hubungan dan dukungan yang baik dari lingkungan masyarakat terhadap sekitar perusahaan.&nbsp;</li>\r\n</ul>\r\n<div>\r\n<h3><strong>Tujaun Administrasi Perkantoran</strong></h3>\r\n</div>\r\n<strong>Tujuan Administrasi Perkantoran - </strong>Umumnya, administrasi perkantoran merupakan pekerjaan kantor yang bertujuan menghimpin, mencatat, mengelolah, menggandakan, mengirim dan menyimpan keterangan-keterangan yang diperlukan dalam kegiatan kantor. Tujuan-tujuan administrasi perkantoran adalah sebagai berikut...<br /><strong>a. Menghimpun, </strong>adalah suatu kegiatan-kegiatan yang mencari data, mengusahakan tersedianya segala informasi yang belum ada sehingga siap untuk digunakan jika diperlukan.<br /><strong>b. Mencatat, </strong>ialah kegiatan yang membutuhkan berbagai peralatan tulis informasi sehingga terwujud tulisan yang dapat dibaca, dikirm, dan disimpan.<br /><strong>c. Mengolah, </strong>yaitu kegiatan yang mengolah informasi dengan maksud untuk menyajikan dengan bentuk yang lebih berguna<br /><strong>d. Menggandakan, </strong>adalah kegiatan yang menyampaikan dengan berbagai cara dan alat.<br /><strong>e. Mengirim, </strong>ialah kegiatan yang menyampaikan dengan berbagai cara dan alat yang informasinya terdiri dari satu pihak kepada pihak yang lainnya. &nbsp;<strong>&nbsp;</strong><br /><strong>f. Menyimpan, </strong>adalah kegiatan yang menaruh informasi dengan sejumalh cara dan alat dalam suatu tempat yang aman.</div>\r\n<div>&nbsp;</div>\r\n<div>&nbsp;</div>\r\n<div>Sumber :&nbsp;http://www.artikelsiana.com/2015/08/pengertian-administrasi-perkantoran.html</div>', 3, 5, '2016-03-18 00:03:57'),
(6, 'Tips Membersihkan Laptop', 'Artikel_6_TipsMembersihkanLaptop.png', '<p style=\"text-align: justify;\">Seperti halnya tubuh kita yang harus senantiasa dirawat agar selalu tampil cantik dan sehat setiap saat, laptop ataupun notebook juga sangat perlu untuk kita rawat agar barang kita tersebut tampak selalu bersih dan kinclong. Di Artikel kali ini saya akan membagi tips cara membersihkan laptop ataupun notebook secara benar, aman, dan praktis agar tidak menimbulkan bekas pada kedua barang tersebut.</p>\r\n<ol>\r\n<li style=\"text-align: justify;\">Untuk membersihkan bagian luar atau bagian eksterior dari laptop, dapat menggunakan kapas basah yang telah direndam. Jangan menggunakan larutan pembersih rumah tangga dalam membersihkan bagian eksterior, gunakan alkohol untuk menghilangkan substansi yang tidak dapat dibersihkan dengan air pada laptop.</li>\r\n<li style=\"text-align: justify;\">Mencegah debu dari udara masuk dan keluar dari laptop. Ini tidak baik karena dapat menyebabkan overheating. Untuk menghilangkan debu, kotoran, rambut, atau zat lain dapat menggunakan kompresi udara atau menggunakan kapas/cotton but. Jika akan menggunakan udara bertekanan untuk membersihkan debu itu adalah ide yang baik, untuk tempat pertama yang harus dibersihkan sebuah benda kecil di antara bilah kipas untuk mencegah lebih dari berputar yang dapat merusak kipas pendingin laptop.</li>\r\n<li style=\"text-align: justify;\">Tidak seperti keyboard desktop, tombol keyboard laptop tidak dapat dibuka. Oleh karena itu saya menyarankan untuk membersihkanya menggunakan kain lembab. Selain itu, dapat juga menggunakan udara bertekananan untuk menyemprot semua debu, kotoran, atau rambut di-antara tombol. Anda juga dapat menggunakan penyedot debu kecil untuk menghisap dan menghapus setiap kotoran dan debu.</li>\r\n<li style=\"text-align: justify;\">Membersihkan touchpad juga penting. Untuk membersihkan touchpad, dapat menggunakan kain lembab untuk membersihkan permukaannya. Membersihkan touchpad laptop akan membantu meningkatkan sensitivitas touchpad laptop.</li>\r\n<li style=\"text-align: justify;\">Kotoran, debu, dan sidik jari dapat menyebabkan layar komputer laptop akan sulit untuk dibaca dan tentu saja tidak bagus. Membersihkan layar Laptop memerlukan prosedur pembersih khusus karena tidak seperti monitor CRT layar laptop tidak terbuat dari kaca. Dalam membersihkan LCD, jangan langsung menyemprotkan spray cairan pembersih pada layar. Jangan juga menggunakan handuk kertas pada layar LCD. Gunakan kapas lembut dalam membersihkan LCD. Anda dapat menggunakan alkohol untuk kain dan usap layar dengan kain lembab jika ada zat yang tidak bisa dihilangkan dengan kapas.</li>\r\n</ol>\r\n<p>Sebelum membersihkan laptop, baca dulu petunjuk pemakaian laptop anda. Biasanya disitu ada cara-cara yang dianjurkan untuk membersihkan. Pembersihan umumnya dilakukan setahun sekali. Tapi jika anda sering membawa laptop bepergian, bersihkan lebih sering.</p>\r\n<ul>\r\n<li style=\"text-align: justify;\">Gunakan kain katun yang lembut untuk membersihkan bagian luar laptop.</li>\r\n<li style=\"text-align: justify;\">Gunakan cairan pembersih computer yang dianjurkan.</li>\r\n<li style=\"text-align: justify;\">Caranya : campur satu bagian alcohol dengan satu bagian air, aduk rata.</li>\r\n<li style=\"text-align: justify;\">Semprotkan cairan pembersih pada lap, jangan langsung ke laptop. Usapkan lap perlahan sehingga debu dan kotoran yang melekat hilang.</li>\r\n<li style=\"text-align: justify;\">Bersihkan hingga kebagian yang tersembunyi, seperti lubang skrup.</li>\r\n</ul>\r\n<p>Layar</p>\r\n<ul>\r\n<li>Bersihkan layar hanya ketika laptop mati. Sebab, dalam keadaan layar gelap itulah kotoran terlihat jelas. Disamping itu untuk menghindari kerusakan.</li>\r\n<li>Gunakan kain lembut seperti katun untuk membersihkannya. Jangan handuk, serbet atau kain lain yang dapat meninggalkan serabut dan goresan pada laptop anda.</li>\r\n<li>Jangan menggunakan cairan ammonia untuk membersihkan layar, karena bisa memburamkan tampilan layar. Gunakan cairan khusus pembersih computer. semprotkan cairan itu pada lap, lantas usapkan kebagian yang hendak dibersihkan. Usap searah dari kiri kekanan atau dari atas ke bawah. Dan yg penting gunakan Screen Protector pada LCD nya agar bisa tahan lama.</li>\r\n</ul>\r\n<p>Keyboard</p>\r\n<ul>\r\n<li style=\"text-align: justify;\">Agar keyboard tidak cepat kotor, usahakan untuk tidak makan atau minum saat mengetik.</li>\r\n<li style=\"text-align: justify;\">Sebaiknya anda membersihkan keyboard saat laptop dalam keadaan mati. Jika tidak atur agar tombol-tombol tidak berfungsi.</li>\r\n<li style=\"text-align: justify;\">Gunakan kain lembut yang sudah dibasahi cairan pembersih untuk mebersihkannya. Kemudian ulangi dengan mengusapkan kain kering, atau bisa gunakan kuas halus.</li>\r\n<li style=\"text-align: justify;\">Sesekali gunakan vacuum cleaner kecil untuk menyedot kotoran. Dan jangan lupa gunakanKeyboard Protector agar tidak kotor &amp; terlindung dari air.</li>\r\n</ul>\r\n<p>Laptop yang berbeda juga memiliki cara yang berbeda untuk membongkarnya. Jika merasa ragu, saya sarankan untuk membawa laptop ke pusat perbaikan berwenang untuk memeriksa dan memberikan solusi yang paling cocok.</p>\r\n<p style=\"text-align: justify;\"><a class=\"example1\" href=\"https://andripriyanto.wordpress.com/2012/01/30/tips-membersihkan-laptop/\" target=\"_blank\">Sumber</a></p>', 1, 6, '2016-03-21 10:13:23'),
(5, 'Peluang Kerja Lulusan Informatika (RPL dan TKJ)', 'Artikel_5_PeluangKerjaLulusanInformatika(RPLdanTKJ).jpg', '<p><strong>Informatika</strong><strong>&nbsp;</strong>dapat diartikan sebagai suatu disiplin ilmu yang mempelajari transformasi berlambang yaitu data yang berbasis pada mesin komputasi. Mencakup beberapa bidang diantaranya sistem informasi, ilmu komputer, ilmu informasi, sistem komputer dan aplikasi-aplikasi pengembangan informatika yang lainnya. Informatika mempelajari struktur, sifat, dan interaksi dari beberapa sistem yang dipakai untuk mengumpulkan data, memproses dan menyimpan hasil pemrosesan tersebut,&nbsp; serta menampilkan dalam bentuk informasi.</p>\r\n<p>Peran Ilmu komputer yang merupakan bagian dari informatika lebih ditekankan pada pemrograman komputer dan rekayasa perangkat lunak (software).&nbsp; Berakar dari elektronika, matematika dan linguistik, basis ilmu komputer adalah pemahaman komprehensif mengenai algoritma. Mulai dari analisis abstrak hingga subyek yang lebih kongkret seperti struktur data, intelejensia buatan, sampai tata antarmuka pengguna. Bidang ini beririsan dengan bidang sistem informasi. Tetapi informatika lebih menitik beratkan kepada pemenuhan kebutuhan manusia yang berhubungan dengan penggunaan komputer. Oleh karena itu maka dipelajari berbagai strategi penerapan teknologi yang dimaksud, tanpa mendalami terlalu jauh konsep dan dasar teori dari basic ilmu komputer itu sendiri (pragmatis).</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Program studi Informatika memiliki 4 keminatan yaitu :</strong></p>\r\n<ul>\r\n<li>Rekayasa Perangkat Lunak (RPL)</li>\r\n<li>Komputasi Cerdas&nbsp; (KC)</li>\r\n<li>Jaringan Komputer (JK)</li>\r\n<li>Teknologi Game (Game Tech)</li>\r\n</ul>\r\n<p><br /><br /></p>\r\n<p>Dengan 4 keminatan diatas, diharapkan lulusan dari program studi informatika dapat mengaplikasikan kemampuannya, baik hard skill maupun soft skill di bebrbagai bidang dan lini profesionalitas. Beberapa prospek profesi yang terkait langsung dengan informatika adalah :<br /><br /><br /></p>\r\n<ol>\r\n<li><strong>Software Engineer</strong>&nbsp;-&nbsp;Berperan dalam pengembangan perangkat lunak untuk berbagai keperluan. Misalnya perangkat lunak untuk pendidikan, telekomunikasi, bisnis, hiburan dan lain-lain, termasuk perangkat lunak untuk model dan simulasi.</li>\r\n</ol>\r\n<ol>\r\n<li><strong>System Analyst dan System Integrator</strong>&nbsp;-&nbsp;Berperan dalam melakukan analisis terhadap sistem dalam suatu instansi atau perusahaan dan membuat solusi yang integratif dengan memanfaatkan perangkat lunak.</li>\r\n<li><strong>Konsultan IT</strong>&nbsp;-&nbsp;Berperan dalam perencanaan dan pengevaluasian penerapan IT pada sebuah organisasi.</li>\r\n<li><strong>Database Engineer / Database Administrator</strong>&nbsp;-&nbsp;Berperan dalam perancangan dan pemeliharaan basis data (termasuk data warehouse) untuk suatu instansi atau perusahaan.</li>\r\n<li><strong>Web Engineer / Web Administrator&nbsp;</strong>-&nbsp;Bertugas merancang dan membangun website beserta berbagai layanan dan fasilitas berjalan di atasnya. Ia juga bertugas melakukan pemeliharaan untuk website tersebut dan mengembangkannya.</li>\r\n<li><strong>Computer Network / Data Communication Engineer&nbsp;</strong>-&nbsp;Bertugas merancang arsitektur jaringan, serta melakukan perawatan dan &nbsp;pengelolaan jaringan dalam suatu &nbsp;instansi atau &nbsp;perusahaan.</li>\r\n<li><strong>Programmer</strong>&nbsp;-&nbsp;Baik sebagai system programmer atau application developer, sarjana informa tika sangat dibutuhkan di berbagai bidang, misalnya bidang perbankan, teleko munikasi, industri IT, media, instansi pemerintah, dan lain-lain.</li>\r\n<li><strong>Software Tester</strong>&nbsp;-&nbsp;Terkait dengan ukuran perangkat lunak, sarjana informatika dapat juga berperan khusus sebagai penguji perangkat lunak yang bertanggung jawab atas kebenaran fungsi dari sebuah perangkat lunak.</li>\r\n<li><strong>Game Developer</strong>&nbsp;-&nbsp;Dengan berbagai bekal keinformatikaan yang diperolehnya termasuk computer graphic, human computer interaction, dll, seorang sarjana informatika juga dapat berperan sebagai pengembang perangkat lunak untuk multimedia game.</li>\r\n<li><strong>Intelligent System Developer</strong>&nbsp;-&nbsp;Dengan berbagai teknik artificial intelligence yang dipelajarinya, seorang sarjana informatika juga dapat berperan sebagai pengembang perangkat lunak yang intelejen seperti sistem pakar, image recognizer, prediction system, data miner, dll.</li>\r\n</ol>\r\n<p>&nbsp;</p>\r\n<p>Sumber :&nbsp;http://adhityasulist.blogspot.co.id/2013/03/peluang-kerja-lulusan-teknik.html</p>', 1, 5, '2016-03-18 00:09:13'),
(7, 'Menginstall Service .NET Framework 3.5 di Windows 8 Secara Offline', '', '<p style=\"text-align: justify;\">Meskipun di windows 8 secara default sudah terinstall .NET Framework versi 4 namun ada beberapa aplikasi (terutama game) yang masih membutuhkan framework .NET versi 3.5 yang dimana di windows 8 ini masih belum terinstall. Ketika kita akan menggunakan program yang membutuhkan framework .NET 3.5 tentu saja harus direpotkan dengan instalalsi .NET nya dulu. Bagi yang memiliki akses internet super cepat tentu tidak begitu masalah karena tinggal install saja secara online, tapi bagaimana dengan yang fakir internet?</p>\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n<p style=\"text-align: justify;\">Langkah pertama, masukan DVD installer Windows 8 kedalam drive Anda.</p>\r\n<figure class=\"image\"><img class=\"example1\" src=\"http://www.digitaltrends.com/wp-content/uploads/2013/04/opticaldrive.jpg\" alt=\"Sumber gambar: http://www.digitaltrends.com\" width=\"500\" height=\"318\" />\r\n<figcaption>Sumber gambar: http://www.digitaltrends.com</figcaption>\r\n</figure>\r\n<p>&nbsp;</p>\r\n<p>Langkah kedua, buka command prompt dengan hak akses Administrator.</p>\r\n<figure class=\"image\"><img class=\"example1\" src=\"https://andripriyanto.files.wordpress.com/2014/07/tanpa-judul-2.jpg\" alt=\"Jalankan Command Prompt\" width=\"500\" height=\"281\" />\r\n<figcaption>Jalankan Command Prompt</figcaption>\r\n</figure>\r\n<p>&nbsp;</p>\r\n<p style=\"text-align: justify;\">&nbsp;Langkah ketiga, kemudian ketikkan perintah berikut ini:</p>\r\n<p style=\"text-align: center;\">DISM /Online /enable-feature /featurename:NetFX3 /All /Source:<strong>F</strong>:\\sources\\sxs /LimitAccess</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Perhatikan</strong>, pada baris&nbsp;F:\\sources\\sxs, ganti huruf F dengan huruf drive tempat DVD drive pada komputer Anda.</p>\r\n<p>&nbsp;</p>\r\n<p>Kemudian tunggulah proses nya hingga selesai. Jika sudah selesai, restart komputer Anda dan voila .NET sudah terinstall di komputer :D</p>\r\n<p><img class=\"example1\" src=\"https://andripriyanto.files.wordpress.com/2014/07/dism.png\" alt=\"\" width=\"500\" height=\"253\" /></p>', 1, 6, '2016-03-21 10:20:18'),
(8, 'FDM, Download Manager Alternatif Berlisensi GNU', '', '<p>Saat ini sudah banyak media penyimpanan yang mengandalkan internet atau yang lebih dikenal online storage. Dan dengan sangat populernya media ini maka kebutuhan untuk melakukan download dari internet pun meningkat sehingga banyak pula software-software<em>download manager</em> bertebaran di jagat internet. Kebanyakan sofware <em>download manager </em>ini kebanyakan menawarkan keunggulan di download yang lebih cepat, bisa melanjutkan download yang terputus, sampai pada proses download yang bisa dijadwalkan.<span id=\"more-1300\"></span></p>\r\n<p>Software seperti ini banyak yang sifatnya gratis namun memiliki fasilitas yang kurang begitu memuaskan, adapun <em>download manager </em>yang memiliki fitur canggih dan mudah penggunaanya tetapi software tersebut berharga yang lumayan mahal. Karena hal itu juga, banyak dari kita yang menggunakan <em>download manager </em>yang ilegal padahal penggunaan software ini sangatlah tidak beretika dan terlarang.</p>\r\n<p>Di tulisan saya kali ini saya akan berbagi tentang sebuah <em>download manager</em> yang memiliki fitur canggih tetapi tidak berbayar, alias gratis dengan lisensi GNU. <em>Download manager </em>ini bernama Free Download Manager. Yapz, sesuai dengan namanya &ldquo;Free&rdquo; software ini memang free dan sama sekali tidak berbayar. Lalu fitur apa saja kah yang ditawarkan oleh <em>download manager </em>ini? Berikut adalah beberapa fitur yang ditwarkan software ini:</p>\r\n<ul>\r\n<li>Berlisensi GNU</li>\r\n<li>BitTorrent support</li>\r\n<li>Remote Control</li>\r\n<li>Portable mode</li>\r\n<li>Enhanced audio/video files support</li>\r\n<li>100% gratis dan aman</li>\r\n<li>Akselerasi Download</li>\r\n<li>Melanjutkan download yang rusak</li>\r\n<li>Manajemen file yang lebih pintar dan penjadwal yang lebih powerful</li>\r\n<li>Pengaturan penggunaan traffic internet</li>\r\n<li>Site Explorer</li>\r\n<li>HTML Spider</li>\r\n<li>Download dari berbagai mirror sekaligus</li>\r\n<li>dan lain sebaginya</li>\r\n</ul>\r\n<p>Selain memiliki fitur yang lengkap seperti diatas, <em>download manager </em>ini juga mudah dalam segi penggunaan. Orang awam sekalipun akan segera terbiasa dengan <em>download manager </em>ini. Dan juga FDM ini memiliki fitur yang seperti <em>download manager</em> lain miliki, yaitu integrasi dengan browser.</p>\r\n<p>&nbsp;</p>\r\n<figure class=\"image align-center\"><img class=\"example1\" src=\"https://andripriyanto.files.wordpress.com/2013/11/untitled1.jpg\" alt=\"Free Download Manager\" width=\"436\" height=\"230\" />\r\n<figcaption>Free Download Manager</figcaption>\r\n</figure>\r\n<p>&nbsp;</p>\r\n<p>Yapz, itu sudah.. Apabila teman-teman sekalian terlalu tidak mampu untuk membeli software yang asli, sesungguhnya banyak sekali software alternatif yang bersifat gratis bahkan ada juga yang open source. Gunakan lah yang gratis itu, hargai kerja keras para developer yang telah bersusah payah men-develop software dengan memberikan beberapa pundi rupiah yang teman-teman miliki <span class=\"wp-smiley wp-emoji wp-emoji-smile\" title=\":)\">:)</span></p>\r\n<p>Yuks ah kita rame-rame beralih ke software legal. Karena untuk berbuat baik itu tidak susah, bisa dimulai dengan menggunakan software legal.</p>\r\n<p>Oh iya hampir lupa. Info lebih lanjut tentang Free Donwload Manager bisa teman-teman lihat di&nbsp;<a title=\"Free Download Manager\" href=\"http://freedownloadmanager.org/\" target=\"_blank\">sini</a>&nbsp;atau&nbsp;di <a title=\"Free Download Manager\" href=\"http://freedownloadmanager.org/\" target=\"_blank\">link</a> itu juga teman-teman bisa menemukan dimana software in bisa di download.</p>\r\n<figure class=\"image align-center\"><img class=\"example1\" src=\"https://andripriyanto.files.wordpress.com/2013/11/untitled.jpg\" alt=\"Free Download Manager\" width=\"433\" height=\"301\" />\r\n<figcaption>Free Download Manager</figcaption>\r\n</figure>\r\n<p>&nbsp;</p>\r\n<figure class=\"image align-center\"><img class=\"example1\" src=\"https://andripriyanto.files.wordpress.com/2013/11/untitled2.jpg\" alt=\"Tampilan Free Download Manager\" width=\"500\" height=\"281\" />\r\n<figcaption>Tampilan Free Download Manager</figcaption>\r\n</figure>\r\n<p><a class=\"example1\" href=\"https://andripriyanto.wordpress.com/2013/11/01/fdm-download-manager-alternatif-berlisensi-gnu\" target=\"_blank\">&nbsp;Sumber</a></p>', 1, 6, '2016-03-21 10:28:41'),
(9, 'Kingsoft, Solusi Aplikasi Office Gratis', '', '<p style=\"text-align: justify;\">Salah satu rahasia umum yang ada di negeri Indonesia ini adalah banyaknya kasus pembajakan terhadap software, bahkan beberapa sumber telah menyebutkan bahwa tingkat pembajakan software di negeri ini telah menembus 80%. Artinya apabila kamu adalah seorang developer software, misalkan penghasilan yang kamu dapat itu 100 juta kamu hanya dapat 20 juta saja.</p>\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n<p style=\"text-align: justify;\">Nah, kemudian apa korelasi judul artikel ini dengan kasus pembajakan yang telah dibahas sebelumnya? Apakah artikel ini akan membahas pembajakan? Hukum? Pencurian? Pencabulan? Oh, tentu tidak <span class=\"wp-smiley wp-emoji wp-emoji-bigsmile\" title=\":D\">:D</span>. Seperti yang telah diketahui oleh banyak orang, salah satu faktor tingginya angka pembajakan software di negeri ini adalah karena mahalnya harga software yang original, sementara software yang berharga mahal tersebut memang dibutuhkan.<span id=\"more-1280\"></span></p>\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n<p style=\"text-align: justify;\">Solusi yang telah mainstream adalah menggunakan software yang berlisensi GNU/GPL yang biasanya gratis atau menggunakan software yang memang tidak berbayar. Software-software seperti ini banyak sekali bisa ditemukan di internet, namun hal tersebut tidaklah banyak membantu dikarenakan software-software yang gratis tersebut biasanya tidak familiar dengan para pengguna komputer di negeri ini jadi mereka kembali lagi ke software yang bajakan.</p>\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n<p style=\"text-align: justify;\">Untuk mengatasi hal tersebut, kini telah ada software alternatif untuk aplikasi perkantoran yang tidak berbayar. Software tersebut bernama Kingsoft Office. Software ini sudah 1 paket dengan 3 aplikasi yang umum digunakan yaitu KIngsoft Office Writter untuk word processor nya, Kingsoft Office Spreadsheets untuk aplikasi spreadsheet, dan Kingsift Office Presentation untuk aplikasi pembuat presentasi. Selain gratis, software ini juga memiliki fitur yang sangat sama dengan aplikasi office buatan Microsoft dan juga aplikasi ini bisa membaca file yang dihasilkan dari aplikasi Microsoft Office (doc, docx, xls, xlsx, ppt, pptx, dan lain sebagainya). Selain itu aplikasi ini bisa dibilang jauh lebih ringan dibanding kan dengan aplikasi office lain yang sejenis. Dari segi interface juga Kingsoft office ini memiliki interface yang mirip dengan aplikasi Office keluaran dari Microsoft, sehingga apabila orang-orang yang telah terbiasa dengan Microsoft Office sekalipun akan mudah untuk beradaptasi dalam menggunakan aplikasi Kingsoft ini.</p>\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n<p style=\"text-align: justify;\">Sebenarnya aplikasi ini terdiri dari 2 versi, yaitu Pro (berbayar) dan free (gratis). Namun tidak ada perbedaan yang signifikan diantara keduanya. Buat kamu yang pingin nyoba dan tau lebih banyak lagi tentang Kingsoft ini bisa langsung saja kunjungi web nya di&nbsp;<a href=\"http://www.ksosoft.com/\">http://www.ksosoft.com/.</a></p>\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n<p style=\"text-align: justify;\">Berikut ada beberapa screen shoot Kingsoft.</p>\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n<figure class=\"image align-center\"><img class=\"example1\" src=\"https://andripriyanto.files.wordpress.com/2013/08/1.png\" alt=\"Kingsoft Writter\" width=\"500\" height=\"281\" />\r\n<figcaption>Kingsoft Writter</figcaption>\r\n</figure>\r\n<p>&nbsp;</p>\r\n<figure class=\"image align-center\"><img class=\"example1\" src=\"https://andripriyanto.files.wordpress.com/2013/08/2.png\" alt=\"Kingsoft Spreadsheet\" width=\"500\" height=\"281\" />\r\n<figcaption>Kingsoft Spreadsheet</figcaption>\r\n</figure>\r\n<p>&nbsp;</p>\r\n<figure class=\"image align-center\"><img class=\"example1\" src=\"https://andripriyanto.files.wordpress.com/2013/08/3.png\" alt=\"Kingsoft Office Presentation\" width=\"500\" height=\"281\" />\r\n<figcaption>Kingsoft Office Presentation</figcaption>\r\n</figure>\r\n<p style=\"text-align: justify;\"><a class=\"example1\" href=\"https://andripriyanto.wordpress.com/2013/08/30/kingsoft-solusi-aplikasi-office-gratis\" target=\"_blank\">Sumber</a></p>', 1, 6, '2016-03-21 10:34:10'),
(10, 'Penyebab Umum yang Memperlambat Kinerja Komputer', 'Artikel_PenyebabUmumyangMemperlambatKinerjaKomputer.jpg', '<p style=\"text-align: justify;\" align=\"justify\">Sebagai pengguna komputer, tentu kita semua ingin agar kinerja komputer yang kita gunakan bagus, respon yang cepat, tanpa harus menunggu lama untuk mengakses data, dan lain sebagainya. Oleh karena itu ada baiknya kita mengetahui apa saja yang menyebabkan atau berpengaruh dengan lambatnya komputer kita. Berikut pembahasan ringkas hal-hal yang paling sering menyebabkan kinerja komputer menjadi lambat plus tips mengatasinya.</p>\r\n<ol>\r\n<li style=\"text-align: justify;\"><span id=\"more-509\"></span><strong>Malware (virus, worm, trojan, dsb)</strong><br />Ketika komputer kita terkena malware ( virus, worm, trojan, dan sejenisnya), sudah hampir dapat dipastikan bahwa kinerja komputer akan lambat. Virus menggunakan banyak resources komputer baik RAM atau CPU, termasuk selalu memantau aktivitas kita selama menggunakan komputer. Hal ini tentu sangat berpengaruh terhadap kinerja aplikasi lain. Solusi untuk mencegah ini bagi pengguna Windows adalah menginstall Antivirus dan tidak hanya berhenti disitu saja, tetapi rutin update antivirus tersebut. Jadwalkan paling tidak seminggu sekali jika komputer kita tidak online.<br /><br /></li>\r\n<li style=\"text-align: justify;\"><strong>Spyware, Adware, dan sesuatu yang sejenisnya</strong><br />Jika kita sering menggunakan komputer untuk ber-internet, jika tidak berhati-hati maka akan ada kemungkinan komputer bisa terkena spyware. Efeknya mungkin tidak begitu besar dengan kinerja komputer, tetapi bisa berpengaruh pada akses internet, dan berbagai hal yang menganggu kenyamanan berinternet. Dan yang lebih buruk, data-data penting (user, password, account dll) kita bisa di ketahui oleh si pembuat spyware ini. Solusinya kita bisa menginstall Anti-spyware yang juga harus selalu diupdate, hanya saja perlu dipilah-pilah mana yang tidak banyak menggunakan resources komputer kita, karena tidak jarang Antispyware ini menggunakan CPU dan Memory yang cukup besar. Jika antivirus sudah menyertakanya, kita tidak perlu menambah. Atau gunakan saja yang versi portable, dan scan dari spyware secara berkala.<br /><br /></li>\r\n<li style=\"text-align: justify;\"><strong>Banyaknya aplikasi yang berjalan background</strong><br />Semakin banyak kita menggunakan program, biasanya akan semakin memperlambat kinerja komputer, meskipun pengaruhnya ada yang relatif kecil dan ada yang besar. Penting untuk diketahui ketika menginstall software, cek apakah ada aplikasi yang senantiasa berjalan di belakang (background). Hal ini bisa di ketahui dengan program seperti Autoruns ataupun Windows Task Manager. Dalam hal ini solusinya adalah menggunakan sofware yang penting saja, pilih satu software jika ada beberapa software sejenis atau mempunyai fitur hampir sama dan jika ada versi Portable-nya maka bisa menjadi alternatif. Untuk mengurangi program yang berjalan di background, gunakan Autoruns, dan non aktifkan aplikasi background yang tidak penting. Untuk mengatahui apakah aplikasi yang di install akan menjalankan program di belakang, install software seperti WinPatrol.<br /><br /></li>\r\n<li style=\"text-align: justify;\"><strong>Hardisk yang sudah tua</strong><br />Ketika komputer kita masih menggunakan Hardisk yang sudah cukup lama (tua), mungkin lebih dari 5 tahun, maka kinerja komputer bisa semakin lambat. Untuk mengecek, kita bisa menggunakan software gratis HDD Tune atau sejenisnya. HDD SATA normal biasanya rata-rata akses read (baca) sekitar 70 &ndash; 90 MB/s. Jika misal rata-rata akses HDD dibawah 50 MB/s maka kinerja biasanya akan terasa lambat. Solusi ketika hardisk sudah sangat lambat, mungkin bisa dicoba dengan full format (backup data penting Anda terlebih dahulu). Meskipun untuk hardisk tua hal ini biasanya tidak akan banyak membantu, sehingga yang paling baik adalah dengan mengganti hardisk baru, dan jika masih ingin menggunakan hardisk lama, gunakan sebagai secondary hardisk saja.<br /><br /></li>\r\n<li style=\"text-align: justify;\"><strong>RAM yang kurang memadai (ngepas banget)</strong> Banyak sedikitnya jumlah RAM yang kita gunakan memang tidak bisa dibuat standard sama untuk satu komputer dengan komputer lain atau bahkan sistem operasi. Meskipun ketika akan menginstall Windows ada spesifikasi minimal RAM, tetapi jenis aplikasi yang kita gunakan juga harus diperhitungkan. Untuk mengecek, buka saja Task Manager dan di bagian Performance periksa PF Usage dan juga Physical Memory yang menunjukkan total Memory fisik (RAM) dan sisa tersedia (Available).<br /><br />\r\n<table border=\"0\" align=\"center\">\r\n<tbody>\r\n<tr>\r\n<td align=\"center\" valign=\"middle\"><img src=\"https://andripriyanto.files.wordpress.com/2011/11/komputerlambat.jpg?w=357&amp;h=159\" alt=\"Penyebab Umum yang Memperlambat Kinerja Komputer\" width=\"357\" height=\"159\" border=\"1\" /></td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>Jika kita tidak sedang menjalankan aplikasi apapun, tetapi sisa RAM tidak lebih dari setengahnya, biasanya kinerja komputer akan lambat, maka harus diperiksa aplikasi apa saja yang menggunakan banyak memory (RAM), secara umum bisa dicek di tab Process, kolom Mem Usage. Jika memang RAM kita pas-pasan (misal windows XP dengan RAM kurang dari 256 MB, atau windows 7 dengan RAM kurang dari 512 MB), maka solusi termurah adalah mengurangi aplikasi yang banyak memakan banyak memory. Dan solusi terbaik adalah upgrade RAM.</p>\r\n</li>\r\n<li><strong>Konflik aplikasi atau program yang di install</strong><br />Tidak jarang dua aplikasi dalam kategori yang sama bisa berakibat terjadinya konflik, yang semakin memperlambat kinerja komputer. Tanda-tanda terjadi konflik adakan komputer yang bermasalah setelah kita menginstall suatu software, padahal sebelumnya tidak ada masalah. Yang sering terjadi konflik biasanya di kategori software security, semisal antivirus. Misalnya kita menggunakan 2 antivirus atau lebih. Meskipun beberapa antivirus bisa berjalan bersamaan, tetapi tetap tidak direkomendasikan, kecuali untuk pengguna ahli atau untuk ujicoba. Belum lagi ketika masih harus menginstall software security lainnya. Jika ada alternatif berbagai software sejenis, maka jika memungkinkan pilih satu saja yang bisa mewakili, dan unggul dalam kinerja dan hasil. Terutama untuk jenis software yang banyak mengakses sumber daya atau sistem operasi.<br /><br /></li>\r\n<li><strong>Pemilihan software yang kurang tepat</strong><br />Tidak sedikit orang hanya ikut-ikutan (trend) dalam penggunaan software, padahal fitur yang diinginkan sebenarnya terdapat dalam software lain yang kecil dan gratis. Jika spesifikasi komputer kita memang minimal atau kita ingin bekerja dengan cepat, maka pilihlah software yang tepat. Software dengan ukuran besar tidak senantiasa lebih baik dan tepat bagi masing-masing kita. Berikut beberapa contohnya, ketika kita hanya ingin burning data ke CD/DVD solusi yang tepat bisa menggunakan software ImgBurn yang hanya berukuran sekitar 5 MB atau software burning gratis lainnya yang relatif kecil daripada menginstall Nero Multimedia Suite yang berukuran sekitar 354 MB. Atau ketika kita bekerja dengan data terkompresi (zip/rar), software gratis seperti 7zip yang hanya berukuran sekitar 1 MB seharusnya sudah mencukupi, daripada menginstall Winzip 15 yang berukuran hampir 13 MB dan juga tidak gratis. Atau jika menggunakan Adobe Photoshop 7 atau CS1/2 sudah mencukupi untuk kebutuhan grafis, maka menginstall Photoshop CS5 perlu difikir ulang, karena spesifikasi yang dibutuhkan cukup tinggi, sehingga kerja bisa semakin lambat.<br /><br /></li>\r\n<li><strong>Banyaknya software yang terinstall</strong><br />Meskipun software-software yang di install tidak berjalan di belakang, tetapi hampir setiap software selalu menambahkan entry (data) ke registry, sehingga semakin banyak software yang di install ukuran registry (windows) juga akan semakin besar. Karena registry ini akan di akses baik ketika komputer berjalan maupun sudah berjalan, besar kecilnya juga mempengaruhi ke kecepatan/ waktu respon-nya. Solusinya adalah menggunakan software yang memang diperlukan saja, Uninstall software yang tidak penting dan gunakan Uninstaller seperti Revo Uninstaller agar proses uninstallasi lebih tuntas. Untuk membersihkan software yang sudah di uninstall, bisa juga menggunakan berbagai Utilities gratis.<br /><br /></li>\r\n<li><strong>Penggunaan efek Windows yang berlebih</strong><br />Windows XP, Vista, maupun seven menyediakan opsi untuk menggunakan tampilan dengan berbagai efek. Jika komputer kita mempunyai spesifikasi yang bagus, tentu berbagai efek ini tidak menjadi masalah, tetapi jika ingin performa cepat, berbagai efek windows bisa di non-aktifkan. Misalnya Untuk windows XP, klik kanan My Computer, pilih tab Advanced dan klik setting bagian Performance, lalu pilih Adjust for best performance.\r\n<p>&nbsp;</p>\r\n<table border=\"0\" align=\"center\">\r\n<tbody>\r\n<tr>\r\n<td align=\"center\" valign=\"middle\"><img src=\"https://andripriyanto.files.wordpress.com/2011/11/komputerlambat2.jpg?w=367&amp;h=535\" alt=\"Penyebab Umum yang Memperlambat Kinerja Komputer\" width=\"367\" height=\"535\" border=\"1\" /></td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>Kita juga bisa melakukan tweak sistem untuk mendapatkan setting yang tepat dan cepat, dengan menggunakan software semisal X-Setup Pro, TuneUp Utilities, TweakXP, dll.</p>\r\n</li>\r\n</ol>\r\n<p>Sebenarnya selain 9 hal diatas masih banyak sebab lain, seperti berbagai service windows yang berjalan yang sebenarnya tidak diperlukan, pemilihan dan pengaturan hardware yang tidak optimal, space primary disk (misalnya drive C:) yang diambang batas, dan lain sebagainya.</p>\r\n<p><a class=\"example1\" href=\"https://andripriyanto.wordpress.com/2011/11/15/penyebab-umum-yang-memperlambat-kinerja-komputer\" target=\"_blank\">Sumber</a></p>', 6, 6, '2016-03-21 10:44:20'),
(11, 'Belajar Matematika dengan MIcrosoft Mathematics', 'Artikel_11_BelajarMatematikadenganMIcrosoftMathematics.gif', '<p style=\"text-align: justify;\" align=\"justify\">Jika anda merupakan seorang siswa, pelajar atau Mahasiswa atau orang yang ingin belajar matematika dengan mudah, kini ada salah satu program gratis yang bisa mendukung dan membantu, Microsoft Mathematics.</p>\r\n<p align=\"justify\">&nbsp;</p>\r\n<p align=\"center\"><img src=\"https://andripriyanto.files.wordpress.com/2011/11/microsoft_math.jpg?w=422&amp;h=201\" alt=\"Belajar Matematika dengan Microsoft Mathematics\" width=\"422\" height=\"201\" border=\"0\" /></p>\r\n<p style=\"text-align: justify;\" align=\"justify\">&nbsp;</p>\r\n<p style=\"text-align: justify;\" align=\"justify\">Microsoft Mathematics merupakan tools yang dapat membantu para siswa maupun mahasiswa untuk menyelesaikan tugas matematika dengan cepat dan mudah. Dengan Microsoft Mathematics para siswa dapat belajar untuk memecahkan rumus secara bertahap, dapat lebih mengerti konsep aljabar, trigonometri, fisika, kimia, dan kalkulus.</p>\r\n<p style=\"text-align: justify;\" align=\"justify\">&nbsp;</p>\r\n<p align=\"center\"><img src=\"https://andripriyanto.files.wordpress.com/2011/11/microsoft_math2.jpg?w=621&amp;h=479\" alt=\"Belajar Matematika dengan MIcrosoft Mathematics\" width=\"621\" height=\"479\" border=\"0\" /></p>\r\n<p align=\"justify\">&nbsp;</p>\r\n<p align=\"justify\">Microsoft Mathematics dapat mengerjakan berbagai fungsi matematika, seperti:</p>\r\n<ul>\r\n<li>Melakukan penghitungan matematika standard seperti akar dan logaritma</li>\r\n<li>Menyelesaikan operasi persamaan dan juga pertidaksamaan</li>\r\n<li>Menyelesaikan aturan segitiga</li>\r\n<li>Melakukan konversi dari suatu satuan ke bentuk satuan lain</li>\r\n<li>Melakukan penghitungan trigonometri, seperti sinus, atau cosinus.</li>\r\n<li>Operasi matriks dan vektor</li>\r\n<li>Statistika dasar</li>\r\n<li>Operasi kompleks</li>\r\n<li>Menggambar grafik 2D maupun 3D dalam diagram kartesius</li>\r\n<li>Operasi turunan, integral, dan limit</li>\r\n<li>Menyelesaikan rumus-rumus dan persamaan umum</li>\r\n</ul>\r\n<p align=\"justify\">&nbsp;</p>\r\n<p align=\"justify\">Saat artikel ini ditulis, Microsoft Technet baru saja merilis versi baru program ini, yaitu Microsoft Mathematics 4.0. Software ini bisa kita download secara gratis, dengan ukuran 17.6 MB untuk windows 32bit serta 18.9 MB untuk windows 64 bit. Software ini bisa berjalan di Windows 7, Windows Vista (SP2), Windows XP (32-bit) (SP3), Windows Server 2008 R2 (64-bit), Windows Server 2008 (SP2), dan Windows Server 2003 (32-bit) (SP2). Selain itu, untuk menjalankan program ini, di komputer harus sudah terinstall Microsoft dotNET Framework 3.5 SP1 dan juga spesifikasi minimal komputer kita adalah minimal CPU/Processor 500 MHz, RAM/Memory minimal 256 MB, VGA card (Video) 64 MB, serta tampilan layar minimal 800&times;600.</p>\r\n<p align=\"justify\">Microsoft Mathematics bisa didownload di&nbsp;<a title=\"Download Microsoft Mathematic\" href=\"http://www.4shared.com/file/WIB8_U5N/Microsoft_Mathematics_40.html\" target=\"_blank\">sini</a>&nbsp;atau&nbsp;di <a title=\"Download Microsoft Mathematics\" href=\"http://bit.ly/MSMath\" target=\"_blank\">sini</a>.</p>\r\n<p align=\"justify\">&nbsp;</p>\r\n<p align=\"justify\"><a class=\"example1\" href=\"https://andripriyanto.wordpress.com/2011/11/08/belajar-matematika-dengan-microsoft-mathematics\" target=\"_blank\">Sumber</a></p>', 6, 6, '2016-03-21 10:55:55');
INSERT INTO `artikel` (`id_artikel`, `judul_artikel`, `gambar_utama`, `artikel`, `id_kategori`, `id_user`, `tgl_posting`) VALUES
(12, 'Membuat Daftar Isi Menggunakan Microsoft Word', 'Artikel_12_MembuatDaftarIsiMenggunakanMicrosoftWord.png', '<p align=\"justify\">Biasanya dalam pembuatan makalah ataupun karya ilmiah lainya, sering kali kita menyimpan daftar isi, cover, dan isi secara terpisah agar pengaturan halaman tidak terlalu ribet. Di artikel ini akan saya jelaskan bagaimana cara menyatukan file-file tersebut kedalam satu file sekailgus mengatur format penomoran halaman dan juga pembuatan daftar isi secara otomatis.<span id=\"more-995\"></span></p>\r\n<p align=\"justify\">&nbsp;</p>\r\n<p align=\"center\"><a href=\"https://andripriyanto.files.wordpress.com/2012/01/word_daftar_isi.jpg\"><img src=\"https://andripriyanto.files.wordpress.com/2012/01/word_daftar_isi.jpg?w=437&amp;h=113\" alt=\"\" width=\"437\" height=\"113\" /></a></p>\r\n<p align=\"justify\"><strong>Langkah pertama</strong>, jika file makalah masih dalam keadaan terpisah antara cover, daftar isi, dan isi makalahnya, maka satukan dahulu semua isinya menjadi satu file.</p>\r\n<p align=\"center\"><a href=\"https://andripriyanto.files.wordpress.com/2012/01/word_daftar_isi2.jpg\"><img src=\"https://andripriyanto.files.wordpress.com/2012/01/word_daftar_isi2.jpg?w=543&amp;h=287\" alt=\"\" width=\"543\" height=\"287\" /></a></p>\r\n<p align=\"justify\">&nbsp;</p>\r\n<p align=\"justify\"><strong>Langkah kedua</strong>, berikan nomor halaman pada makalah tersebut dengan cara klik tab ribbon Insert -&gt; Group Header &amp; Footer -&gt; Bottom of page -&gt; Plain Number 3. Setelah langkah tersebut dikerjakan, maka seluruh halaman akan diberi nomor halaman pada bagian kiri bawah termasuk pada halaman cover.</p>\r\n<p align=\"center\"><a href=\"https://andripriyanto.files.wordpress.com/2012/01/word_daftar_isi2_2.jpg\"><img src=\"https://andripriyanto.files.wordpress.com/2012/01/word_daftar_isi2_2.jpg?w=392&amp;h=266\" alt=\"\" width=\"392\" height=\"266\" /></a></p>\r\n<p align=\"justify\">&nbsp;</p>\r\n<p align=\"justify\"><strong>Langkah ketiga</strong>, letakkan kursor pada halaman 1.</p>\r\n<p align=\"center\"><a href=\"https://andripriyanto.files.wordpress.com/2012/01/word_daftar_isi3.jpg\"><img src=\"https://andripriyanto.files.wordpress.com/2012/01/word_daftar_isi3.jpg?w=503&amp;h=189\" alt=\"\" width=\"503\" height=\"189\" /></a></p>\r\n<p align=\"justify\">&nbsp;</p>\r\n<p align=\"justify\">Kemudian klik [Page Layout] -&gt; [Breaks] -&gt; [Next Page]</p>\r\n<p align=\"center\"><a href=\"https://andripriyanto.files.wordpress.com/2012/01/word_daftar_isi4.jpg\"><img src=\"https://andripriyanto.files.wordpress.com/2012/01/word_daftar_isi4.jpg?w=306&amp;h=239\" alt=\"\" width=\"306\" height=\"239\" /></a></p>\r\n<p align=\"justify\">&nbsp;</p>\r\n<p align=\"justify\">Setelah itu, klik dua kali pada footer halaman kedua lalu klik tombol Link to Previous untuk mematikan fungsi tersebut.</p>\r\n<p align=\"center\"><a href=\"https://andripriyanto.files.wordpress.com/2012/01/word_daftar_isi5.jpg\"><img src=\"https://andripriyanto.files.wordpress.com/2012/01/word_daftar_isi5.jpg?w=198&amp;h=111\" alt=\"\" width=\"198\" height=\"111\" /></a></p>\r\n<p align=\"justify\">&nbsp;</p>\r\n<p align=\"justify\">Selanjutnya adalah hapus secara manual nomor halaman pada halaman cover. Kemudian klik tombol Close Header &amp; Footer pada ribbon Header &amp; Footer Tools.</p>\r\n<p align=\"center\"><a href=\"https://andripriyanto.files.wordpress.com/2012/01/word_daftar_isi6.jpg\"><img src=\"https://andripriyanto.files.wordpress.com/2012/01/word_daftar_isi6.jpg?w=502&amp;h=177\" alt=\"\" width=\"502\" height=\"177\" /></a></p>\r\n<p align=\"justify\">&nbsp;</p>\r\n<p align=\"justify\"><strong>Langkah keempat</strong>, pada langkah ini, kita akan membuat format nomor halaman untuk KATA PENGANTAR dan DAFTAR isi dimana formatnya adalah berbentuk angka romawi (i, ii, iii, dst..). Letakkan kursor pada halaman KATA PENGANTAR, klik [Insert] -&gt; [Page Number] -&gt; [Format Page Number].</p>\r\n<p align=\"center\"><a href=\"https://andripriyanto.files.wordpress.com/2012/01/word_daftar_isi7.jpg\"><img src=\"https://andripriyanto.files.wordpress.com/2012/01/word_daftar_isi7.jpg?w=197&amp;h=222\" alt=\"\" width=\"197\" height=\"222\" /></a></p>\r\n<p align=\"justify\">&nbsp;</p>\r\n<p align=\"justify\">Kemudian pada kotak dialog Page Number Format, pilih tipe nomor pada bagian number format, jika sudah klik OK.</p>\r\n<p align=\"center\"><a href=\"https://andripriyanto.files.wordpress.com/2012/01/word_daftar_isi8.jpg\"><img src=\"https://andripriyanto.files.wordpress.com/2012/01/word_daftar_isi8.jpg?w=258&amp;h=275\" alt=\"\" width=\"258\" height=\"275\" /></a></p>\r\n<p align=\"justify\">&nbsp;</p>\r\n<p align=\"justify\">Setelah itu seluruh halaman akan memiliki format nomor yang sama, yaitu menggunakan angka romawi. Nah untuk merubah format penomoran untuk halaman BAB I, BAB II, dan seterusnya yang menggunakan nomor angka arab (1, 2, 3, dst..) yang dilakukan pertama membuat lagi Section Break. Caranya masih sama dengan langkah ketiga, hanya saja kursor ditaruh di paling atas pada halaman BAB I. Dan untuk merubah format nomornya juga sama seperti langkah keempat.</p>\r\n<p align=\"justify\">&nbsp;</p>\r\n<p align=\"justify\"><strong>Langkah kelima</strong>, setelah kita menuliskan nomor halaman untuk setiap halaman yang ada, berikutnya adalah membuat daftar isi otomatis, sehingga memungkinkan kita dapat mengupdate secara langsung jika terdapat perubah pada bagian ISI. Misalnya saja penambahan jumlah halaman, perbuhana urutan halaman, dan lain sebagainya.</p>\r\n<p>&nbsp;</p>\r\n<p>Blok judul lalu klik ribbon [Home] -&gt; [Group Style] -&gt; klik tanda panah dibawah nya -&gt; [Save Selection as a New Quick Style]. Kemudian beri nama dengan nama bebas, misal &ldquo;judul&rdquo;</p>\r\n<p align=\"center\"><img src=\"https://andripriyanto.files.wordpress.com/2012/01/word_daftar_isi9.jpg?w=247&amp;h=262\" alt=\"\" width=\"247\" height=\"262\" /></p>\r\n<p style=\"text-align: center;\"><a href=\"https://andripriyanto.files.wordpress.com/2012/01/word_daftar_isi10.jpg\"><img class=\"aligncenter\" src=\"https://andripriyanto.files.wordpress.com/2012/01/word_daftar_isi10.jpg?w=353&amp;h=177\" alt=\"\" width=\"353\" height=\"177\" /></a></p>\r\n<p align=\"justify\">&nbsp;</p>\r\n<p align=\"justify\">Masih dalam keadaan teks di blok, masuk ke Ribbon [Refference] -&gt; [Add Text] -&gt; [Level 1].</p>\r\n<p align=\"center\"><a href=\"https://andripriyanto.files.wordpress.com/2012/01/word_daftar_isi11.jpg\"><img src=\"https://andripriyanto.files.wordpress.com/2012/01/word_daftar_isi11.jpg?w=229&amp;h=135\" alt=\"\" width=\"229\" height=\"135\" /></a></p>\r\n<p align=\"justify\">&nbsp;</p>\r\n<p align=\"justify\">Lakukan hal tersebut untuk seluruh judul. Dan untuk sub judul atur sebagai level 2. Serta sesuaikan untuk level-level yang lain apabila memang lebih dari 3 level.</p>\r\n<p align=\"justify\">&nbsp;</p>\r\n<p align=\"justify\"><strong>Langkah keenam</strong>, kosongkan isi dari halaman DAFTAR ISI kecuali judulnya.</p>\r\n<p align=\"center\"><a href=\"https://andripriyanto.files.wordpress.com/2012/01/word_daftar_isi12.jpg\"><img src=\"https://andripriyanto.files.wordpress.com/2012/01/word_daftar_isi12.jpg?w=484&amp;h=363\" alt=\"\" width=\"484\" height=\"363\" /></a></p>\r\n<p align=\"justify\">Kemudian klik ribbon [Refferences] -&gt; [Table of Contents] -&gt; [Insert Table of Contents]</p>\r\n<p align=\"center\"><a href=\"https://andripriyanto.files.wordpress.com/2012/01/word_daftar_isi13.jpg\"><img src=\"https://andripriyanto.files.wordpress.com/2012/01/word_daftar_isi13.jpg?w=415&amp;h=365\" alt=\"\" width=\"415\" height=\"365\" /></a></p>\r\n<p align=\"justify\">Atur pada bagian Show Level, seberapa banyak level yang digunakan. Lalu klik OK.</p>\r\n<p align=\"center\"><a href=\"https://andripriyanto.files.wordpress.com/2012/01/word_daftar_isi14.png\"><img src=\"https://andripriyanto.files.wordpress.com/2012/01/word_daftar_isi14.png?w=376&amp;h=300\" alt=\"\" width=\"376\" height=\"300\" /></a></p>\r\n<p align=\"center\">&nbsp;</p>\r\n<p style=\"text-align: left;\" align=\"center\"><a class=\"example1\" href=\"https://andripriyanto.wordpress.com/2012/01/29/membuat-daftar-isi-menggunakan-microsoft-word/\" target=\"_blank\">Sumber</a></p>', 1, 6, '2016-03-21 11:03:11');

-- --------------------------------------------------------

--
-- Table structure for table `berita`
--

CREATE TABLE `berita` (
  `id_berita` int(11) NOT NULL,
  `judul_berita` varchar(150) DEFAULT NULL,
  `berita` text,
  `create_dt` datetime DEFAULT CURRENT_TIMESTAMP,
  `create_by` varchar(33) NOT NULL,
  `modify_dt` datetime DEFAULT CURRENT_TIMESTAMP,
  `modify_by` varchar(33) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `berita`
--

INSERT INTO `berita` (`id_berita`, `judul_berita`, `berita`, `create_dt`, `create_by`, `modify_dt`, `modify_by`) VALUES
(12, 'Ujian Nasional : Jujur...!!!', 'SMK BPI berkomitmen melaksanakan Ujian Nasional Berbasis Komputer (UNBK) dengan Jujur.', '2019-11-26 20:36:11', '', '2019-11-26 20:36:11', ''),
(30, 'Festival Kesenian SMK BPI Bandung', 'Festival kesenian SMK diselenggarakan untuk memenuhi nilai Ujian Praktek mata pelajaran Seni Budaya yang digagas oleh guru Seni Budaya SMK BPI Bandung Bapak Asep Ediyana S.Sn dengan menampilkan.', '2019-11-27 11:37:35', 'smkbisch', '2019-11-27 11:37:35', ''),
(31, 'Pelaksanaan Serentak Ujian Praktek', 'Untuk mendapatkan ilmu yang dapat diterapkan di dunia nyata dan Industri, SMK BPI Bandung mengadakan Ujian Praktek disemua bidang keahlian secara serentak.', '2019-11-27 11:38:03', 'smkbisch', '2019-11-27 11:38:03', ''),
(32, 'Simulasi UNBK SMK BPI Bandung', 'Dengan Teknologi yang ada saat ini, Demi mensukseskan Ujian Nasional menjadi lebih baik lagi, Tim Kurikulum SMK BPI Bandung mengadakan Simulasi Ujian Nasional Berbasis Komputer.', '2019-11-27 11:38:27', 'smkbisch', '2019-11-27 11:38:27', ''),
(33, 'Rapat IHT SMK BPI Bandung', 'Untuk Persiapan yang lebih matang lagi, Tim Pengembang Sekolah SMK BPI mengadakan rapat In House Training pada hari Jum\'at, Siswa diliburkan untuk sementara dikarenakan agenda rapat ini antara lain : SOP Siswa dan Guru, PPDB, Unit Produksi, dan lain lain.', '2019-11-27 11:38:50', 'smkbisch', '2019-11-27 11:38:50', ''),
(34, 'Gathering SMK BPI Bandung', 'Gathering Kepegawaian yang dilakukan sebagai akhir dari Rapat In House Training SMK BPI dilaksanakan di Grafika Cikole, Lembang, Kabupaten Bandung Barat.', '2019-11-27 11:39:21', 'smkbisch', '2019-11-27 11:39:21', ''),
(35, 'Beauty Class SMK BPI Bandung', 'Program Beauty Class Paket Keahlian Administrasi Perkantoran di SMK BPI Bandung berlangsung seru, Rabu (18/01/2017).', '2019-11-27 11:39:44', 'smkbisch', '2019-11-27 11:39:44', ''),
(36, 'Pelaksanaan UJIKOM Bidang Keahlian AP dan TKJ', 'Uji Kompetensi Keahlian pada SMK BPI BANDUNG Tahun Pelajaran 2016/2017 merupakan bagian Ujian Nasional. Hasil uji kompetensi menjadi indikator ketercapaian standar kompetensi lulusan yang tertuang dalam permen tentang skl kurikulum 2013.', '2019-11-27 11:40:10', 'smkbisch', '2019-11-27 11:40:10', ''),
(37, 'Acara Android Indonesia Kejar', 'Acara antar SMK BPI khususnya untuk Program Keahlian Rekayasa Perangkat Lunak bekerja sama dengan tim Panitia Android Indonesia Kejar mengadakan Workshop Android untuk pemula maupun advance android programming.', '2019-11-27 11:40:31', 'smkbisch', '2019-11-27 11:40:31', ''),
(38, 'Dirgahayu Republik Indonesia ke- 72', 'Dirgahayu Republik Indonesia ke-72, Makin jaya Indonesiaku, SMK BPI Bandung akan mendukung mu dalam bidang pendidikan dan memajukan pendidikan Indonesia.', '2019-11-27 11:40:56', 'smkbisch', '2019-11-27 11:40:56', ''),
(39, 'Dirgahayu Yayasan BPI ke- 69', 'Dirgahayu Yayasan BPI ke-69, Yayasan Badan Perguruan Indonesia telah berusia 69 tahun berdiri untuk mengabdikan kepada indonesia dalam bidang pendidikan.', '2019-11-27 11:41:25', 'smkbisch', '2019-11-27 11:41:25', ''),
(40, 'Gebyar Kemerdekaan SMK BPI', 'SMK BPI Bandung mengadakan acara bernama Gebyar Kemerdekaan sebagai memperingati hari kemerdakaan indonesia, dengan menampilkan ekstrakurikuler yang ada di smk bpi.', '2019-11-27 11:41:54', 'smkbisch', '2019-11-27 11:41:54', ''),
(41, 'Memperingati Hari Pahlawan di SMK BPI', 'SMK BPI Bandung mengadakan Upacara bendera pada hari senin, dan dilanjutkan oleh upacara kembali di gedung merdeka asia afrika untuk memperingati hari pahlawan nasional Republik Indonesia .', '2019-11-27 11:42:17', 'smkbisch', '2019-11-27 11:42:17', ''),
(42, 'Selamat Tahun Baru 2018', 'Segenap civitas SMK BPI Bandung mengucapkan selamat Tahun baru 2018, semoga Tahun ini menjadi lebih baik dan evaluasi dari tahun sebelumnya. SMK BPI Membuka Penerimaan Peserta Didik Baru tahun ajaran 2018/2019 telah dibuka. Ayo jangan lewatkan kesempatan emas untuk bisa bersekolah di Sekolah Menengah Kejuruan BPI bandung, Berkualitas dan Terpercaya.', '2019-11-27 11:42:47', 'smkbisch', '2019-11-27 11:42:47', ''),
(43, 'FIESTA SMK BPI 2018', 'SMK BPI Bandung mempersembahkan festival SMK BPI Bandung bernama FIESTA, Acara tersebut terdapat kegiatan lomba Jaipongan tingkat pemula dan professional, lomba akustik band tingkat SMP dan SMA/SMK, lomba solo voice tingkat SMP dan Stand Culinary dipersembahkan oleh siswa/siswi SMK BPI Bandung.', '2019-11-27 11:43:20', 'smkbisch', '2019-11-27 11:43:20', ''),
(44, 'MoU dengan PT SKYLINE SEMESTA dan PT CENDIKIA GLOBAL SOLUSI', 'Penandatanganan nota kesepahaman (MoU) SMK BPI Bandung dengna PT SKYLINE SEMESTA dan PT CENDIKIA GLOBAL SOLUSI dalam rangka pengembangan Teaching Factory.', '2019-11-27 11:43:45', 'smkbisch', '2019-11-27 11:43:45', ''),
(45, 'Peresmian Teaching Factory SMK BPI', 'Kepala Kantor Cabang Dinas Pendidikan Wilayah VII, Endang Susilastuti meresmikan Ruang Teaching Factory SMK BPI pada tanggal 22 Agustus 2019 bertempat di kampus Yayasan BPI. Peresmian tersebut juga dihadiri oleh tamu undangan diantaranya adalah Dinas Tenaga Kerja Kota Bandung, Mitra Perusahaan dan Dunia Usaha, SMK imbas SPMI, SMK penerima bantuan teaching factory dan Sekolah Rujukan dan lain-lain. \nProgram Teaching Factory adalah suatu konsep pembelajaran di SMK berbasis produksi atau jasa yang mengacu kepada standar dan prosedur yang berlaku di industri. Program ini dilaksanakan dalam suasana seperti layaknya industri. Implementasi Teaching Factory di SMK diharapkan dapat menjembatani kesenjangan kompetensi antara kebutuhan industri dan kompetensi yang dihasilkan oleh sekolah. Sehingga sekolah akan terus mengikuti perkembangan zaman dan mengikuti kebutuhan dunia kerja untuk meningkatkan kompetensi lulusan SMK yang relevan dengan kebutuhan industri. Ruang Teaching Factory SMK BPI menjadi salah satu prasarana penting yang dapat menunjang pembelajaran bermodel Teaching Factory tersebut.', '2019-08-22 00:00:00', 'smkbisch', '2019-11-27 11:46:55', 'adminteach'),
(46, 'Pemilihan Ketua OSIS SMK BPI', 'SMK BPI Bandung melaksanakan pemilihan Ketua OSIS SMK BPI Bandung periode 2019-2020. Pemilihan Ketua OSIS tersebut diikuti oleh seluruh warga SMK BPI Bandung baik siswa maupun guru dan karyawan. Pemilihan dilaksanakan secara terbuka, jujur dan adil secara online dengan mengisi form yang telah disediakan oleh panitia pemilihan Ketua OSIS. \r\nPada kegiatan tersebut, terdapat 3 kandidat calon Ketua dan Wakil Ketua OSIS. Calon Ketua dan Wakil Ketua nomor 1 atas nama Zidan Andika Hidayat dan Stifan Hilfa. Calon Ketua dan Wakil Ketua nomor 2 atas nama Jessica Yolanda Toripalu dan Nanda Farell. Calon Ketua dan Wakil Ketua nomor 3 atas nama Gespane Fayyadh Oktrino dan Elwi Jodavliano.\r\nPemilihan Ketua OSIS ini selain bertujuan untuk melaksanakan pemilihan Ketua OSIS di periode baru, juga bertujuan sebagai bentuk pemberian edukasi mengenai demokrasi untuk siswa siswi SMK BPI. Kegiatan pemilihan Ketua OSIS diharapkan bisa membekali siswa berupa karakter dan kecakapan untuk menjadi warga negara yang baik. Siswa menjadi tahu bagaimana prosedur pemilihan umum yang benar. Pemilihan ketua OSIS dilaksanakan dengan sebaik-baiknya, sportif dan bertanggung jawab serta tidak menimbulkan konflik setelah pelaksanaan pemilihan ketua OSIS.', '2019-11-27 11:47:21', 'smkbisch', '2019-11-27 11:47:21', ''),
(47, 'Pelantikan dan Serah Terima Jabatan OSIS SMK BPI', 'Ketua OSIS SMK BPI terpilih yaitu Zidan Andika Hidayat beserta jajaran pengurus OSIS periode 2019-2020 resmi dilantik pada tanggal 11 November 2019 bertepatan dengan upacara bendera memperingati Hari Pahlawan. Pelantikan dan serah terima jabatan OSIS tersebut dipimpin langsung oleh Kepala SMK BPI Drs. A. Budi Utomo, M.Pd. dan dihadiri oleh seluruh warga SMK BPI Bandung. Dalam kegiatan pelantikan tersebut dilaksanakan juga pengucapan janji pengurus OSIS yang dibacakan oleh Kepala SMK BPI dan diucap ulang oleh seluruh pengurus OSIS periode 2019-2020. Pada kesempatan tersebut Bapak Drs. A. Budi Utomo, M.Pd., berpesan kepada ketua OSIS dan pengurus OSIS terpilih agar amanah dan dapat melaksanakan tugas sebaik mungkin dalam upaya mewujudkan SMK BPI yang berkualitas, bermartabat dan terpercaya.', '2019-11-27 11:47:48', 'smkbisch', '2019-11-27 11:47:48', ''),
(48, 'SMK BPI Tampilkan Kreasi Teknologi Berbasis IoT', 'Pada tanggal 20 Februari 2020, SMK BPI menggelar salah satu rangkaian acara Labora Fiesta 2020 yaitu pameran dan workshop IoT (Internet of Things). Karya yang dipamerkan diantaranya adalah WuTis (Wudhu Otomatis), e-Cai (Dispenser Elektronik), Pengontrol Jarak Jauh berbasis IoT, dan beberapa karya lainnya. Karya yang ditampilkan bukan hanya hasil pembelajaran semata, namun telah menjuarai berbagai perlombaan tingkat Provinsi maupun Nasional. Selain pameran,  pada kesempatan yang sama, telah diadakan workshop IoT untuk tingkat SMP se-Kota Bandung. Salah satu materi yang ditampilkan yaitu cara kerja pengontrol jarak jauh menggunakan smartphone.', '2020-04-05 20:35:19', 'adminteach', '2020-04-05 20:35:19', ''),
(49, 'Pasanggiri Jaipong Labora Fiesta SMK BPI', 'Siswa SMK BPI menggelar acara tahunan Pasanggiri Jaipong Tunggal dan Umum Kategori SD dalam rangkaian acara Labora Fiesta 2020 pada tanggal 29 Februari 2020 bertempat di Carrefour Soekarno Hatta, Bandung. Kegiatan tersebut diikuti oleh puluhan siswa sekolah dasar mulai dari kategori kelas 1-3 hingga kategori kelas 4-6. Kegiatan ini dilaksanakan sebagai upaya melestarikan seni tradisional Jawa Barat agar tidak tergerus dan terlupakan oleh zaman. Selain pasanggiri jaipong, terdapat penampilan musik akustik dan modern dance.', '2020-04-05 20:36:15', 'adminteach', '2020-04-05 20:36:15', ''),
(50, 'Siswa SMK BPI Mengikuti Kegiatan BPI for Nature', 'Sejumlah siswa SMK BPI turut serta dalam kegiatan BPI for Nature yang diadakan oleh komunitas pecinta alam Yayasan BPI pada tanggal 7 Maret 2020 bertempat di Desa Cikadut, Kab. Bandung. Kegiatan tersebut diikuti oleh siswa SMA BPI 1, SMA BPI 2, SMK BPI dan para alumni serta anggota komunitas pecinta alam itu sendiri. Kegiatan yang dilaksanakan yaitu tadabbur alam dan penanaman tunas pohon. Selain sebagai bentuk penghijauan daerah setempat, kegiatan ini dimaksudkan untuk mengenalkan siswa pada alam agar nantinya mereka lebih menghargai dan menjaga lingkungan tetap lestari.', '2020-04-05 20:37:52', 'adminteach', '2020-04-05 20:37:52', ''),
(51, 'Siswa Kelas XII SMK BPI Mengikuti Tes dan Wawancara Kerja', 'SMK BPI Bandung telah bermitra dengan perusahan-perusahaan andal, salah satunya dengan PT Skyline Semesta.  Kerja sama nyata yang dilakukan yaitu perekrutan pegawai baru yang berasal dari SMK BPI Bandung.  Sebanyak 12 orang siswa telah melaksanakan tes dan wawancara kerja dengan PT Skyline Semesta pada tanggal 10 Maret 2020. Sebelumnya kedua belas siswa tersebut telah melalui proses seleksi nilai dan kompetensi keahlian yang dilakukan oleh pihak sekolah. Perekrutan tersebut membuktikan bahwa siswa SMK BPI berkualitas dan siap bersaing di dunia kerja. Bahkan sudah dapat bekerja sebelum dinyatakan lulus sebagai siswa.  ', '2020-04-05 20:40:42', 'adminteach', '2020-04-05 20:40:42', ''),
(52, 'SMK BPI Menyelenggarakan Seminar Bertemakan Dunia Kerja', 'Pada tanggal 12 Maret 2020 bertempat di Aula Yayasan BPI, seluruh siswa kelas XII SMK BPI Bandung mengikuti seminar yang diselenggarakan oleh SMK BPI bekerjasama dengan STMIK & Politeknik LPKIA. Seminar tersebut bertemakan \"Tata Cara Melamar Kerja dan Menyiasati Wawancara Kerja\" yang diberikan kepada siswa sebagai bekal menghadapi persaingan di dunia kerja. Melalui upaya ini, lulusan SMK BPI diharapkan mampu bersaing di dunia industri baik sebagai pekerja maupun sebagai penyedia lapangan kerja. ', '2020-04-05 20:41:37', 'adminteach', '2020-04-05 20:41:37', ''),
(53, 'Pembelajaran Jarak Jauh SMK BPI dalam Masa Darurat COVID-19', 'Merujuk pada Surat Edaran Mendikbud No. 4 Tahun 2020 tentang pelaksanaan kebijakan pendidikan dalam masa darurat penyebaran corona virus disease (COVID-19), SMK BPI Bandung melaksanakan kegiatan pembelajaran jarak jauh terhitung mulai tanggal 16 Maret s.d. 29 Maret 2020. Hal tersebut dilaksanakan sebagai salah satu upaya pencegahan penyebaran COVID-19 pada satuan pendidikan. \nPembelajaran jarak jauh dilaksanakan dengan mengacu pada agenda kegiatan yang harus dilaksanakan oleh siswa setiap harinya. Adapun opsi yang diberikan oleh sekolah yaitu pembelajaran  daring dan pembelajaran nondaring, berupa modul maupun portofolio siswa  dengan durasi waktu maksimal 6 jam tiap harinya agar siswa tidak merasa terbebani. Seluruh siswa SMK BPI melaksanakan kegiatan pembelajaran jarak jauh tersebut lima hari dalam satu minggu. Pembelajaran jarak jauh dipandu dan dipantau oleh guru mata pelajaran, wali kelas, dan orang tua siswa. Selain pembelajaran jarak jauh, SMK BPI juga menggalakan pola hidup sehat. Setiap hari,  para siswa dan guru berolahraga secara rutin dan mengonsumsi makanan bergizi untuk meningkatkan imun tubuh.  \n', '2020-04-05 20:43:59', 'adminteach', '2020-04-05 20:43:59', ''),
(54, 'Ujian Sekolah Daring SMK BPI', 'Siswa kelas XII SMK BPI Bandung melaksanakan ujian sekolah bertempat di rumah masing-masing dalam bentuk daring. Hal ini dilakukan sebagai upaya mencegah penyebaran virus COVID-19. Kegiatan tersebut dimulai tanggal 30 Maret 2020 untuk gladi bersih dan pelaksanaann Ujian Sekolah daring pada tanggal 1 s.d. 3 April 2020. Ujian sekolah dapat dilaksanakan melalui telepon genggam maupun laptop masing-masing siswa dengan mengakses laman elearning.smkbpi.sch.id . Adapun mata pelajaran yang diujikan yaitu Matematika, Pendidikan Agama Islam, Pendidikan Kewarganegaraan, Bahasa Indonesia, Bahasa Inggris, dan Teori Kejuruan. Ujian sekolah tersebut berjalan dan diikuti oleh seluruh siswa kelas XII SMK BPI.', '2020-04-05 20:44:37', 'adminteach', '2020-04-05 20:44:37', ''),
(55, 'Penilaian Tengah Semester Genap Daring', 'Siswa kelas X dan XI SMK BPI Bandung melaksanakan Penilaian Tengah Semester (PTS) Genap TA 2019/2020 bertempat di rumah masing-masing dalam bentuk daring.  Kegiatan tersebut dimulai tanggal 6 April 2020 untuk gladi bersih dan pelaksanaan PTS daring pada tanggal 7 s.d. 11 April 2020. Ujian dapat dilaksanakan melalui telepon genggam maupun laptop masing-masing siswa dengan mengakses laman elearning.smkbpi.sch.id . Seluruh kegiatan PTS tersebut dapat berjalan dengan lancar dan tertib berkat kerja sama yang baik antara pihak sekolah, siswa dan orang tua siswa.', '2020-04-11 00:00:00', 'adminteach', '2020-04-13 19:47:11', 'adminteachSMK'),
(61, 'BDR SMK BPI Bandung', 'Mengantisipasi penularan virus COVID-19 di Indonesia, seluruh kegiatan pembelajaran di SMK BPI Bandung masih dilakukan secara daring. Kegiatan Belajar dari Rumah (BDR) tersebut dilaksanakan secara interaktif dengan memanfaatkan berbagai teknologi mulai dari aplikasi teleconference, aplikasi E-learning hingga pemanfaatan aplikasi chatting. Berbagai aplikasi tersebut dimanfaatkan oleh tenaga pendidik demi terciptanya suasana belajar yang kondusif meski dilaksanakan tanpa tatap muka. Penilaian proses pembelajaran pun tetap memadukan unsur kepribadian, pengetahuan dan keterampilan. Maka dari itu keikutsertaan peserta didik selalu dipantau selama proses pembelajaran berlangsung. Meskipun begitu semua kegiatan tetap dirancang sebagai pembelajaran yang memudahkan baik dari segi beban belajar, beban tugas, beban kuota, bahkan media atau perangkat agar seluruh peserta didik terhindar dari beban yang berlebih. ', '2020-08-10 00:00:00', 'adminteachSMK', '2020-09-24 22:26:07', 'adminteachSMK'),
(56, 'Pembagian Rapor Tengah Semester via Daring', 'Jumat, 17 April 2020 dilaksanakan pembagian Rapor Tengah Semester Genap TA 2019/2020 bertempat di rumah masing-masing untuk seluruh siswa kelas X dan XI SMK BPI Bandung. Kegiatan pembagian rapor tersebut dilaksanakan secara daring sebagai bentuk pencegahan dan pemutusan rantai wabah COVID-19. Hasil penilaian dikirimkan dalam bentuk file PDF ke WhatsApp ataupun email orang tua peserta didik melalui wali kelas masing-masing. Selain pembagian rapor, orang tua juga dapat berkonsultasi mengenai perkembangan putera-puterinya di sekolah. Hal tersebut membuktikan bahwa meskipun kegiatan sekolah terhambat dengan adanya pandemi ini, SMK BPI tetap memberikan pelayanan dan kegiatan pembelajaran terbaik untuk seluruh peserta didik.', '2020-04-17 00:00:00', 'adminteach', '2020-04-17 11:31:49', 'adminteach'),
(57, 'PPDB SMK BPI Bandung', 'SMK BPI Bandung membuka Pendaftaran Peserta Didik Baru (PPDB) untuk tahun ajaran 2020/2021. SMK BPI merupakan salah satu penyelenggara sekolah kejuruan terbaik di Kota Bandung yang bermartabat, berkualitas dan terpercaya. Terletak di lokasi yang strategis di Kota Bandung SMK BPI memiliki fasilitas pembelajaran lengkap dan memadai, mulai dari ruang kelas yang nyaman, laboratorium mandiri untuk setiap kompetensi keahliannya, lingkungan yang aman dan lain sebagainya. \nKompetensi Keahlian yang tersedia di SMK BPI Bandung adalah Otomatisasi Tata Kelola Perkantoran (OTKP), Rekayasa Perangkat Kelas (RPL) dan Teknik Komputer Jaringan (TKJ). Bekerja sama dengan beberapa perusahaan dan Lembaga terkemuka serta didukung dengan guru yang kompeten di bidangnya, SMK BPI Bandung mengembangkan bakat dan minat yang dimiliki peserta didik hingga menjadi seorang ahli di bidangnya. Lulusannya siap bekerja, berwirausaha maupun siap melanjutkan studi. SMK BPI Bandung merupakan pilihan yang tepat, maka segera daftarkan diri karena kuota terbatas. Pendaftaran bisa dilakukan secara daring melalui situs web atau hubungi kontak 082116359966 (Yayan Himawan).\nCari SMK terbaik di Kota Bandung? Pilih SMK BPI saja! \n#smkbisa #smkbandung #jabarjuara #bandungjuara', '2020-04-22 00:00:00', 'adminteach', '2020-04-22 16:30:27', 'adminteach'),
(58, 'Sertifikasi Uji Kompetensi Daring Tahun 2020', 'Siswa kelas XII SMK BPI Bandung melaksanakan sertifikasi uji kompetensi bertempat di rumah masing-masing dalam bentuk daring. Kegiatan tersebut dilaksanakan pada tanggal 21 April 2020 untuk Kompetensi Keahlian TKJ dan tanggal 30 April 2020 untuk Kompetensi Keahlian OTKP dan RPL. Pelaksanaan sertifikasi uji kompetensi ini bertujuan untuk mengukur pencapaian kompetensi siswa pada level tertentu sesuai Kompetensi Keahlian yang ditempuh selama masa pembelajaran di SMK. Siswa kelas XII TKJ melaksanakan Ujikom berupa pengaturan mikrotik secara daring, siswa kelas XII RPL berupa presentasi proyek aplikasi sistem yang telah dibuat masing-masing siswa, dan siswa kelas XII OTKP berupa pengelolaan dokumen administrasi. Kegiatan tersebut dilaksanakan secara daring dan diuji oleh para asesor yang kompeten pada bidang yang diujikan.\nSetelah dinyatakan lulus dalam Uji Kompetensi peserta didik akan mendapatkan sebuah sertifikat keahlian yang disertakan nilai sesuai dengan kompetensinya masing-masing. Sertifikat tersebut yang membedakan siswa SMK dengan siswa SMA, karena dengan sertifikat tersebut siswa SMK dapat langsung terjun ke dunia kerja karena kemampuannya telah diakui dan ditandai dengan sertifikat tersebut. SMK BPI Bandung dapat membuktikan bahwa dalam keadaan waspada pandemi COVID-19 ini tidak mengurangi kualitas lulusannya.', '2020-05-01 00:00:00', 'adminteach', '2020-05-05 20:53:34', 'adminteach'),
(59, 'Pembagian Rapor Akhir Tahun Ajaran 2019/2020', 'Jumat, 19 Juni 2020 dilaksanakan pembagian Rapor Akhir Semester Genap TA 2019/2020 untuk seluruh siswa kelas X dan XI SMK BPI. Kegiatan pembagian rapor dilaksanakan di Kampus SMK BPI Bandung yang dilaksanakan antara orangtua/ wali peserta didik dengan wali kelas masing-masing. Selain pembagian rapor, kegiatan tersebut dimaksudkan agar orang tua/ wali dapat berkonsultasi mengenai perkembangan putera-puterinya di sekolah secara langsung. Meskipun diadakan di tengah pandemi COVID-19, pertemuan tersebut tetap memperhatikan protokol kesehatan yang berlaku diantaranya pengecekan suhu sebelum masuk ke area sekolah dan mewajibkan penggunaan masker ataupun faceshield. SMK BPI sendiri menyediakan tempat cuci tangan dengan keran otomatis dan handsanitizer di beberapa sudut sekolah, serta menerapkan penjadwalan pengambilan rapor untuk mencegah kerumunan. Wali kelas, guru maupun staf yang bertugas pun dilengkapi dengan sarung tangan maupun faceshield sebagai perlindungan diri. Hal tersebut membuktikan SMK BPI tetap memberikan pelayanan terbaik untuk seluruh peserta didik dengan tetap waspada dan memperhatikan himbauan pemerintah. SMK terbaik di Kota Bandung? Pasti SMK BPI!', '2020-06-19 00:00:00', 'adminteachSMK', '2020-06-23 20:59:58', 'adminteachSMK'),
(60, 'Masa Pengenalan Lingkungan Sekolah Daring SMK BPI', 'Merujuk pada Panduan Penyelenggaraan Pembelajaran TA Baru di Masa Pandemi COVID-19, sekolah tidak diperbolehkan melaksanakan Masa Pengenalan Lingkungan Sekolah (MPLS) secara langsung. Maka dari itu, SMK BPI Bandung melaksanakan kegiatan MPLS secara virtual terhitung mulai tanggal 13 Juli s.d. 15 Juli 2020. Hal tersebut dilaksanakan sebagai salah satu upaya pencegahan penyebaran COVID-19 pada satuan Pendidikan, dikarenakan kesehatan dan keselamatan adalah hal yang paling penting untuk diperhatikan.\nPembukaan masa pengenalan lingkungan sekolah dilaksanakan pada tanggal 13 Juli 2020. Rincian kegiatan yang dilaksanakan adalah dimulai dari Upacara Pembukaan, Sambutan, Pembacaan Ikrar Jabar Tolak Kekerasan, Pengenalan Guru dan staf SMK BPI Bandung, Tata Tertib hingga Struktur Kurikulum dari setiap kompetensi keahlian.\nPada hari kedua, materi yang disampaikan adalah Adaptasi Kebiasaan Baru, Etika di sosial media, serta Narkoba dan Dampaknya bagi Kesehatan. Pada hari ketiga, materi yang disampaikan adalah Bimbingan Karir setelah Lulus SMK, Pengenalan Ekstrakurikuler, dan Informasi Pembelajaran dari Kurikulum serta Penutupan MPLS. Materi disampaikan secara interaktif dan menarik oleh para pemateri melalui aplikasi WhatsApp grup dan Zoom meeting. Selain materi-materi yang disampaikan juga diadakan berbagai permainan untuk mencairkan suasana.\nSetelah pemberian materi selesai, peserta diminta memberikan umpan balik melalui aplikasi mentimeter. Berdasarkan umpan balik yang diberikan, peserta menilai bahwa MPLS yang dilaksanakan oleh SMK BPI dianggap menarik, bermanfaat, menambah wawasan, bahkan peserta memberikan testimoni bahwa meskipun MPLS dilaksanakan secara daring tetapi MPLS tidak membosankan dan terasa layaknya pertemuan langsung. Berakhirnya kegiatan MPLS menandakan bahwa para peserta resmi menjadi Peserta Didik SMK BPI Tahun Ajaran 2020/2021. Kami ucapkan selamat bergabung di SMK BPI Bandung.', '2020-07-16 00:00:00', 'adminteachSMK', '2020-07-19 20:35:02', 'adminteachSMK'),
(62, 'HUT Yayasan BPI Bandung', 'Bertepatan pada hari Selasa, 18 Agustus 2020 Yayasan Badan Perguruan Indonesia merayakan hari jadi yang ke-72. Dalam usianya yang cukup panjang dan hanya berselisih 3 tahun dengan Hari Kemerdekaan RI, merupakan suatu prestasi yang sangat membanggakan, karena selama periode tersebut Yayasan BPI dapat eksis menjawab tantangan zaman, perubahan sosial, budaya & teknologi, juga berbagai kebijaksanaan pemerintah di bidang pendidikan. \nPeringatan Hari Ulang Tahun BPI kali ini, berlangsung dalam keprihatinan berkenaan dengan pandemi Covid-19. Maka dari itu peringatan dilaksanakan secara sederhana dengan jumlah undangan yang terbatas dan tetap memperhatikan protokol kesehatan serta dapat disaksikan melalui siaran live streaming. Namun keadaan tersebut tidak mengurangi kekhidmatan acara. Melalui acara peringatan tersebut disampaikan apresiasi atas berbagai pencapaian dan harapan besar agar Yayasan BPI kedepannya dapat tumbuh dan berkembang dalam rangka memajukan Pendidikan Indonesia.', '2020-08-18 00:00:00', 'adminteachSMK', '2020-09-24 22:55:58', 'adminteachSMK'),
(63, 'PTS Ganjil TA 20/21 secara Daring', 'Kegiatan Penilaian Tengah Semester Ganjil Tahun Ajaran 2020/2021 SMK BPI Bandung dilaksanakan secara daring mulai dari tanggal 21 s.d. 25 September 2020 dengan mengakses laman elearning.smkbpi.sch.id . Kegiatan tersebut diikuti oleh seluruh peserta didik kelas X, kelas XI, dan kelas XII yang bertempat di rumah masing-masing melalui telepon genggam maupun perangkat laptop atau komputer peserta didik. Meskipun dilaksanakan secara jarak jauh pelaksanaan Penilaian Tengah Semester dirancang untuk tetap disiplin terhadap tata tertib dan menjunjung tinggi kejujuran. Berkat kerja sama yang baik antara pihak sekolah, peserta didik maupun orang tua seluruh kegiatan PTS tersebut dapat berjalan dengan tertib dan lancar.', '2020-09-24 00:00:00', 'adminteachSMK', '2020-09-24 23:00:29', 'adminteachSMK');

-- --------------------------------------------------------

--
-- Table structure for table `bursa_kerja`
--

CREATE TABLE `bursa_kerja` (
  `id_bursa_kerja` int(11) NOT NULL,
  `judul_bursa_kerja` varchar(150) NOT NULL,
  `gambar_utama` varchar(150) DEFAULT NULL,
  `bursa_kerja` text NOT NULL,
  `tgl_posting` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bursa_kerja`
--

INSERT INTO `bursa_kerja` (`id_bursa_kerja`, `judul_bursa_kerja`, `gambar_utama`, `bursa_kerja`, `tgl_posting`) VALUES
(2, 'Lowongan Bagi Alumni Administrasi Perkantoran', 'BursaKerja_1_LowonganBagiAlumniAdministrasiPerkantoran.jpg', '<p>Bagi Alumni SMK BPI Administrasi Perkantoran, silakan mencoba....semoga berhasil....</p>', '2016-03-22 08:58:34'),
(3, 'Lowongan Admin \"Rabbit Hole\"', 'BursaKerja_3_LowonganAdmin\"RabbitHole\".jpg', '<p>Sumber :&nbsp;</p>', '2016-03-22 12:40:25'),
(4, 'Lowongan Kerja MSV Pictures', 'BursaKerja_4_LowonganKerjaMSVPictures.jpg', '<p>Selamat mencoba :)</p>', '2016-03-25 02:54:01');

-- --------------------------------------------------------

--
-- Table structure for table `ekstrakulikuler`
--

CREATE TABLE `ekstrakulikuler` (
  `id_ekstrakulikuler` int(11) NOT NULL,
  `nama_ekstrakulikuler` varchar(150) DEFAULT NULL,
  `deskripsi` text,
  `id_pegawai` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ekstrakulikuler`
--

INSERT INTO `ekstrakulikuler` (`id_ekstrakulikuler`, `nama_ekstrakulikuler`, `deskripsi`, `id_pegawai`) VALUES
(3, 'Balap Karung', 'ini adalah', 10),
(4, 'Paskibra', '<p>Paskibra adalah</p>', 19),
(7, 'Pramuka', '<p>JNJKN</p>', 20),
(8, 'Angklung', '<p>jhbhjb</p>', 22),
(9, 'Basket', '<p>jbjb</p>', 17);

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `title` varchar(150) DEFAULT NULL,
  `description` text,
  `image` varchar(200) NOT NULL,
  `start` date DEFAULT NULL,
  `end` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `title`, `description`, `image`, `start`, `end`) VALUES
(34, 'Malam Bina Iman dan Takwa', 'Rohis SMK BPI, Bandung', 'event11.jpg', '2019-11-27', '2019-11-27'),
(33, 'Bakti Sosial SMK BPI', 'Osis SMK BPI, Bandung', 'event13.jpg', '2019-11-27', '2019-11-27'),
(32, 'Stand Penerimaan Siswa/i baru', 'Tim PPDB SMK BPI, Bandung', 'event14.jpg', '2019-11-27', '2019-11-27'),
(66, 'Uji Kompetensi Online', 'Kelas XII TKJ TA 2019/2020', '20200421_113335_0000.png', '2020-04-21', '2020-04-21'),
(27, 'Memperingati Hari Pahlawan', 'SMK BPI, Bandung', 'event18.jpg', '2019-11-27', '2019-11-27'),
(29, 'Selamat Hari Pahlawan', 'SMK BPI, Bandung', 'event17.jpg', '2019-11-27', '2019-11-27'),
(30, 'Acara Gebyar Kemerdekaan SMK BPI', 'SMK BPI, Bandung', 'event16.jpg', '2019-11-27', '2019-11-27'),
(31, 'Acara Android Indonesia Kejar', 'Tim RPL SMK BPI kerja sama Android Indonesia Kejar, Bandung', 'event15.jpg', '2019-11-27', '2019-11-27'),
(35, 'Pesantren Kilat', 'SMK BPI, Bandung', 'event12.jpg', '2019-11-27', '2019-11-27'),
(36, 'Ujikom TKJ', 'Kurikulum SMK BPI, Bandung', 'event10.jpg', '2019-11-27', '2019-11-27'),
(37, 'Lomba Akustik SMP', 'Fiesta SMK BPI, Bandung', 'event1.jpg', '2019-11-27', '2019-11-27'),
(38, 'Lomba Akustik Tingkat SMA/SMK', 'Fiesta SMK BPI, Bandung', 'event2.jpg', '2019-11-27', '2019-11-27'),
(39, 'Juara Lomba Tingkat SMP', 'Fiesta SMK BPI, Bandung', 'event3.jpg', '2019-11-27', '2019-11-27'),
(40, 'Panitia Osis', 'Fiesta SMK BPI, Bandung', 'event4.jpg', '2019-11-27', '2019-11-27'),
(41, 'Panitia Osis', 'Fiesta SMK BPI, Bandung', 'event7.jpg', '2019-11-27', '2019-11-27'),
(42, 'Juara Lomba Akustik Tingkat SMA/SMK', 'Fiesta SMK BPI, Bandung', 'event8.jpg', '2019-11-27', '2019-11-27'),
(43, 'Lomba Pasanggiri Jaipong', 'Fiesta SMK BPI, Bandung', 'event6.jpg', '2019-11-27', '2019-11-27'),
(47, 'Sertijab Pengurus OSIS', 'Pelantikan dan Serah Terima Jabatan Pengurus OSIS', 'sertijab_osis.png', '2019-11-11', '2019-11-11'),
(46, 'Peresmian Teaching Factory SMK BPI', 'Teaching Factory SMK BPI', '2.jpg', '2019-12-26', '2019-12-26'),
(48, 'Pameran dan Workshop IoT', 'Aula Yayasan BPI, rangkaian FIESTA 2020', 'workshop.png', '2020-02-20', '2020-02-20'),
(49, 'Pasanggiri Jaipong', 'Carrefour Soetta, rangkaian FIESTA 2020', '5da71d1a-a432-4e47-9ab8-dc252af78452.jpg', '2020-02-29', '2020-02-29'),
(50, 'Pasanggiri Jaipong', 'Carrefour Soetta, rangkaian FIESTA 2020', 'pasanggiri2.png', '2020-02-29', '2020-02-29'),
(51, 'BPI for Nature', 'Desa Cikadut, Kab. Bandung', 'bpifornature.png', '2020-03-07', '2020-03-07'),
(52, 'Seminar Dunia Kerja', 'Aula Yayasan BPI, diikuti siswa kelas XII SMK BPI', 'semminar.png', '2020-03-12', '2020-03-12'),
(53, 'Pembelajaran Jarak Jauh', 'Dalam masa darurat COVID-19', 'pjj.png', '2020-03-16', '2020-03-29'),
(54, 'Ujian Sekolah Online', 'Diikuti seluruh siswa kelas XII SMK BPI', 'usonline.png', '2020-04-01', '2020-04-04'),
(59, 'PTS Online', 'Semester Genap TA 2019/2020', 'WhatsApp_Image_2020-04-10_at_17.55_.44_.jpeg', '2020-04-07', '2020-04-11'),
(58, 'Gladi Ujikom Online XII OTKP', 'Kerja sama dengan ASPAPI', '1.png', '2020-04-09', '2020-04-09'),
(60, 'PJJ SMK BPI Tahap II', 'Melalui TVRI dan aplikasi belajar lainnya', 'WhatsApp_Image_2020-04-16_at_17.05_.06_.jpeg', '2020-04-13', '2020-04-17'),
(61, 'Simulasi Ujikom Online XII TKJ', 'Setting mikrotik secara online', '20200417_160830_0000.png', '2020-04-16', '2020-04-17'),
(62, 'Pembagian Rapor Online', 'Tengah Semester Genap TA 2019/2020', '20200417_192540_0000.jpg', '2020-04-17', '2020-04-17'),
(65, 'Penyemprotan Disinfektan', 'Di lingkungan SMK BPI Bandung', '20200421_174105_0000.png', '2020-04-20', '2020-04-20'),
(67, 'PPDB SMK BPI BANDUNG', 'Tahun Ajaran 2019/2020', 'IMG-20200420-WA0019.jpg', '2020-04-21', '2020-04-21'),
(68, 'SMARTREN ONLINE', 'Pembukaan Pesantren Ramadhan', '20200427_135431_0000.png', '2020-04-27', '2020-04-27'),
(69, 'SMARTREN ONLINE ', 'Pesantren Ramadhan 1441 H Hari ke-2', 'IMG-20200429-WA0007.jpg', '2020-04-28', '2020-04-28'),
(70, 'Uji Kompetensi Online', 'Kelas XII OTKP TA 2019/2020', 'IMG-20200430-WA0025.jpg', '2020-04-30', '2020-04-30'),
(71, 'Uji Kompetensi Online', 'Kelas XII RPL TA 2019/2020', '20200501_164455_0000.jpg', '2020-04-30', '2020-04-30'),
(72, 'SMARTREN ONLINE', 'Dihadiri guru, orang tua dan peserta didik SMK BPI', '20200515_174142_0000.png', '2020-05-15', '2020-05-15'),
(74, 'Bakti Sosial', 'OSIS SMK BPI Bandung', '20200521_123559_0000.jpg', '2020-05-20', '2020-05-20'),
(75, 'PPDB SMK BPI BANDUNG', 'Kunjungi stand PPDB di Kampus BPI', 'IMG-20200603-WA0023.jpg', '2020-06-03', '2020-06-03'),
(76, 'PAT ONLINE', 'Kelas X dan XI TA 2019/2020', '20200608_123014_0000.jpg', '2020-06-03', '2020-06-08'),
(77, 'Tempat Cuci Tangan Otomatis', 'Beradaptasi dengan Kebiasaan Baru', '20200623_095223_0000.png', '2020-06-23', '2020-06-23'),
(78, 'MPLS Daring SMK BPI', 'Masa Pengenalan Lingkungan Sekolah', '20200719_210551_0000.jpg', '2020-07-13', '2020-07-15');

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `id_feedback` int(11) NOT NULL,
  `nama` varchar(150) DEFAULT NULL,
  `kontak` varchar(50) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `instansi` varchar(100) DEFAULT NULL,
  `isi_feedback` text,
  `tgl_posting` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `baca_feedback` int(11) NOT NULL,
  `balas_feedback` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`id_feedback`, `nama`, `kontak`, `email`, `instansi`, `isi_feedback`, `tgl_posting`, `baca_feedback`, `balas_feedback`) VALUES
(1, 'VAR-IT', '-', 'varit.solution@gmail.com', 'UPI', 'semoga bermanfaat', '2016-02-14 05:15:08', 1, 1),
(2, 'VAR-IT (1)', '-', 'varit.solution@gmail.com', 'UPI', 'Silakan konsultasi dengan hubungi kontak atau email yang tertera', '2016-02-14 05:15:12', 1, 1),
(18, 'izzah', '08984657657', 'izzah.tiari@student.upi.edu', 'UPI', 'komentar', '2016-02-21 22:10:22', 0, 0),
(19, 'tessterr', '11122233', 'tesstee', 'tessterr', 'tesssterrr', '2016-03-21 17:53:40', 0, 0),
(20, 'Asu', '0857123456789', 'asu@asu.com', 'Asu Banget', 'Test ', '2016-04-05 04:24:45', 1, 1),
(21, 'Tita Rosita', '081321065256', 'tr.rosita@gmail.com', 'bandung', 'Ysh. Admin,\n\nMohon informasi utk PPDB thn 2016/2017, beserta biaya nya. \n\nTks.\n\nsalam,', '2017-01-24 13:05:34', 0, 0),
(22, 'magie mardian', '085722709097', 'mardianmagie@gmail.com', 'giestore', 'salam', '2016-07-20 12:48:06', 0, 0),
(23, 'magie mardian', '085722709097', 'mardianmagie@gmail.com', 'giestore', 'salam.. sebelumnya sya ucapkn trima kasih atas perhatiannya. Perkenalkan nama saya Magie Mardian, saya ingin bertanya, kesalahan fatal apa?? yang adik saya buat sehingga di berhentikan mengajar di SMK BPI Bandung. walaupun itu bkn urusan saya perihal d kluarkan / tidaknya. yang saya sayangkan, mengapa tidak ada surat resmi pemberhentian atau sebelumny bertatap muka untuk memberhentikan kerja, kenapa harus lewat WA, adik sya salah satu guru administrasi perkantoran yg kmarin di berhintakan secara aneh via WA lho..tidak etis untuk sbuah lembaga PENDIDIKAN, WA tersebut d kirim oleh ketua jurusan YAYAN HIMAWAN S.Pd. yang saya permasalhkan disini adalah, adik sampai menangis tiada henti, sedang hamil lho adik saya, yg kaget d berhentikan krja via WA, gk ada surat resmi / pemanggilan yg bersangkutan/ surat paklaring. maaf bila ada salah kata dari saya, maklum saya tidak berpendidikan,, sekali lagi sya hanya ingin tahu apa ksalahan fatal adik saya, sampai kejadian ini terjadi.. bkn mslh d berhentikan kerja itu hak lembaga / pejabat SMK BPI BANDUNG,, tp via WA nya itu lho.. maaf via email bukan WA, saya mhon kejelasanny terima kasih\nno saya 085722709097\n\nSeyogyanya Lembaga Pendidikan itu lebih menghargai, berpendidikan, saling menghargai dan beretika / bermoral', '2017-01-24 13:07:39', 0, 0),
(24, 'van cleef alhambra bracelet white imitation', 'van cleef alhambra bracelet white imitation', 'jnavdwkxima@gmail.com', 'http://www.vancleefalhambra.com/cheap-fake-vintage-alhambra-bracelet-carnelian-5-motifs-vcard35500-p', '\"Do the initiative sponsors want them (newlywed seniors who can\'t have kids) to LIVE IN SIN?\"\n <a href=\"http://www.vancleefalhambra.com/cheap-fake-vintage-alhambra-bracelet-carnelian-5-motifs-vcard35500-p295.html\">van cleef alhambra bracelet white imitation</a> [url=http://www.vancleefalhambra.com/cheap-fake-vintage-alhambra-bracelet-carnelian-5-motifs-vcard35500-p295.html]van cleef alhambra bracelet white imitation[/url]', '2017-01-24 13:08:06', 0, 0),
(25, '1', '1', 'sample@email.tst', '1', '1', '2016-08-19 10:57:48', 0, 0),
(26, 'rikrik sunaryadi', '085220522569', 'rikriksunaryadi@gmail.com', 'BELIFE-BEST LIFE ', 'Salam...\nPerkenalkan Saya Ririk Sunaryadi, dari BELIFE- BEST LIFE (Laerning Consultant & Partnership) ingin menawarkan program-program kegiatan kami, untuk para siswa-siswi ataupun untuk para guru dan juga staf lainya, bisakah dikirimkan  alamat emailnya?\nsmoga kita bbisa bekerjasama serta membawa kebaikan untuk kita semua.\nterima kasih', '2017-01-24 13:08:24', 0, 0),
(27, '1', '1', 'sample@email.tst', '1', '1', '2016-11-20 09:48:56', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `galeri`
--

CREATE TABLE `galeri` (
  `id_galeri` int(11) NOT NULL,
  `judul_galeri` varchar(50) DEFAULT NULL,
  `foto` varchar(150) DEFAULT NULL,
  `deskripsi` varchar(200) DEFAULT NULL,
  `id_kategori` int(11) DEFAULT NULL,
  `tgl_posting` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `galeri`
--

INSERT INTO `galeri` (`id_galeri`, `judul_galeri`, `foto`, `deskripsi`, `id_kategori`, `tgl_posting`) VALUES
(29, 'Lomba Akustik', '71.jpg', 'Fiesta smk bpi bandung', NULL, '2019-11-27 05:05:53'),
(30, 'Lomba Akustik', '61.jpg', 'Fiesta smk bpi bandung', NULL, '2019-11-27 05:06:08'),
(37, 'Lomba Akustik', '15.jpg', 'Fiesta smk bpi bandung', NULL, '2019-11-27 05:11:14'),
(36, 'Lomba Akustik', '14.jpg', 'Fiesta smk bpi bandung', NULL, '2019-11-27 05:10:49'),
(35, 'Lomba Akustik', '16.jpg', 'Fiesta smk bpi bandung', NULL, '2019-11-27 05:10:15'),
(25, 'Lomba Akustik', '8.jpg', 'Fiesta smk bpi bandung', NULL, '2019-11-27 05:04:28'),
(24, 'Lomba Akustik', '9.jpg', 'Fiesta smk bpi bandung', NULL, '2019-11-27 05:04:03'),
(23, 'Lomba Akustik', '6.jpg', 'Fiesta smk bpi bandung', NULL, '2019-11-27 05:03:17'),
(21, 'Lomba Akustik', '21.jpg', 'Fiesta smk bpi bandung', NULL, '2019-11-27 04:58:37'),
(20, 'Panitia', 'event4.jpg', 'Fiesta SMK BPI Bandung', NULL, '2019-11-27 04:57:17'),
(22, 'Lomba Akustik', '10.jpg', 'fiesta smk bpi bandung', NULL, '2019-11-27 05:02:37'),
(31, 'Lomba Akustik', '4.jpg', 'Fiesta smk bpi bandung', NULL, '2019-11-27 05:06:50'),
(32, 'Lomba Akustik', '3.jpg', 'Fiesta smk bpi bandung', NULL, '2019-11-27 05:07:04'),
(33, 'Lomba Akustik', '2.jpg', 'Fiesta smk bpi bandung', NULL, '2019-11-27 05:07:23'),
(34, 'Lomba Akustik', '1.jpg', 'Fiesta smk bpi bandung', NULL, '2019-11-27 05:07:39'),
(38, 'Lomba Akustik', '12.jpg', 'Fiesta smk bpi bandung', NULL, '2019-11-27 05:11:32'),
(39, 'Lomba Akustik', '13.jpg', 'Fiesta smk bpi bandung', NULL, '2019-11-27 05:11:52'),
(40, 'Lomba Akustik', '17.jpg', 'Fiesta smk bpi bandung', NULL, '2019-11-27 05:12:18');

-- --------------------------------------------------------

--
-- Table structure for table `guru_piket`
--

CREATE TABLE `guru_piket` (
  `id_guru_piket` int(11) NOT NULL,
  `id_pegawai` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `histori_kelas`
--

CREATE TABLE `histori_kelas` (
  `id_histori_kelas` int(11) NOT NULL,
  `id_kelas` int(11) DEFAULT NULL,
  `nis` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `info_ppdb`
--

CREATE TABLE `info_ppdb` (
  `id_info_ppdb` int(11) NOT NULL,
  `judul_info_ppdb` varchar(150) DEFAULT NULL,
  `info_ppdb` text,
  `tgl_posting` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_user` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `info_ppdb`
--

INSERT INTO `info_ppdb` (`id_info_ppdb`, `judul_info_ppdb`, `info_ppdb`, `tgl_posting`, `id_user`) VALUES
(6, 'Informasi Penerimaan Peserta Didik Baru Tahun 2016/2017', '<p><strong><img class=\"example1\" src=\"D:\\3 semua tentang SMK\\2015-2016\\PPDB\\brosur\" alt=\"Brosur SMK BPI 2016-2017\" />Prosedur Pendaftaran</strong></p>\r\n<p><strong>Tahap Pertama (Maret &ndash; Juni 2016)</strong></p>\r\n<p>&nbsp;</p>\r\n<p>1. Pembelian Formulir Rp 200.000,-</p>\r\n<p>&nbsp; &nbsp; Catatan :</p>\r\n<p>&nbsp; &nbsp; a. Formulir tidak memastikan calon peserta didik otomatis mendapatkan tempat.</p>\r\n<p>&nbsp; &nbsp; b. Uang pembelian formulir tidak dapat dikembalikan.</p>\r\n<p>&nbsp;</p>\r\n<p>2. Membayar biaya pendidikan</p>\r\n<p>&nbsp; &nbsp; Dengan perincian :</p>\r\n<table border=\"1\" width=\"472\">\r\n<tbody>\r\n<tr>\r\n<td width=\"28\">\r\n<p style=\"text-align: center;\"><strong>NO</strong></p>\r\n</td>\r\n<td width=\"151\">\r\n<p style=\"text-align: center;\"><strong>KOMPONEN</strong></p>\r\n</td>\r\n<td width=\"123\">\r\n<p style=\"text-align: center;\"><strong>BIAYA (Rp)</strong></p>\r\n</td>\r\n<td width=\"170\">\r\n<p style=\"text-align: center;\"><strong>KETERANGAN</strong></p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width=\"28\">\r\n<p>1</p>\r\n</td>\r\n<td width=\"151\">\r\n<p>Formulir Pendaftaran</p>\r\n</td>\r\n<td width=\"123\">\r\n<p>200.000</p>\r\n</td>\r\n<td width=\"170\">\r\n<p>Tidak dapat dikembalikan</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width=\"28\">\r\n<p>2</p>\r\n</td>\r\n<td width=\"151\">\r\n<p>DSP</p>\r\n</td>\r\n<td width=\"123\">\r\n<p>3.150.000</p>\r\n</td>\r\n<td width=\"170\">\r\n<p>Dapat dicicil 50 % selama di kelas X (10 bulan)</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width=\"28\">\r\n<p>3</p>\r\n</td>\r\n<td width=\"151\">\r\n<p>Praktik Lab (3 Tahun)</p>\r\n</td>\r\n<td width=\"123\">\r\n<p>1.500.000</p>\r\n</td>\r\n<td width=\"170\">\r\n<p>Tidak dapat dikembalikan</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td rowspan=\"4\" width=\"28\">\r\n<p>4</p>\r\n</td>\r\n<td width=\"151\">\r\n<p>SPP Bulan Juli 2016,</p>\r\n<p>Rincian :</p>\r\n</td>\r\n<td width=\"123\">\r\n<p>340.000</p>\r\n</td>\r\n<td rowspan=\"4\" width=\"170\">\r\n<p>Tidak dapat dikembalikan</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width=\"151\">\r\n<p>SPP per Bulan</p>\r\n</td>\r\n<td width=\"123\">\r\n<p>315.000</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width=\"151\">\r\n<p>Iuran OSIS</p>\r\n</td>\r\n<td width=\"123\">\r\n<p>15.000</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width=\"151\">\r\n<p>Tabungan</p>\r\n</td>\r\n<td width=\"123\">\r\n<p>10.000</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width=\"28\">\r\n<p>5</p>\r\n</td>\r\n<td width=\"151\">\r\n<p>PSAS</p>\r\n</td>\r\n<td width=\"123\">\r\n<p>800.000</p>\r\n</td>\r\n<td width=\"170\">\r\n<p>Koperasi BPI</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width=\"28\">\r\n<p><strong>&nbsp;</strong></p>\r\n</td>\r\n<td width=\"151\">\r\n<p><strong>TOTAL </strong></p>\r\n</td>\r\n<td width=\"123\">\r\n<p><strong>5.990.000 </strong></p>\r\n</td>\r\n<td width=\"170\">\r\n<p><strong>&nbsp;</strong></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>&nbsp; Catatan :</p>\r\n<p>&nbsp; a. Pembiayaan yang telah dilakukan tidak dapat dikembalikan.</p>\r\n<p>&nbsp; b. Pembiayaan yang telah dilakukan tidak dapat dialihkan kepada calon peserta didik lain.&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>3. Melengkapi persyaratan pemberkasan administrasi, diantaranya :</p>\r\n<p>&nbsp; &nbsp; a. Mengisi Formulir&nbsp;</p>\r\n<p>&nbsp; &nbsp; b. Fotokopi Ijazah (legalisir)</p>\r\n<p>&nbsp; &nbsp; c. Fotokopi SKHUN (legalisir)</p>\r\n<p>&nbsp; &nbsp; d. Surat Kelakuan baik dari sekolah asal (Asli)</p>\r\n<p>&nbsp; &nbsp; e. Fotokopi Kartu Keluarga</p>\r\n<p>&nbsp; &nbsp; f. Dan keterangan lain yang mendukung</p>\r\n<p>&nbsp;</p>\r\n<p>Respon cepat informasi PPDB dapat menghubungi (SMS/Telepon) : <strong>0895-0928-1643</strong></p>', '2016-03-15 11:59:37', 5);

-- --------------------------------------------------------

--
-- Table structure for table `jabatan`
--

CREATE TABLE `jabatan` (
  `id_jabatan` int(11) NOT NULL,
  `jabatan` varchar(100) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `urut` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jabatan`
--

INSERT INTO `jabatan` (`id_jabatan`, `jabatan`, `status`, `urut`) VALUES
(1, 'Kepala Sekolah', 1, 1),
(3, 'Staf TU', 0, 9),
(15, 'Ketua Jurusan Rekayasa Perangkat Lunak', 1, 3),
(6, 'Waka Bidang Kesiswaan', 1, 2),
(7, 'Waka Bidang Kurikulum', 1, 2),
(8, 'Karyawan', 0, 10),
(9, 'Guru', 0, 6),
(12, 'Wakil Manajemen Mutu', 1, 2),
(14, 'Ketua Jurusan Teknik Komputer dan Jaringan', 1, 3),
(16, 'Ketua Jurusan Administrasi Perkantoran', 1, 3),
(17, 'Bimbingan Konseling', 0, 5),
(18, 'Kepala Tata Usaha', 1, 7),
(19, 'Waka Bidang Hubin', 1, 2),
(21, 'Ketua BKK', 1, 4),
(22, 'Staff Kurikulum', 0, 4),
(23, 'Pembina OSIS', 1, 4);

-- --------------------------------------------------------

--
-- Table structure for table `kategori_artikel`
--

CREATE TABLE `kategori_artikel` (
  `id_kategori_artikel` int(11) NOT NULL,
  `kategori_artikel` varchar(150) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori_artikel`
--

INSERT INTO `kategori_artikel` (`id_kategori_artikel`, `kategori_artikel`) VALUES
(1, 'Artikel IT'),
(2, 'Kegiatan'),
(3, 'Administrasi Perkantoran'),
(4, 'Teknik Komputer dan Jaringan'),
(5, 'Rekayasa Perangkat Lunak'),
(6, 'Tips dan Trik'),
(7, 'Lain-Lain');

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE `kelas` (
  `id_kelas` int(11) NOT NULL,
  `kelas` varchar(12) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelas`
--

INSERT INTO `kelas` (`id_kelas`, `kelas`) VALUES
(10, 'X-TI 1'),
(11, 'X-TI 2'),
(12, 'X-AP'),
(13, 'XI RPL'),
(14, 'XI TKJ'),
(15, 'XI AP'),
(16, 'XII RPL'),
(17, 'XII TKJ'),
(18, 'XII AP');

-- --------------------------------------------------------

--
-- Table structure for table `kepegawaian`
--

CREATE TABLE `kepegawaian` (
  `id_pegawai` int(11) NOT NULL,
  `nip` varchar(20) DEFAULT NULL,
  `id_jabatan` int(11) DEFAULT NULL,
  `nama` varchar(150) DEFAULT NULL,
  `jenis_kelamin` varchar(15) DEFAULT NULL,
  `alamat` varchar(250) DEFAULT NULL,
  `no_kontak` varchar(15) DEFAULT NULL,
  `sosmed` varchar(150) DEFAULT NULL,
  `mata_pelajaran` varchar(150) NOT NULL,
  `foto` varchar(150) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kepegawaian`
--

INSERT INTO `kepegawaian` (`id_pegawai`, `nip`, `id_jabatan`, `nama`, `jenis_kelamin`, `alamat`, `no_kontak`, `sosmed`, `mata_pelajaran`, `foto`) VALUES
(1, 'N/A', 1, 'Drs. Akhmad Budi Utomo, M.Pd.', 'L', '-', '-', '', '-', 'pegawai_1.jpg'),
(2, 'N/A', 16, 'Yayan Himawan, S.Pd.', 'L', '-', '-', '', 'Produktif Adm. Perkantoran', 'pegawai_2.jpg'),
(9, 'N/A', 14, 'Acep Komarudin, S.Si', 'L', '-', '-', '', 'Produktif TKJ', 'pegawai_9.jpg'),
(13, 'N/A', 9, 'Dra. Anne Susanawati', 'P', '-', '-', '', 'Bahasa Inggris', 'pegawai_13.jpg'),
(14, 'N/A', 19, 'Dra. Hj Anggani', 'P', '-', '089610093005', '', 'PAI', 'pegawai_14.jpg'),
(7, 'N/A', 15, 'M. Noor Basuki,S.Si', 'L', '-', '-', '', 'Produktif RPL', 'pegawai_7.jpg'),
(8, 'N/A', 9, 'Andri Priyanto', 'L', '-', '085794301094', 'twitter.com/andri.priyanto', 'Produktif TKJ', 'pegawai_8.jpg'),
(15, 'N/A', 7, 'Tatang M.Pd', 'L', '-', '085220306874', '', '-', 'pegawai_15.jpg'),
(16, 'N/A', 6, 'Agus Nugroho, S.Pd', 'L', '-', '082219231023', '', 'IPS', 'pegawai_16.jpg'),
(17, 'N/A', 9, 'Imas Yulianti, SE', 'P', '-', '-', '', 'Kewirausahaan', 'pegawai_17.jpg'),
(18, 'N/A', 9, 'Asep Ediana, S.Sn', 'L', '-', '-', '', 'Seni Budaya', 'pegawai_male_default.jpg'),
(19, 'N/A', 21, 'Agus Salim, S.Pdi', 'L', '-', '-', '', 'PAI', 'pegawai_19.jpg'),
(20, 'N/A', 12, 'Doni Agus Maulana, S.Pd', 'L', '-', '-', '', 'Bimbingan Konseling', 'pegawai_20.jpg'),
(21, 'N/A', 22, 'Ratih Amalia,S.Pd', 'P', '-', '-', '', 'Matematika', 'pegawai_21.jpg'),
(22, 'N/A', 9, 'Wahyu Sinarningsih, S.Pd', 'P', '-', '-', '', 'Bahasa Indonesia', 'pegawai_female_default.jpg'),
(23, 'N/A', 9, 'Agista Akbari W, S.S', 'P', '-', '-', '', 'Bahasa Inggris', 'pegawai_23.jpg'),
(24, 'N/A', 17, 'Ika Nurhayati,S.Psi', 'P', '-', '-', '', 'Bimbingan Konseling', 'pegawai_24.jpg'),
(25, 'N/A', 9, 'Ade Aso, S.Pd', 'L', '-', '-', '', 'Produktif TKJ', 'pegawai_25.jpg'),
(26, 'N/A', 9, 'Adyan Marendra, S.Pd', 'L', '-', '-', '', 'Produktif TKJ', 'pegawai_26.jpg'),
(27, 'N/A', 9, 'Putri Yuliasari Cesar, S.Pd', 'P', '-', '-', '', 'Produktif Adm. Perkantoran', 'pegawai_27.jpg'),
(28, 'N/A', 9, 'Dita Amelia P, S.Pd', 'P', '-', '-', '', 'Produktif Adm. Perkantoran', 'pegawai_28.jpg'),
(29, 'N/A', 9, 'Restu Arti Setia, S.Pd', 'P', '-', '-', '', 'Produktif Adm. Perkantoran', 'pegawai_29.jpg'),
(30, 'N/A', 9, 'Galih Nalapraya, S.Si', 'L', '-', '-', '', 'Produktif TKJ', 'pegawai_30.jpg'),
(31, 'N/A', 9, 'Kristi Herdiyanti, S.Pd', 'P', '-', '-', '', 'Produktif RPL', 'pegawai_31.jpg'),
(32, 'N/A', 9, 'Rahman Taufik, S.Pd', 'L', '-', '-', '', 'Produktif RPL', 'pegawai_male_default.jpg'),
(33, 'N/A', 9, 'Ahmad Fuadi Hasan S.Pd', 'L', '-', '-', '', 'Pendidikan Jasmani, Olahraga, dan Kesehatan', 'pegawai_male_default.jpg'),
(34, 'N/A', 9, 'Resti Khairun Nissa,S.Pd', 'P', '-', '-', '', 'Matematika', 'pegawai_34.jpg'),
(35, 'N/A', 9, 'Vera Aldillah S, S.Pd', 'P', '-', '-', '', 'Bahasa Indonesia', 'pegawai_female_default.jpg'),
(36, 'N/A', 18, 'Endang Sutisna', 'L', '-', '-', '', '-', 'pegawai_36.jpg'),
(37, 'N/A', 3, 'Tri Muldiyanto', 'L', '-', '-', '', '-', 'pegawai_37.jpg'),
(38, 'N/A', 3, 'Supriyo', 'L', '-', '-', '', '-', 'pegawai_38.jpg'),
(39, 'N/A', 3, 'Sarimin', 'L', '-', '-', '', '-', 'pegawai_39.jpg'),
(40, 'N/A', 23, 'Andri Refianto', 'L', '-', '-', '', 'Pendidikan Kewarganegaraan', 'pegawai_40.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `keterangan_absen`
--

CREATE TABLE `keterangan_absen` (
  `id_keterangan` int(11) NOT NULL,
  `keterangan` varchar(25) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `keterangan_absen`
--

INSERT INTO `keterangan_absen` (`id_keterangan`, `keterangan`) VALUES
(1, 'sakit'),
(2, 'izin'),
(3, 'alpha'),
(4, 'terlambat');

-- --------------------------------------------------------

--
-- Table structure for table `other`
--

CREATE TABLE `other` (
  `id_other` int(11) NOT NULL,
  `judul_upload` varchar(150) DEFAULT NULL,
  `file_upload` varchar(150) DEFAULT NULL,
  `tgl_posting` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pendaftaran`
--

CREATE TABLE `pendaftaran` (
  `id_pendaftaran` int(11) NOT NULL,
  `tgl_pendaftaran` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `nama_pendaftar` varchar(50) NOT NULL,
  `nisn` varchar(10) NOT NULL,
  `jenis_kelamin` varchar(15) NOT NULL,
  `tempat_lahir` varchar(30) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `agama` varchar(30) NOT NULL,
  `alamat` text NOT NULL,
  `no_telepon` varchar(15) NOT NULL,
  `no_handphone` varchar(15) NOT NULL,
  `email` varchar(50) NOT NULL,
  `nama_ortu` varchar(50) NOT NULL,
  `asal_sekolah` varchar(50) NOT NULL,
  `jurusan` varchar(15) NOT NULL,
  `asal_informasi` varchar(150) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendaftaran`
--

INSERT INTO `pendaftaran` (`id_pendaftaran`, `tgl_pendaftaran`, `nama_pendaftar`, `nisn`, `jenis_kelamin`, `tempat_lahir`, `tanggal_lahir`, `agama`, `alamat`, `no_telepon`, `no_handphone`, `email`, `nama_ortu`, `asal_sekolah`, `jurusan`, `asal_informasi`) VALUES
(1, '2020-05-11 08:05:27', 'Rissa Agustin', '0047554965', 'P', 'Bandung', '1970-01-01', 'Islam', 'Jl.karees saluran no.68/121.rt 02/RW 01.\nKec.Batununggal,Kota Bandung', '081321722993', '082120789704', '', 'Koidah', 'Kemala Bhayangkari', 'OTKP', 'Info Keluarga'),
(2, '2020-06-02 05:15:44', 'Muhammad Rakhsha Nabil', '0058651226', 'L', 'Bandung', '2005-04-26', 'Islam', 'Jln Riung Bagja IIB No 5, RT 6 RW 9 kecamatan Gedebage kelurahan Cisaranteun Kidul', '+62 812-2393-43', '081322083276', 'rakhshamuhammad@gmail.com', 'Imam Hambali', 'SMPN 18 Bandung', 'RPL', 'Media Online'),
(3, '2020-06-05 03:38:33', 'Muhammad Sahlan Ramdhani', '3044732468', 'L', 'Bandung', '2004-10-13', 'Islam', 'Jalan Karasak baru no 3 RT 03 RW 02 Karasak kec Astanaanyar Bandung', '085722662724', '0881023454570', 'cepo.sodich@gmail.com', 'M. Cecep Sodikin', 'Pesantren Muhammadiyah', 'TKJ', 'Info Keluarga'),
(5, '2020-06-06 04:58:48', 'ATHALLAH ADITYA SYAHPUTRA', '0054515861', 'L', 'Bandung', '2004-11-28', 'Islam', 'Jl kekal 130b kpad gegerkalong bandung', '087821067222', '081388896725', 'sarirachyuwati797@gmail.com', 'Ally syahbana', 'Smp labschool upi bandung', 'OTKP', 'Info Keluarga'),
(6, '2020-06-09 13:24:33', 'Zahra Aulia Hamdani', '0053722419', 'P', 'Subang', '2005-05-29', 'Islam', 'Buana ciwastra residence blok A1 No.39 Bandun9', '082129287711', '081214915475', 'wdrafigalih@gmail.com', 'Wida Maulida', 'SMPN 1 PAGADEN -SUBANG', 'OTKP', 'Info Keluarga'),
(10, '2020-06-13 02:20:20', 'FADZAR RAMADHAN', '1718.7.165', 'L', 'BANDUNG', '2010-06-04', 'ISLAM', 'SMPN 1 BALEENDAH', '08986922119', '08986922119', 'XX123@gmail.com', 'Doni Agus Maulana', 'SMP NEGERI 1 BALEENDAH', 'TKJ', 'Info Keluarga'),
(9, '2020-06-11 04:52:48', 'Muhammad Yahya Alfarizi', '1819-08-06', 'L', 'Bandung', '2004-06-12', 'Islam', 'Komplek bumi harapan AA 4 no 29 desa cibiru hilir kecamatan cileunyi', '0818647463', '081717708303', 'yahyaalfarizi2929@yahoo.com', 'Rissa khoerunisa hardianti', 'Smp muhamadiyah', 'RPL', 'Info Keluarga'),
(11, '2020-06-22 06:31:11', 'KEVIN NUGRAHA FIRDAUS HASSAN', '0052676183', 'L', 'BANDUNG', '2005-01-09', 'ISLAM', 'SIDOMUKTI 36, BANDUNG', '08562309286', '08992024101', 'kurniawanchaidir@gmail.com', 'KURNIAWAN CHAIDIR', 'SMPN 1 CIANJUR', 'OTKP', 'Media Online'),
(12, '2020-06-23 03:46:55', 'Regina felizia juliar arini', '0046991398', 'P', 'Bandung', '2004-07-17', '', 'Jl pagarsih gg sitimariah 6 Rt09/01', '085950547904', '08979762230', 'reginafeliziaar@gmail.com', 'Maudy madelain', 'Smp dewisartika', 'OTKP', 'Media Online'),
(13, '2020-06-23 10:16:40', 'Galih Kusuma Wiranata', '171807231', 'L', 'Bandung', '2005-06-06', 'Islam', 'Jl. Batu Rahayu No.69 RT 004 RW 010', '0818201165', '085933537145', 'dedijubaedi@yahoo.co.id', 'Dedi Jubaedi', 'SMP Kemala Bhayangkari', 'TKJ', 'Media Online'),
(14, '2020-06-24 06:35:51', 'Rd. Fillery riantanu busthomi', '0054364652', 'L', 'Bandung', '2005-08-22', 'Islam', 'Jln. Pasir honje 1V no. 22 rt 006/ rw 013 kelurahan padasuka kecamatan vimenyan', '081223663878', '0895-2453-9599', 'ritajuwieta2@gmail.com', 'R. Hari busthomi ariffin ST. MSi', 'Smp pgri 10', 'RPL', 'Info Keluarga'),
(15, '2020-06-30 14:35:44', 'Muhamad Dzaky Abdullah', '0052974089', 'L', 'Bandung', '2005-03-26', 'Islam', 'Jl. Sekelimus Utara 1, No. 16', '087822845059', '087737597107', 'keephajim@gmail.com', 'Maman Rustaman', 'SMPS VIJAYA KUSUMA', 'TKJ', 'Media Online'),
(16, '2020-07-02 11:36:30', 'Muhammad Viary Hanifasha Maulana', '0043859184', 'L', 'Bandung', '2004-03-14', 'Islam', 'Putraco Gading Regency Jl. Gading Utara V no 8/15 Soekarno Hatta Bandung', '087735397171', '082316568009', 'eva.ois72@gmail.com', 'Budi Hendra Maulana', 'SMPN 8 Bandung', 'TKJ', 'Media Online'),
(17, '2020-07-08 10:56:52', 'YASZMIN RAHMA DIHARJA', '0055047669', 'P', 'BANDUNG', '2005-04-25', 'Islam', 'Jl. Karees sapuran no.35/121', '082219520016', '082219520016', 'Yaszminrd@gmail.com', 'WENY WINDANINGSIH', 'SMPN 4 BANDUNG', 'OTKP', 'Media Online'),
(18, '2020-07-08 11:02:46', 'Deri Saputra', '0048075870', 'L', 'Bandung', '2004-12-12', 'Islam', 'Jl.ciroyom IV no.180/78 RT 01 RW 08\nKel.ciroyom  kec.andir', '087824735629', '0895356019213', 'derisaputra848@gmail.com', 'Iyang Rohmat/Eti Rohaeti', 'SMP YWKA BANDUNG', 'RPL', 'Info Keluarga'),
(19, '2020-07-08 11:41:28', 'Naufal Firmansyah Suryalaga', '0047228019', 'L', 'Bandung', '2004-11-16', 'Islam', 'Jalan Flaminggo No.45 RT:04 RW:03', '082216697247', '085156450011', 'andisuryalaga24@gmail.com', 'Andi Suryalaga', 'SMP NEGERI 40 BANDUNG', 'RPL', 'Lainnya'),
(20, '2020-07-08 13:28:04', 'Raihan fawaiz', '0000000000', 'L', 'Bandung', '2004-09-05', 'Islam', 'Jl Maleer 5 no 155/188 A', '08213030802', '08213030802', 'hshsjsj@gmail.com', 'Erwin', 'SMP KEMALA BHAYANGKARI', 'TKJ', 'Media Online'),
(21, '2020-07-08 15:00:02', 'MUHAMMAD DAFFA FEDORATULLAH ', '0069988340', 'L', 'Bandung ', '2005-05-15', 'Islam ', 'Jl. Babakan Ciamis No. 86 A RT.06 RW.03 Kelurahan Babakan Ciamis Kecamatan Sumur Bandung, Kota Bandung ', '082129111125', '087719237291', 'dickyganyong@yahoo.co.id', 'Dicky Gustiana ', 'SMP PGII I ', 'TKJ', 'Media Online,Lainnya'),
(22, '2020-07-08 17:17:11', 'Jonathan Felix Dion Prasatya', '0047443982', 'L', 'Bandung', '2004-11-07', 'Katholik', 'Kp. Cilaja muncang, RT.03 RT.02, Desa Mekarmanik, Kec. Cimenyan, Kab. Bandung', '087821341598', '087821341598', 'Tyojonathan12@gmail.com', 'Justinus Dewanto Dwi H', 'SMPK Rehoboth', 'TKJ', 'Info Keluarga'),
(23, '2020-07-09 04:51:55', 'Denny Rinaldy', '0046543030', 'L', 'Bandung', '2004-11-24', 'ISLAM', 'Jl.Kacapiring No.42/122 Rt 04 Rw 03 Kel. Kacapiring  Kec. Batununggal Kota Bandung', '082113089300', '082111430355', 'rinaldy.denny991@gmail.com', 'Aang Dedi Supriadi', 'SMPN 20 Bandung', 'RPL', 'Lainnya'),
(24, '2020-07-09 09:42:05', 'Iqbal Firdaus', '0032003533', 'L', 'Bandung', '2003-10-03', 'Islam', 'Jl.babakan Sumedang ko.79', '085860002029', '085860002029', 'haifasamiyah95@gmail.com', 'Aos Saepudin ', 'SMP Al Kamil Islamic boarding school', 'RPL', 'Media Online'),
(25, '2020-07-10 03:31:44', 'Refa Nayalindra Puspita', '', 'P', 'Bandung', '2004-12-29', 'Islam', 'Jl Sukamanah baru RT 05 RW 13 kel. Sukapura kec. Kiaracondong Kota Bandung', '0895331417317', '0895331417317', 'refanayalindra@gmail.com', 'Indra Berlian Cahayadi', 'SMPN 31 Bandung', 'OTKP', 'Info Keluarga'),
(26, '2020-07-10 09:33:58', 'Amanda Permana putri', '0047944859', 'P', 'Bandung', '2008-08-31', 'Islam', 'Jln cibuntu sayuran Gg sayuran III RT 02 RW 06 Kel warung Muncang', '081315569495', ' 085524577671', 'ranaprmna@gmail.com', 'Rana Permana', 'Pasundan 2 Bandung', 'OTKP', 'Media Cetak'),
(27, '2020-07-10 10:32:39', 'H Heri Permana', '0043494925', 'L', 'bandung', '2004-03-09', 'Islam', 'Komplek bumi harapan cibiru blok AA 9 No. 11', '085864752741', '085523734021', 'putrasheva436@gmail.com', 'Heri permana ', 'Smp vijaya kusuma', 'RPL', 'Lainnya'),
(28, '2020-07-10 13:04:34', 'MUHAMMAD FAJAR MAULANA', '171807149', 'L', 'Bandung', '2004-04-21', 'Islam', 'Jl cijerah gg saluyu 1', '087737801318', '087836387410', 'MuhammadFajarMaulana529@gmail.com', 'ADE RUSMANA', 'SMP PASUNDAN 7 BANDUNG', 'RPL', 'Info Keluarga'),
(29, '2020-08-13 15:06:53', 'anonimus', '78855666', 'L', 'bandung', '2000-08-17', 'anonimus', 'jl.dipatiukur', '089666123404', '089666403404', 'anonimus@gmail.com', 'saa', 'saa', 'RPL', 'Kerjasama');

-- --------------------------------------------------------

--
-- Table structure for table `profil_jurusan`
--

CREATE TABLE `profil_jurusan` (
  `id_jurusan` int(11) NOT NULL,
  `ketua_jurusan` varchar(15) NOT NULL,
  `nama_jurusan` varchar(200) NOT NULL,
  `deskripsi` text,
  `logo_jurusan` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profil_jurusan`
--

INSERT INTO `profil_jurusan` (`id_jurusan`, `ketua_jurusan`, `nama_jurusan`, `deskripsi`, `logo_jurusan`) VALUES
(1, '9', 'Teknik Komputer Jaringan', '<p><strong>Terakreditasi A</strong></p>\r\n<p>Program Keahlian bagi peserta didik yang berminat dalam perakitan-perbaikan komputer (hardware), Networking (jaringan). Menciptakan tenaga terampil dalam bidang/posisi administrator komputer, technical support, computer network maintenance, teknisi maupun keamanan jaringan computer,dll.</p>', 'logojurusan_1.png'),
(2, '7', 'Rekayasa Perangkat Lunak', '<p><strong>Terakreditasi A</strong></p>\r\n<p>Program Keahlian bagi peserta didik yang berminat dalam membuat aplikasi (software-program komputer) berbasis web, desktop, android. Menciptakan tenaga terampil dalam bidang/posisi Operator Web, Junior Programmer, Senior Programmer, Analis Sistem dan Web Design,dll.</p>', 'logojurusan_2.jpg'),
(3, '2', 'Administrasi Perkantoran', '<p><strong>Terakreditasi A</strong></p>\r\n<p>Program Keahlian bagi peserta didik yang berminat dalam administrasi perkantoran, tata kelola administrasi kantor-perusahaan, sekretaris. Menciptakan tenaga terampil dalam bidang/posisi sekretaris, staff administrasi perkantoran, customer service, dll.</p>', 'logojurusan_3.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `profil_sekolah`
--

CREATE TABLE `profil_sekolah` (
  `id_sekolah` int(11) NOT NULL,
  `nama_sekolah` varchar(150) NOT NULL,
  `logo` varchar(150) DEFAULT NULL,
  `foto_profil` varchar(100) NOT NULL,
  `alamat` varchar(250) DEFAULT NULL,
  `no_kontak` varchar(15) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `sambutan_kepsek` text NOT NULL,
  `deskripsi` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profil_sekolah`
--

INSERT INTO `profil_sekolah` (`id_sekolah`, `nama_sekolah`, `logo`, `foto_profil`, `alamat`, `no_kontak`, `email`, `sambutan_kepsek`, `deskripsi`) VALUES
(1, 'SMK BPI Bandung', 'logo_SMKBPIBandung.png', 'fotoprofil_SMKBPIBandung.gif', 'Jl. Burangrang No 08, Kelurahan Burangrang, Kec. Lengkong, Bandung, Jawa Barat', '022 - 7305735', 'info@smkbpi.sch.id', '<p style=\"text-align: left;\"><img class=\"example1\" src=\"../asset/img/pegawai_1.jpg\" alt=\"Kepala Sekolah\" width=\"116\" height=\"144\" align=\"left\" hspace=\"10px\" vspace=\"10px\" /></p>\r\n<p>Assalamualaikum Warahmatullah Wabarakatuh</p>\r\n<p style=\"text-align: justify;\">Alhamdulillahi robbil alamin saya ucapkan&nbsp;kehadlirat Allah SWT, bahwasannya dengan rahmat dan karunia-Nya lah akhirnya Website sekolah ini dapat kami perbaharui. Kami mengucapkan selamat datang SMK&nbsp;BPI Bandung, penyelenggara pendidikan kejuruan berkualitas dan terpercaya.</p>\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n<p style=\"text-align: justify;\">Komitmen kami, dengan segenap jiwa dan raga. SMK BPI akan berupaya amanah dalam mendidik generasi bangsa menjadi pribadi-pribadi berakhlak mulia, berwawasan luas, kompeten, dan berdaya saing tinggi sebagai solusi tantangan perkembangan zaman. Sesuai dengan prinsip pelayanan pendidikan kami : Mengedepankan Pendidikan Kepribadian dan Akhlak Mulia Berbasis Penilaian Holistik.</p>\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n<p style=\"text-align: justify;\">Media website ini kami hadirkan sebagai jembatan informasi mengenai perkembangan SMK BPI dan Keilmuan yang mutakhir. Semoga melalui ketersediaan informasi melalui website ini, SMK BPI senantiasa dikenal sebagai lembaga yang tidak hanya concern terhadap perkembangan internal lembaga tetapi juga concern terhadap perkembangan keilmuan yang bergerak sangat cepat.</p>\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n<p style=\"text-align: justify;\">Saya berharap media ini dapat dijadikan sebagai wahana interaksi yang positif dan memajukan semua pihak terutama civitas akademika SMK BPI. Mari, dengan penuh semangat dan keikhlasan kita bekerja, berkarya dan menjadi pribadi-pribadi positif sebagai \"agen perubahan\" penjawab tantangan zaman.</p>\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n<p>Terima kasih semoga Allah &lsquo;Azza Wa Jalla menyertai doa kita semua&hellip;&hellip;amin.</p>', '<p style=\"text-align: justify;\">&nbsp;</p>\r\n<p style=\"text-align: justify;\"><strong>Sejarah</strong></p>\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n<p style=\"text-align: justify;\">Diawali pada tahun pelajaran baru tahun 2008, &nbsp;keputusan Pengurus yayasan BPI Bandung untuk mendirikan SMK BPI&nbsp; yang memungkinkan untuk memanfaatkan seluruh sarana dan prasarana yang mungkin untuk memberikan pelayanan pendidikan yang dibutuhkan oleh masyarakat. Diperkuat oleh izin operasional SMK BPI oleh DISDIK Kota Bandung No. 421.5/580/PSMAK/2008.</p>\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n<p style=\"text-align: justify;\">Bermodalkan sarana dan prasarana, serta SDM yang ada di lingkungan Yayasan BPI, maka pada saat akhir penutupan Penerimaan Siswa Baru (PSB) ada 9 (sembilan) siswa yang tertarik dan memilih Jurusan Rekayasa Perangkat Lunak (RPL).&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n<p style=\"text-align: justify;\">Kepala Sekolah, atau Pejabat Kepala&nbsp; SMK BPI pertama adalah : SURYADI SURYANINGRAT, M.M hingga tahun 2010.</p>\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n<p style=\"text-align: justify;\">SMK BPI Bandung &nbsp;menjalin kerja sama dengan Dunia Usaha/Dunia Industri sebagai institusi pasangan. Berikut daftar institusi pasangan SMK BPI:</p>\r\n<ol>\r\n<li>Amisya Tours</li>\r\n<li>Dinas Kebudayaan dan Pariwisata Kota Bandung Prov. Jawa Barat</li>\r\n<li>IT ISTA</li>\r\n<li>Rumah Sakit Pendidikan UNPAD</li>\r\n<li>com</li>\r\n<li>Bita Enarcon</li>\r\n<li>Balai Bahasa Provinsi Jawa Barat</li>\r\n<li>Industri Susu Alam Murni</li>\r\n<li>Cimahi Creative Association</li>\r\n<li>Dinas Kebudayaan dN Pariwisata Kota Bandung</li>\r\n<li>Axioo Mitra Abadi</li>\r\n<li>Simaya Jejaring Mandiri</li>\r\n<li>Dinas Perhubungan Kota Bandung</li>\r\n<li>Bank Jabar Banten</li>\r\n<li>Trans Studio Mall</li>\r\n<li>Pindad Persero</li>\r\n<li>Global Media Nusantara</li>\r\n<li>Mandiri, Tbk</li>\r\n<li>Kantor Imigrasi Kelas I, Bandung</li>\r\n<li>Koperasi Pegawai Pemerintah Kota Bandung</li>\r\n<li>Gedung Keuangan Negara</li>\r\n<li>Bank Bukopin</li>\r\n<li>Premier Equity Futures</li>\r\n<li>Gapura Angkasa</li>\r\n<li>Yayasan BPI</li>\r\n</ol>\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n<p style=\"text-align: justify;\">Selain kerja sama dengan DU/DI, SMK BPI Bandung bekerja sama dengan beberapa Perguruan Tinggi Negeri maupun Swasta dan beberapa SMK&nbsp; antara lain :</p>\r\n<ol>\r\n<li>STMIK &nbsp;AMIKOM Yogyakarta</li>\r\n<li>SMK Walisongo, Jakarta</li>\r\n<li>SMK Daarut Tauhid Bandung</li>\r\n<li>SMK Cybermedia, Jakarta</li>\r\n<li>SMK Timika, Papua</li>\r\n<li>Universitas Pendidikan Indonesia</li>\r\n<li>Sekolah Farmasi ITB</li>\r\n</ol>\r\n<p>&nbsp;</p>\r\n<p><strong>PROGRAM UNGGULAN</strong></p>\r\n<p>1. Program Awal Tahun</p>\r\n<ul>\r\n<li>Penerimaan Peserta Didik Baru</li>\r\n<li>Masa Pengenalan Lingkungan Sekolah</li>\r\n<li>In House Training Guru dan Karyawan SMK BPI</li>\r\n<li>Pelantikan Pengurus OSIS</li>\r\n</ul>\r\n<p>2. Program Pertengahan Tahun</p>\r\n<ul>\r\n<li>Audit Internal Manajemen Mutu SMK BPI</li>\r\n<li>Ujian Tengah Semester dan Ujian Akhir Semester Ganjil</li>\r\n<li>Pembagian Raport Semester 1</li>\r\n<li>Kunjungan Industri</li>\r\n<li>Praktik Kerja Industri</li>\r\n<li>Fiesta SMK BPI : Culinary &amp; Acoustic Festival</li>\r\n</ul>\r\n<p>3. Program Akhir Tahun</p>\r\n<ul>\r\n<li>Promosi SMK BPI</li>\r\n<li>Rangkaian Ujian : Uji Kompetensi, Ujian Sekolah, dan Ujian Nasional, UKK.</li>\r\n<li>Rapat Tinjauan Manajemen</li>\r\n<li>Surveillance Audit Manajemen Mutu</li>\r\n<li>Pembagian Raport Semester 2</li>\r\n<li>Survey Kepuasan Pelanggan</li>\r\n</ul>\r\n<p>&nbsp;</p>');

-- --------------------------------------------------------

--
-- Table structure for table `profil_unit_kerja`
--

CREATE TABLE `profil_unit_kerja` (
  `id_unit_kerja` int(11) NOT NULL,
  `unit_kerja` varchar(150) DEFAULT NULL,
  `deskripsi` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profil_unit_kerja`
--

INSERT INTO `profil_unit_kerja` (`id_unit_kerja`, `unit_kerja`, `deskripsi`) VALUES
(1, 'Kurikulum', '<p>Unit kerja yang bertugas untuk&nbsp;memahami, mengkaji dan melaksanakan dan pengembangan Kurikulum, melakukan sinkronisasi kurikulum dinas pendidikan dengan kebutuhan di dunia industri sehingga melahirkan kurikulum yang terintegrasi.</p>\r\n<p>&nbsp;</p>\r\n<p>Kurikulum SMK BPI mengacu kepada Kurikulum 2013 dengan dipadukan dengan keilmuan mutakhir di dunia industri agar peserta didik mendapatkan pengetahuan dan keterampilan yang sesuai perkembangan zaman.</p>'),
(3, 'Kesiswaan', '<p>Unit kerja yang bertugas dalam mengatur perkembangan peserta didik (siswa) demi tercipta&nbsp;pribadi yang disiplin, mandiri, dan berakhlak mulia. Memastikan perkembangan peserta didik (siswa) berada pada jalur yang benar berdasarkan mekanisme yang di rancang untuk menstimulasi perkembangan peserta didik. Kesiswaan didampingin oleh unit kerja :</p>\r\n<ol>\r\n<li>Bimbingan Konseling</li>\r\n<li>OSIS</li>\r\n</ol>'),
(5, 'Manajemen Mutu', '<p>Unit kerja yang bertugas untuk mengawal implementasi sistem manajemen mutu (SMM) ISO 9001:2008. Sistem manajemen mutu akan memastikan bahwa mekanisme pelayanan pendidikan di SMK BPI berjalan sesuai prosedur yang telah ditetapkan.</p>'),
(7, 'Hubungan Industri', '<p>Unit kerja yang bertugas sebagai jembatan penjalin kemitraan antara SMK BPI dengan dunia industri atau lembaga lainnya sebagai jejaring dalam pelaksanaan program :</p>\r\n<ol>\r\n<li>Praktik Kerja Industri (Prakerin)</li>\r\n<li>Kemitraan, sebagai penyedia lowongan pekerjaan</li>\r\n<li>Program pendidikan lainnya</li>\r\n</ol>\r\n<p>Unit kerja ini didampingi oleh unit kerja :</p>\r\n<p>Bursa Kerja Khusus (BKK) sebagai unit pencari lowongan pekerjaan bagi lulusan dan alumni SMK BPI.</p>');

-- --------------------------------------------------------

--
-- Table structure for table `silabus`
--

CREATE TABLE `silabus` (
  `id_silabus` int(11) NOT NULL,
  `silabus` varchar(150) NOT NULL,
  `mata_pelajaran` varchar(100) NOT NULL,
  `tahun_berlaku` int(11) NOT NULL,
  `tgl_posting` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `silabus`
--

INSERT INTO `silabus` (`id_silabus`, `silabus`, `mata_pelajaran`, `tahun_berlaku`, `tgl_posting`) VALUES
(1, 'Silabus_2015-2016_1_PemrogramanDasarKelasX.doc', 'Pemrograman Dasar Kelas X', 2015, '2016-03-16 07:55:57'),
(3, 'Silabus_Sekarang_3_PerakitanKomptuer.docx', 'Perakitan Komptuer', 2015, '2016-03-21 09:58:18');

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE `siswa` (
  `id_siswa` int(11) NOT NULL,
  `nis` varchar(10) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `jenis_kelamin` varchar(2) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `id_kelas` int(11) DEFAULT NULL,
  `foto` varchar(150) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`id_siswa`, `nis`, `nama`, `jenis_kelamin`, `username`, `password`, `id_kelas`, `foto`) VALUES
(1, '141510001', 'ABILLILAH AULIYA  S', 'L', '141510001', 'e6efb1a6cedcd14eba9c3a3838318913', 16, 'pegawai_male_default.jpg'),
(2, '141510002', 'ADELYA FITRI PRATIWI', 'P', '141510002', 'a0057704947eee4950a71e7ad93433f0', 16, 'pegawai_female_default.jpg'),
(3, '141510004', 'ALDO JULIO  P', 'L', '141510004', '549437a5960180adb0bc2c76bbce8d7a', 16, 'pegawai_male_default.jpg'),
(4, '141510005', 'ALIIF RAIS ASSIDIQ', 'L', '141510005', '1894cc6b79983d783de74751ad5e6308', 16, 'pegawai_male_default.jpg'),
(5, '141510006', 'ANDIKA RIZKI', 'L', '141510006', 'f6aebf7dfd9166acb6e3c7c75abce74c', 16, 'pegawai_male_default.jpg'),
(6, '141510007', 'ANDRI AJI ARIANTO', 'L', '141510007', '7e899ac8bdb9df7a0a08ac83b4577eab', 16, 'pegawai_male_default.jpg'),
(7, '141510008', 'ANDRI SUDRAJAT  S.', 'L', '141510008', '29519d28132eb995e7d6779167bbc0ba', 16, 'pegawai_male_default.jpg'),
(8, '141510009', 'ARDIANSYAH  Z  F', 'L', '141510009', '3d2ad5a7c51f38ad2e8b7b6744af3669', 16, 'pegawai_male_default.jpg'),
(9, '141510011', 'DANI CANDRA S', 'L', '141510011', '05fb51e959608d3f9a19dfd81d536cd5', 16, 'pegawai_male_default.jpg'),
(10, '141510012', 'DENI MAULANA  M', 'L', '141510012', '90a62a89685beaedeeba5e019967f45f', 16, 'pegawai_male_default.jpg'),
(11, '141510013', 'DEWA GEDE G', 'L', '141510013', '7751eabd21b1fce22326c097e72b32d5', 16, 'pegawai_male_default.jpg'),
(12, '141510014', 'DIAN ANDRIA', 'L', '141510014', '1b4554a8fdba8004742fd37b3a421b6a', 16, 'pegawai_male_default.jpg'),
(13, '141510015', 'ERIK ANDRAWINA', 'L', '141510015', 'a03631c7b1145e8f3389fbeae3cb13d2', 16, 'pegawai_male_default.jpg'),
(14, '141510016', 'FERI ADRIAN', 'L', '141510016', '45c71a7d27028a46ecdbf3afef0813a4', 16, 'pegawai_male_default.jpg'),
(15, '141510017', 'GALANG AKMAL M', 'L', '141510017', 'a4567629f281e8b6ace9d51faa1163d3', 16, 'pegawai_male_default.jpg'),
(16, '141510018', 'JAKA PRAYUDA  A.', 'L', '141510018', '06937c158b31c017a81f847a4a3c790f', 16, 'pegawai_male_default.jpg'),
(17, '131410018', 'MOCHAMAD FIRMAN', 'L', '131410018', '3e4b0b4a3fd03c837b9a9c7125558635', 16, 'pegawai_male_default.jpg'),
(18, '141510020', 'MUHAMAD BIMA', 'L', '141510020', 'e9b2804a5056dfd958c3540abad4cf21', 16, 'pegawai_male_default.jpg'),
(19, '141510021', 'MUHAMMAD AKBARI', 'L', '141510021', '6d4c99d21ecfb9874653a6f84c10a489', 16, 'pegawai_male_default.jpg'),
(20, '141510022', 'NAFIS MUHAMMAD N', 'L', '141510022', 'd5f396863dd62812aa9dcff2c7c98ebf', 16, 'pegawai_male_default.jpg'),
(21, '141510023', 'SALMA FITRIAZI  D', 'P', '141510023', '7b95db9a70a37373ab8c5d5db95062b4', 16, 'pegawai_female_default.jpg'),
(22, '141510024', 'SOFIAN HENRI  CH', 'L', '141510024', '1367d33b310f753cab84ebee003c18e2', 16, 'pegawai_male_default.jpg'),
(23, '141510025', 'TIMOTIUS GERALDY', 'L', '141510025', '9ca7f976ac35c10615ce8ec961ada15d', 16, 'pegawai_male_default.jpg'),
(24, '141510026', 'VENI NUR ELA SARI', 'P', '141510026', 'f0dc050731358e71506c08e272ceef40', 16, 'pegawai_female_default.jpg'),
(25, '141510027', 'WILDAN PRATAMA', 'L', '141510027', 'ffa3806a94d032003efc15541edc52b2', 16, 'pegawai_male_default.jpg'),
(26, '141510028', 'WILMAN APRIADI', 'L', '141510028', '238a3b044eaa5f3fede67caebc50fe06', 16, 'pegawai_male_default.jpg'),
(27, '141510029', 'YANA DARMAWAN', 'L', '141510029', '000faff6b2f06d4c01af7fa39f8bee90', 16, 'pegawai_male_default.jpg'),
(28, '141510030', 'YULIANTHY  P', 'P', '141510030', 'a82a0ebddb2083ab2180821385cf01e2', 16, 'pegawai_female_default.jpg'),
(29, '151611099', 'PRANA MANGGUNG  P', 'L', '151611099', '4fd67ab723ec56ea66f033b8331632fa', 16, 'pegawai_male_default.jpg'),
(30, '151611100', 'RIZQIA AWALU KAMILA', 'P', '151611100', '0fd066fccdddf1904c5266d9d59aa904', 16, 'pegawai_female_default.jpg'),
(31, '141510031', 'AGUNG MARDIANTO', 'L', '141510031', '99b520fe3ec0d0a0e2b05d8bd7e97937', 17, 'pegawai_male_default.jpg'),
(32, '141510032', 'AGUNG SETIA  N', 'L', '141510032', '2ed78af7b81d0719a948539560c94a12', 17, 'pegawai_male_default.jpg'),
(33, '141510034', 'ANANDA ANDIKA  BP', 'L', '141510034', '314eac17cf9501ebc61374982042f6ff', 17, 'pegawai_male_default.jpg'),
(34, '141510035', 'ARIEF BUDI  KUSUMAH', 'L', '141510035', '9cfaf89cc9307aacdb3ed8d6307126b8', 17, 'pegawai_male_default.jpg'),
(35, '141510036', 'ARIQ ESA PRATAMA', 'L', '141510036', '39f3abd39487f10e3c4d00071b34ff9b', 17, 'pegawai_male_default.jpg'),
(36, '141510038', 'DAVIN GIOVANO WILIANTO', 'L', '141510038', 'ca108537047b82e97d1c217da65730d1', 17, 'pegawai_male_default.jpg'),
(37, '141510039', 'DHONI SUBAGJA', 'L', '141510039', 'e116cdb4dfcf54c6921728fa90deec7a', 17, 'pegawai_male_default.jpg'),
(38, '141510040', 'DIDA HANDIAN', 'L', '141510040', '929c31be96a8d2329c7cf6b1dff645e8', 17, 'pegawai_male_default.jpg'),
(39, '141510041', 'EKA BANGKIT  S', 'L', '141510041', 'a9d2f8ca7e03f2c77ffeaa709d254934', 17, 'pegawai_male_default.jpg'),
(40, '141510043', 'GILANG DWI  G', 'L', '141510043', 'f7bb3c9e55ab9aaaebcd159705c88dd8', 17, 'pegawai_male_default.jpg'),
(41, '141510044', 'GIYAS FAUZI RASYID', 'L', '141510044', 'c99225fcfb1e0c0b0b451afba5b2713f', 17, 'pegawai_male_default.jpg'),
(42, '141510045', 'GUNTUR WINANGUN', 'L', '141510045', '829b811d133f98c5e0a1c4c33135c023', 17, 'pegawai_male_default.jpg'),
(43, '141510046', 'HASBY SHAHID  A', 'L', '141510046', '87982417b112cdfbc49902efe6d4284f', 17, 'pegawai_male_default.jpg'),
(44, '141510048', 'IRSAN SETIAWAN', 'L', '141510048', 'bbece4fdd39619e216170dbdbed71775', 17, 'pegawai_male_default.jpg'),
(45, '141510050', 'MOCHAMAD AGIL', 'L', '141510050', '06fa50b97370a044adfc0158630444f9', 17, 'pegawai_male_default.jpg'),
(46, '141510051', 'MUHAMAD RIKI  R', 'L', '141510051', '3dc01e4b457add6e0c55782dee8c9a3c', 17, 'pegawai_male_default.jpg'),
(47, '141510052', 'MUHAMAD FAHMI  A', 'L', '141510052', 'd2c0194e7f902a0607cb3e362b659147', 17, 'pegawai_male_default.jpg'),
(48, '141510053', 'MUHAMMAD FAISAL R', 'L', '141510053', 'f5620c4264bfb421053f358fb8153d53', 17, 'pegawai_male_default.jpg'),
(49, '141510054', 'MUHAMMAD RAKA A S', 'L', '141510054', '0bea620d461b5e88ae4da4acd6fb7cd6', 17, 'pegawai_male_default.jpg'),
(50, '141510055', 'MUHAMMAD ZENCA R', 'L', '141510055', '61ec6aee505280062da907f9e55225ad', 17, 'pegawai_male_default.jpg'),
(51, '141510056', 'MUSLIH SAHAL', 'L', '141510056', '8de4c23c1cbf6c122d88451f9e50e57a', 17, 'pegawai_male_default.jpg'),
(52, '141510057', 'MOCH. RAFLY K', 'L', '141510057', 'f1d977896706c88fc52200e7cae853ef', 17, 'pegawai_male_default.jpg'),
(53, '141510058', 'NITA FEBRIYANTI', 'P', '141510058', 'c195fa47205168fcb8bcb87d01517ce4', 17, 'pegawai_female_default.jpg'),
(54, '141510059', 'RAHAYU ANDINI', 'P', '141510059', '48a7d78e909f57a2016fb0c6f8afe0c3', 17, 'pegawai_female_default.jpg'),
(55, '141510060', 'RAKASH ZULFIKAR', 'L', '141510060', 'f469480ce73536398d27f2e78469da07', 17, 'pegawai_male_default.jpg'),
(56, '141510061', 'REFALDI', 'L', '141510061', '64f3bf0e51ec6f318c975f93040ee3a6', 17, 'pegawai_male_default.jpg'),
(57, '141510063', 'SATRIO ADHY W', 'L', '141510063', '0e4b4357d3a68af48def71bea70d5304', 17, 'pegawai_male_default.jpg'),
(58, '141510064', 'SURYA PRAYOGA', 'L', '141510064', '1d6f1f6213b881363ac2e430f0d8b5f6', 17, 'pegawai_male_default.jpg'),
(59, '141510065', 'SYAHRIO SANTOSO', 'L', '141510065', '2f8c0fa5ae1c6f97afadce5cf984a549', 17, 'pegawai_male_default.jpg'),
(60, '141510066', 'YAN ANDRIYAN', 'L', '141510066', '84d4d836583a27ada900a583b7e7bf7c', 17, 'pegawai_male_default.jpg'),
(61, '141510067', 'YODI RIVELINO', 'L', '141510067', 'a09d8a9905da895c3fcec802f2e1941e', 17, 'pegawai_male_default.jpg'),
(62, '141510068', 'YOGA YUDIANA', 'L', '141510068', 'f84efc376f69db6ae7c16fd21f2ebb97', 17, 'pegawai_male_default.jpg'),
(63, '141510070', 'ALDA DWISKA PUTRI ', 'P', '141510070', '84ef5fd6616d15f2344893762e1bcf1d', 18, 'pegawai_female_default.jpg'),
(64, '141510071', 'AMALIA SHABRINA', 'P', '141510071', 'bf4433dc39f138fdb08bd410aadadd66', 18, 'pegawai_female_default.jpg'),
(65, '141510072', 'ANGGUN MAHARRANI', 'P', '141510072', '23de90adf8bfe16712f759efcb5565c5', 18, 'pegawai_female_default.jpg'),
(66, '141510073', 'ANNISA NOER HADI', 'P', '141510073', '8f141bf8b7b6afb3f1250224ca6b6aa0', 18, 'pegawai_female_default.jpg'),
(67, '141510074', 'DESI RACHMAHWATI  N', 'P', '141510074', 'ec82d6727680c7244c0ca631b8cc8b0e', 18, 'pegawai_female_default.jpg'),
(68, '141510075', 'DESTYA RANDYANI', 'P', '141510075', '61fb488d36bdac1a314503e2a31eb72a', 18, 'pegawai_female_default.jpg'),
(69, '141510076', 'DEVI RISMAWATI', 'P', '141510076', 'c5264f527e7debff150b2d5827158b66', 18, 'pegawai_female_default.jpg'),
(70, '141510077', 'DHEA  RACHMAWATI', 'P', '141510077', '719a25c999324e0227efbac983f92ee2', 18, 'pegawai_female_default.jpg'),
(71, '141510078', 'EDELWEIS', 'P', '141510078', 'be93050a031f56e0e4bbe565461d9c76', 18, 'pegawai_female_default.jpg'),
(72, '141510079', 'FAMILA APRIANTI', 'P', '141510079', 'f2693959525f04b467f7d6695892ef91', 18, 'pegawai_female_default.jpg'),
(73, '141510080', 'FEBRIANTI', 'P', '141510080', '2bfb5b3ca211c65c9785d6d0513318d9', 18, 'pegawai_female_default.jpg'),
(74, '141510081', 'INDAH KHOIRUNNISA', 'P', '141510081', '2b98c8c88614800a18f33a115d3a257f', 18, 'pegawai_female_default.jpg'),
(75, '141510082', 'KIRANA SEPTIYANI P', 'P', '141510082', '3646876e47f54561f7de4dbd590a45bd', 18, 'pegawai_female_default.jpg'),
(76, '141510083', 'LITA TAMARISKA  S', 'P', '141510083', '2c1ae6cfb188b9388075bc01241f1607', 18, 'pegawai_female_default.jpg'),
(77, '141510084', 'LUFTYFEBRIYANTI', 'P', '141510084', '7154372dd69e7f90af7b24bed3003af7', 18, 'pegawai_female_default.jpg'),
(78, '141510085', 'MITA NOVIANTI', 'P', '141510085', 'e228983a9c902810fd7564c53e37bad1', 18, 'pegawai_female_default.jpg'),
(79, '141510086', 'NABILA RIZTINA', 'P', '141510086', '7ced8b115a00f130474ada047b396af1', 18, 'pegawai_female_default.jpg'),
(80, '141510087', 'NADYA PUSPITARINI', 'P', '141510087', '833f36c64470a6b93e509d05a1104444', 18, 'pegawai_female_default.jpg'),
(81, '141510088', 'NAILLA AZHARI  N', 'P', '141510088', '1b2184ca6a40dae31aa435f9de056cf6', 18, 'pegawai_female_default.jpg'),
(82, '141510089', 'NATASYA MENTARI D', 'P', '141510089', '030130d214368af6faf479ea9d09899d', 18, 'pegawai_female_default.jpg'),
(83, '141510090', 'NENDAH KHOFIFAH', 'P', '141510090', 'dab66dcbf71306cc1a16f1501c590eae', 18, 'pegawai_female_default.jpg'),
(84, '141510091', 'NITA JULIAN NABILA', 'P', '141510091', '61ab1b0ab6ed185029b2cbff9c5c07e0', 18, 'pegawai_female_default.jpg'),
(85, '141510092', 'NOVI TRIANI JUARIAH', 'P', '141510092', 'ea06b2ec1072dd53e4d87a0a51a9974e', 18, 'pegawai_female_default.jpg'),
(86, '141510093', 'PUTRI ANGGITA F K', 'P', '141510093', 'f17ce97116bfe556c32fb0dc14fbee5f', 18, 'pegawai_female_default.jpg'),
(87, '141510094', 'RISKA  KRISTY', 'P', '141510094', '09451d39d7d87e7290e94a69eb703876', 18, 'pegawai_female_default.jpg'),
(88, '141510095', 'RISKA TISNAYANTI', 'P', '141510095', 'abea099e22a1cba407f9731bec72c706', 18, 'pegawai_female_default.jpg'),
(89, '141510096', 'RIZKIA MUTIARA H', 'P', '141510096', '9c34182d07d82ccc8b312d45f592b84a', 18, 'pegawai_female_default.jpg'),
(90, '141510097', 'ROSALINA', 'P', '141510097', '608aee7d4d4053002c5fc8f468b48c28', 18, 'pegawai_female_default.jpg'),
(91, '141510098', 'SHAFIRA PUTRI AZKIA', 'P', '141510098', '6cbc84f4ffbcdbc0e904e794791e9f5a', 18, 'pegawai_female_default.jpg'),
(92, '141510099', 'SHAFIRA RIZKI  A', 'P', '141510099', '135b99d29aa0b0b74662e2bc57af8e35', 18, 'pegawai_female_default.jpg'),
(93, '141510100', 'SHALSYA ARTAMEIVIA', 'P', '141510100', '350f8206cc704e2625acbb7ec52436c7', 18, 'pegawai_female_default.jpg'),
(94, '141510101', 'TIA MEYDIANA', 'P', '141510101', 'efde1fac19a59f32e07b8d5cbd00cad7', 18, 'pegawai_female_default.jpg'),
(95, '141510102', 'WINDA ROSWIANDA', 'P', '141510102', '6fabbc407f3616f4f6a70dbc926b613f', 18, 'pegawai_female_default.jpg'),
(96, '141510103', 'YULI PUJIANTI', 'P', '141510103', 'f24c3e399abb4003c11b801d99dd190b', 18, 'pegawai_female_default.jpg'),
(97, '141510104', 'YULIA DEVI YANTI  M', 'P', '141510104', '5e77aa807da42f8ba037e829bb00c8e6', 18, 'pegawai_female_default.jpg'),
(98, '141510105', 'YULINDA EKA PUTRI', 'P', '141510105', '599e83a9e9f8226eba9a0f3866e6861d', 18, 'pegawai_female_default.jpg'),
(99, '141510107', 'SILVANA APRIYAN TI  F', 'P', '141510107', 'be858050feb80e686f9c4bbc0c47445a', 18, 'pegawai_female_default.jpg'),
(100, '1006441', 'Anak Ganteng', 'L', '1006441', '22fd60de8c1b8ef03bf63e20dfe69b20', 18, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `slide`
--

CREATE TABLE `slide` (
  `id_slide` int(11) NOT NULL,
  `judul_slide` varchar(50) NOT NULL,
  `deskripsi` varchar(350) NOT NULL,
  `aksi_slide` int(11) NOT NULL,
  `publish` varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `struktur_kurikulum`
--

CREATE TABLE `struktur_kurikulum` (
  `id_struktur_kurikulum` int(11) NOT NULL,
  `struktur_kurikulum` varchar(150) NOT NULL,
  `id_jurusan` int(11) NOT NULL,
  `tahun_berlaku` int(11) NOT NULL,
  `tgl_posting` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `struktur_kurikulum`
--

INSERT INTO `struktur_kurikulum` (`id_struktur_kurikulum`, `struktur_kurikulum`, `id_jurusan`, `tahun_berlaku`, `tgl_posting`) VALUES
(1, 'StrukturKurikulum_2015_1_1.docx', 1, 2015, '2016-02-09 05:00:02');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `nama_user` varchar(150) NOT NULL,
  `username` varchar(150) NOT NULL,
  `password` varchar(150) NOT NULL,
  `level_user` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `nama_user`, `username`, `password`, `level_user`) VALUES
(13, 'Admin', 'adminteachSMK', '0bd884c12fb14ff4f937e3110b334a67', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_siswa_to`
--

CREATE TABLE `user_siswa_to` (
  `id_siswa_to` int(11) NOT NULL,
  `nama_siswa_to` varchar(150) NOT NULL,
  `username_siswa_to` varchar(150) NOT NULL,
  `password_siswa_to` varchar(150) NOT NULL,
  `asal_sekolah` varchar(150) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `visi_misi`
--

CREATE TABLE `visi_misi` (
  `id_visi_misi` int(11) NOT NULL,
  `visi` text,
  `misi` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `visi_misi`
--

INSERT INTO `visi_misi` (`id_visi_misi`, `visi`, `misi`) VALUES
(1, '<p><em>Penyelenggaran Pendidikan Kejuruan Berkualitas dan Terpercaya Kota Bandung Tahun 2018.</em></p>\r\n<p>&nbsp;</p>\r\n<p>SMK BPI Bandung bertekad untuk menciptakan peserta didik yang berakhlak mulia, terampil/kompeten, berdaya saing tinggi. Dalam menghadapi tantangan global dalam dunia pekerjaan, perguruan tinggi, dan wirausaha.</p>', '<ol>\r\n<li>Mewujudkan tata kelola, sistem pengendalian manajemen, dan sistem pengawasan internal yang modern, efektif, dan efisien.</li>\r\n<li>Mewujudkan budaya religi, jujur, disiplin, beretika, berestetika, pekerja keras, kreatif, inovatif, kompetitif, dan berkwalitas.</li>\r\n<li>Mewujudkan dinamisasi peningkatan kualitas pendidikan berkarakter yang berkesinambungan dan berkelanjutan.</li>\r\n<li>Mewujudkan Produk kompetensi keahlian Bernilai Jual Pasar Global.</li>\r\n<li>Memperluas akses kemitraan dunia kerja yang menjamin lapangan kerja dan prakerin bagi peserta didik dan lulusan SMK BPI.</li>\r\n<li>Mewujudkan lulusan yang handal di bidangnya dan fasih berbahasa Inggris sehingga dipercaya oleh segenap dunia kerja pemerintah&nbsp; maupun swasta.</li>\r\n<li>Mewujudkan jiwa <em>entrepreneurship</em> kuat yang mampu meningkatkan kwalitas hidup civitas akademika SMK BPI.</li>\r\n</ol>');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `absensi`
--
ALTER TABLE `absensi`
  ADD PRIMARY KEY (`id_absensi`);

--
-- Indexes for table `artikel`
--
ALTER TABLE `artikel`
  ADD PRIMARY KEY (`id_artikel`);

--
-- Indexes for table `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`id_berita`);

--
-- Indexes for table `bursa_kerja`
--
ALTER TABLE `bursa_kerja`
  ADD PRIMARY KEY (`id_bursa_kerja`);

--
-- Indexes for table `ekstrakulikuler`
--
ALTER TABLE `ekstrakulikuler`
  ADD PRIMARY KEY (`id_ekstrakulikuler`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id_feedback`);

--
-- Indexes for table `galeri`
--
ALTER TABLE `galeri`
  ADD PRIMARY KEY (`id_galeri`);

--
-- Indexes for table `guru_piket`
--
ALTER TABLE `guru_piket`
  ADD PRIMARY KEY (`id_guru_piket`);

--
-- Indexes for table `histori_kelas`
--
ALTER TABLE `histori_kelas`
  ADD PRIMARY KEY (`id_histori_kelas`);

--
-- Indexes for table `info_ppdb`
--
ALTER TABLE `info_ppdb`
  ADD PRIMARY KEY (`id_info_ppdb`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`id_jabatan`);

--
-- Indexes for table `kategori_artikel`
--
ALTER TABLE `kategori_artikel`
  ADD PRIMARY KEY (`id_kategori_artikel`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id_kelas`);

--
-- Indexes for table `kepegawaian`
--
ALTER TABLE `kepegawaian`
  ADD PRIMARY KEY (`id_pegawai`);

--
-- Indexes for table `keterangan_absen`
--
ALTER TABLE `keterangan_absen`
  ADD PRIMARY KEY (`id_keterangan`);

--
-- Indexes for table `other`
--
ALTER TABLE `other`
  ADD PRIMARY KEY (`id_other`);

--
-- Indexes for table `pendaftaran`
--
ALTER TABLE `pendaftaran`
  ADD PRIMARY KEY (`id_pendaftaran`);

--
-- Indexes for table `profil_jurusan`
--
ALTER TABLE `profil_jurusan`
  ADD PRIMARY KEY (`id_jurusan`);

--
-- Indexes for table `profil_sekolah`
--
ALTER TABLE `profil_sekolah`
  ADD PRIMARY KEY (`id_sekolah`);

--
-- Indexes for table `profil_unit_kerja`
--
ALTER TABLE `profil_unit_kerja`
  ADD PRIMARY KEY (`id_unit_kerja`);

--
-- Indexes for table `silabus`
--
ALTER TABLE `silabus`
  ADD PRIMARY KEY (`id_silabus`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`id_siswa`),
  ADD UNIQUE KEY `nis` (`nis`);

--
-- Indexes for table `slide`
--
ALTER TABLE `slide`
  ADD PRIMARY KEY (`id_slide`);

--
-- Indexes for table `struktur_kurikulum`
--
ALTER TABLE `struktur_kurikulum`
  ADD PRIMARY KEY (`id_struktur_kurikulum`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `user_siswa_to`
--
ALTER TABLE `user_siswa_to`
  ADD PRIMARY KEY (`id_siswa_to`);

--
-- Indexes for table `visi_misi`
--
ALTER TABLE `visi_misi`
  ADD PRIMARY KEY (`id_visi_misi`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `absensi`
--
ALTER TABLE `absensi`
  MODIFY `id_absensi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `artikel`
--
ALTER TABLE `artikel`
  MODIFY `id_artikel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `berita`
--
ALTER TABLE `berita`
  MODIFY `id_berita` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `bursa_kerja`
--
ALTER TABLE `bursa_kerja`
  MODIFY `id_bursa_kerja` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `ekstrakulikuler`
--
ALTER TABLE `ekstrakulikuler`
  MODIFY `id_ekstrakulikuler` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id_feedback` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `galeri`
--
ALTER TABLE `galeri`
  MODIFY `id_galeri` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `guru_piket`
--
ALTER TABLE `guru_piket`
  MODIFY `id_guru_piket` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `histori_kelas`
--
ALTER TABLE `histori_kelas`
  MODIFY `id_histori_kelas` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `info_ppdb`
--
ALTER TABLE `info_ppdb`
  MODIFY `id_info_ppdb` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `jabatan`
--
ALTER TABLE `jabatan`
  MODIFY `id_jabatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `kategori_artikel`
--
ALTER TABLE `kategori_artikel`
  MODIFY `id_kategori_artikel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `kelas`
--
ALTER TABLE `kelas`
  MODIFY `id_kelas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `kepegawaian`
--
ALTER TABLE `kepegawaian`
  MODIFY `id_pegawai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `keterangan_absen`
--
ALTER TABLE `keterangan_absen`
  MODIFY `id_keterangan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `other`
--
ALTER TABLE `other`
  MODIFY `id_other` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pendaftaran`
--
ALTER TABLE `pendaftaran`
  MODIFY `id_pendaftaran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `profil_jurusan`
--
ALTER TABLE `profil_jurusan`
  MODIFY `id_jurusan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `profil_sekolah`
--
ALTER TABLE `profil_sekolah`
  MODIFY `id_sekolah` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `profil_unit_kerja`
--
ALTER TABLE `profil_unit_kerja`
  MODIFY `id_unit_kerja` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `silabus`
--
ALTER TABLE `silabus`
  MODIFY `id_silabus` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `siswa`
--
ALTER TABLE `siswa`
  MODIFY `id_siswa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1001;

--
-- AUTO_INCREMENT for table `slide`
--
ALTER TABLE `slide`
  MODIFY `id_slide` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `struktur_kurikulum`
--
ALTER TABLE `struktur_kurikulum`
  MODIFY `id_struktur_kurikulum` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `user_siswa_to`
--
ALTER TABLE `user_siswa_to`
  MODIFY `id_siswa_to` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `visi_misi`
--
ALTER TABLE `visi_misi`
  MODIFY `id_visi_misi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
