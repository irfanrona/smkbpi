$(document).ready(function () {

    $('#mydata').DataTable();

    $(".readmore").expander({

        slicePoint: 200,

        expandText: 'Lanjut membaca ',

        userCollapseText: 'Sembunyikan'

    });

    $(".read").expander({

        slicePoint: 50,

        expandText: 'Lanjut membaca ',

        userCollapseText: 'Sembunyikan'

    });



    $('.btn-delete').on('click', function (e) {

        e.preventDefault();

        const href = $(this).attr('href');



        Swal.fire({

            title: 'Apakah anda yakin?',

            text: 'Data ini akan dihapus!',

            type: 'warning',

            showCancelButton: true,

            confirmButtonColor: '#3085d6',

            cancelButtonColor: '#d33',

            confirmButtonText: 'Hapus!'

        }).then((result) => {

            if (result.value) {

                document.location.href = href;

            }

        });

    });



    const flashdata = $('.flash-data').data('flashdata');

    if (flashdata) {

        Swal.fire({

            type: 'success',

            title: 'Data berhasil ' + flashdata,

            showConfirmButton: false,

            timer: 1500

        });

    }



});